<?php

namespace Services;

class ListPager
{
	//DEPENDENCY INJECTION
    private $request;
	
	private $NextPage;
	private $PrevPage;
	private $LastPage;
		
	private $Repository;
	private $Results;
	private $ResultsCount;
	
    private $MaxResults;
	private $FirstResult=0;
	private $CurrentPage=1;
	
	private $FilterSession;
	private $OrderSession;

    public function __construct($request)
    {
        $this->request = $request;
    }
	
	//INIT THE PAGER FOR LIST
	public function setRepositoryAndMaxResults($Repository, $MaxResults)
	{
        $this->Repository = $Repository;
        $this->MaxResults = $MaxResults;
		$this->setPagerData();
	}
	
	
	private function setPagerData()
	{
		//GET ALL RESULTS
		$AllResults = $this->Repository->getResult();
		$this->ResultsCount=count($AllResults); //NUMBER OF RESULTS
				
		//CHECKING LIST PAGE
		if($this->request->query->get('page') && is_numeric($this->request->query->get('page')) ):
			$this->CurrentPage = $this->request->query->get('page');
			$this->FirstResult = ( $this->CurrentPage * $this->MaxResults ) - $this->MaxResults;
		else:
			$this->FirstResult = 0;
		endif;

		$this->NextPage = $this->CurrentPage+1;
		$this->PrevPage = $this->CurrentPage-1;
		$this->LastPage = ceil($this->ResultsCount/$this->MaxResults);
	}
	public function getResults()
	{
		//GET PAGED RESULTS		
		$this->Results= $this->Repository
			->setFirstResult( $this->FirstResult )
			->setMaxResults( $this->MaxResults)
			->getResult();
			
		return $this->Results;
	}
	
	public function getPagerData()
	{
		//CHECKING NEXT AND PREV PAGE
		if($this->PrevPage < 1): $this->PrevPage = null; endif;
		if($this->NextPage > $this->LastPage): $this->NextPage = null;	endif;

		//CREATING LINKS
    	$Links = array(); $NumberLinks = 5;
	    $tmp   = $this->CurrentPage - floor($NumberLinks / 2);
	    $check = $this->LastPage - $NumberLinks + 1;
	    $limit = $check > 0 ? $check : 1;
	    $begin = $tmp > 0 ? ($tmp > $limit ? $limit : $tmp) : 1;

	    $i = (int) $begin;
	    while ($i < $begin + $NumberLinks && $i <= $this->LastPage)
	    {
	      $Links[] = $i++;
	    }

		//ASSEMBLING ARRAY TO RETURN
		$PagerData= array(
			'PrevPage'=> $this->PrevPage,
			'NextPage'=> $this->NextPage,
			'CurrentPage'=> $this->CurrentPage,
			'LastPage'=> $this->LastPage,
			'Links'=> $Links,
		);

		return $PagerData;
	}
}
