<?php

namespace Wamadmin\AssistantBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class DefaultController extends Controller
{
    public function indexAction()
    {
        return $this->render('WamadminAssistantBundle:Default:index.html.twig');
    }
}
