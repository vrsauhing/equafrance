<?php

namespace Wamadmin\AdminBundle\Controller;

//LIBRARIES
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;

//ENTITIES
use Wamadmin\CoreBundle\Entity\ClientUser;

//FORMS
use Wamadmin\CoreBundle\Form\_ClientUser\ClientUserForm;
use Wamadmin\CoreBundle\Form\_ClientUser\ClientUserEditForm;
use Wamadmin\CoreBundle\Form\_ClientUser\ClientUserFilterForm;

class ClientUserController extends Controller
{
    public function ListAction(Request $request)
    {
		//init list session
		$session = $request->getSession();
		$OrderSession='OrderClientUserList'; $FilterSession='FilterClientUserList';
		
		//preparing filter form 
    	$FilterForm = $this->createForm(new ClientUserFilterForm(), $session->get($FilterSession) );
				
		//check if list has filter form
		if ($request->isMethod('POST')):
			$FilterForm->bind($request);
			$this->ProcessFilter($FilterForm, $FilterSession);//processing filter POST
		endif;
		
		//preparing respository with session variables
		$Repository= $this->getDoctrine()->getRepository('WamadminCoreBundle:ClientUser')
			->findClientUsers(
				$session->get($FilterSession),
				$session->get($OrderSession)
			);
			
		$ListPager = $this->get('ListPager');
		$ListPager->setRepositoryAndMaxResults(	$Repository, 15);
		
		//rendering template
		return $this->render('WamadminAdminBundle:ClientUser:List.html.twig',array(
        	'Repository'=>$ListPager->getResults(), 
        	'ListPager'=>$ListPager->getPagerData(),
        	'FilterSession'=>$FilterSession, 
        	'OrderSession'=>$OrderSession, 
        	'FilterForm'=>$FilterForm->createView(),
        ));
	}
			
    public function NewAction(Request $request)
    {
		$Repository = new ClientUser();
    	$Form = $this->createForm(new ClientUserForm(), $Repository );
		
		if ($request->isMethod('POST')):
			$Form->bind($request);
			$ProcessResponse=$this->ProcessForm($Form, $Repository, true);//processing form POST
			if($ProcessResponse===true): 
				return $this->redirect($this->generateUrl('wamadmin_ClientUser_Edit', array('id' => $Repository->getId()) ));
			endif;
		endif;
		
		//rendering template
		return $this->render('WamadminAdminBundle:ClientUser:New.html.twig',array( 
        	'form'=>$Form->createView(),
        ));
		
    }
	
    public function EditAction(Request $request, $id)
    {		
    	$Repository = $this->getDoctrine()->getRepository('WamadminCoreBundle:ClientUser')->find($id);
    	
		$Form = $this->createForm(new ClientUserEditForm(), $Repository );
		
		if ($request->isMethod('POST')):
			$Form->bind($request);
			$this->ProcessForm($Form, $Repository, false); //processing form POST
		endif;
		
		//rendering template
		return $this->render('WamadminAdminBundle:ClientUser:Edit.html.twig',array( 
        	'form'=>$Form->createView(),
        	'Repository'=>$Repository,
        ));
			
    }
	
	public function DeleteAction(Request $request)
    {
		$Repository = $this->getDoctrine()->getRepository('WamadminCoreBundle:ClientUser')->find( $request->query->get('id'));
		
		$em = $this->getDoctrine()->getManager();
		$em->remove($Repository);
		$em->flush();
		
		$this->get('session')->getFlashBag()->add('success', 'El cliente fue eliminado con éxito');
		
		return $this->redirect($this->generateUrl('ClientUser_List'));
	}
	
	//PROCESS FORM POST
    private function ProcessForm($Form, $Repository, $IsNew)
    {
		if ($Form->isValid()):
		
			if($IsNew===true):
				$data = $Form->getData();
				$encoder = $this->get('security.encoder_factory')->getEncoder($Repository);//password encoder
				$Repository->setPassword( $encoder->encodePassword( $data->getPassword(), $Repository->getSalt()) );
			endif;
				
			$em = $this->getDoctrine()->getManager();
			$em->persist($Repository); $em->flush();
			
			$this->get('session')->getFlashBag()->add('success', 'El cliente fue guardado con éxito');
			return true;
		else:	
			foreach($Form->getErrors() as $error):
				$this->get('session')->getFlashBag()->add('error', $error->getMessage() );
			endforeach;
		endif;
	}
	
	//PROCESS FILTER POST
    private function ProcessFilter($FilterForm, $FilterSession)
    {				
		$session = $this->getRequest()->getSession();
		if ($FilterForm->isValid())
		{
			$session->set($FilterSession, $FilterForm->getData()); //set filter session
			$this->get('session')->getFlashBag()->add('success', 'Se ha filtrado el listado exitosamente');
		}
		else
		{			
			foreach($FilterForm->getErrors() as $error):
				$this->get('session')->getFlashBag()->add('error', $error->getMessage() );
			endforeach;
		}
	}
}
