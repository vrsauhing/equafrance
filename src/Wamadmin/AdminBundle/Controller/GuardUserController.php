<?php

namespace Wamadmin\AdminBundle\Controller;

//LIBRARIES
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;

//ENTITIES
use Wamadmin\CoreBundle\Entity\GuardUser;

//FORMS
use Wamadmin\CoreBundle\Form\_GuardUser\GuardUserForm;
use Wamadmin\CoreBundle\Form\_GuardUser\GuardUserEditForm;
use Wamadmin\CoreBundle\Form\_GuardUser\GuardUserFilterForm;

class GuardUserController extends Controller
{
    public function ListAction(Request $request)
    {
		//init list session
		$session = $request->getSession();
		$OrderSession='OrderGuardUserList'; $FilterSession='FilterGuardUserList';
		
		//preparing filter form 
    	$FilterForm = $this->createForm(new GuardUserFilterForm(), $session->get($FilterSession) );
				
		//check if list has filter form
		if ($request->isMethod('POST')):
			$FilterForm->bind($request);
			$this->ProcessFilter($FilterForm, $FilterSession);//processing filter POST
		endif;
		
		//preparing respository with session variables
		$Repository= $this->getDoctrine()->getRepository('WamadminCoreBundle:GuardUser')
			->findGuardUsers(
				$session->get($FilterSession),
				$session->get($OrderSession)
			);
			
		$ListPager = $this->get('ListPager');
		$ListPager->setRepositoryAndMaxResults(	$Repository, 15);
		
		//rendering template
		return $this->render('WamadminAdminBundle:GuardUser:List.html.twig',array(
        	'Repository'=>$ListPager->getResults(), 
        	'ListPager'=>$ListPager->getPagerData(),
        	'FilterSession'=>$FilterSession, 
        	'OrderSession'=>$OrderSession, 
        	'FilterForm'=>$FilterForm->createView(),
        ));
	}
			
    public function NewAction(Request $request)
    {
		$Repository = new GuardUser();
    	$Form = $this->createForm(new GuardUserForm(), $Repository );
		
		if ($request->isMethod('POST')):
			$Form->bind($request);
			$ProcessResponse=$this->ProcessForm($Form, $Repository, true);//processing form POST
			if($ProcessResponse===true): 
				return $this->redirect($this->generateUrl('wamadmin_GuardUser_Edit', array('id' => $Repository->getId()) ));
			endif;
		endif;
		
		//rendering template
		return $this->render('WamadminAdminBundle:GuardUser:New.html.twig',array( 
        	'form'=>$Form->createView(),
        ));
		
    }
	
    public function EditAction(Request $request, $id)
    {		
    	$Repository = $this->getDoctrine()->getRepository('WamadminCoreBundle:GuardUser')->find($id);
    	
		$Form = $this->createForm(new GuardUserEditForm(), $Repository );
		
		if ($request->isMethod('POST')):
			$Form->bind($request);
			$this->ProcessForm($Form, $Repository, false); //processing form POST
		endif;
		
		//rendering template
		return $this->render('WamadminAdminBundle:GuardUser:Edit.html.twig',array( 
        	'form'=>$Form->createView(),
        	'Repository'=>$Repository,
        ));
			
    }
	
	public function DeleteAction(Request $request)
    {
		$Repository = $this->getDoctrine()->getRepository('WamadminCoreBundle:GuardUser')->find( $request->query->get('id'));
		
		$em = $this->getDoctrine()->getManager();
		$em->remove($Repository);
		$em->flush();
		
		$this->get('session')->getFlashBag()->add('success', 'El usuario fue eliminado con éxito');
		
		return $this->redirect($this->generateUrl('GuardUser_List'));
	}
	
	//PROCESS FORM POST
    private function ProcessForm($Form, $Repository, $IsNew)
    {
		if ($Form->isValid()):
		
			if($IsNew===true):
				$data = $Form->getData();
				$encoder = $this->get('security.encoder_factory')->getEncoder($Repository);//password encoder
				$Repository->setPassword( $encoder->encodePassword( $data->getPassword(), $Repository->getSalt()) );
			endif;
				
			$em = $this->getDoctrine()->getManager();
			$em->persist($Repository); $em->flush();
			
			$this->get('session')->getFlashBag()->add('success', 'El usuario fue guardado con éxito');
			return true;
		else:	
			foreach($Form->getErrors() as $error):
				$this->get('session')->getFlashBag()->add('error', $error->getMessage() );
			endforeach;
		endif;
	}
	
	//PROCESS FILTER POST
    private function ProcessFilter($FilterForm, $FilterSession)
    {				
		$session = $this->getRequest()->getSession();
		if ($FilterForm->isValid())
		{
			$session->set($FilterSession, $FilterForm->getData()); //set filter session
			$this->get('session')->getFlashBag()->add('success', 'Se ha filtrado el listado exitosamente');
		}
		else
		{			
			foreach($FilterForm->getErrors() as $error):
				$this->get('session')->getFlashBag()->add('error', $error->getMessage() );
			endforeach;
		}
	}
}
