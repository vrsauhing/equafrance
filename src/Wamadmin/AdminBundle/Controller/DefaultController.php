<?php

namespace Wamadmin\AdminBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class DefaultController extends Controller
{
    public function indexAction()
    {
        return $this->render('WamadminAdminBundle:Default:index.html.twig');
    }
}
