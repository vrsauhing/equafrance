<?php 
namespace Application\CoreBundle\Form\DataTransformer;

use Symfony\Component\Form\DataTransformerInterface;
use Symfony\Component\Form\Exception\TransformationFailedException;
use Doctrine\Common\Persistence\ObjectManager;
use Application\CoreBundle\Entity\Client;

class ClientToObjectTransformer implements DataTransformerInterface
{
    /**
     * @var ObjectManager
     */
    private $om;

    /**
     * @param ObjectManager $om
     */
    public function __construct(ObjectManager $om)
    {
        $this->om = $om;
    }

    /**
     * Transforms an object (Client) to a string (id).
     *
     * @param  Client|null $Client
     * @return string
     */
    public function transform($Client)
    {
        if (null === $Client) {
            return "";
        }

        return $Client->getLastname().', '.$Client->getName();
    }

    /**
     * Transforms a string (name) to an object (Client).
     *
     * @param  string $name
     *
     * @return Client|null
     *
     * @throws TransformationFailedException if object (Client) is not found.
     */
    public function reverseTransform($name)
    {
        if (!$name):
			throw new TransformationFailedException(sprintf('An Client with Id "%s" does not exist!',$name	));
		else:
			$name= explode(', ', $name);
		endif;
		
		if(count($name)==2):
			$Client = $this->om->getRepository('ApplicationCoreBundle:Client')->findOneBy(array('lastname' => $name[0], 'name'=>$name[1]));

			if (null === $Client):
				throw new TransformationFailedException(sprintf('An Client with Id "%s" does not exist!',$name	));
			endif;

			return $Client;
		else:
			throw new TransformationFailedException(sprintf('An Client with Id "%s" does not exist!',$name	));
		endif;
		
    }
}