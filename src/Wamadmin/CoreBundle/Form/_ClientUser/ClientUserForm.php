<?php

namespace Wamadmin\CoreBundle\Form\_ClientUser;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class ClientUserForm extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('id', 'hidden');
		$builder->add('name', 'text', array(
            'error_bubbling'=>true
        ));
		$builder->add('lastname', 'text',  array(
            'error_bubbling'=>true
        ));
		$builder->add('email', 'email',  array(
            'error_bubbling'=>true
        ));
		$builder->add('username', 'text',  array(
            'error_bubbling'=>true
        ));
		$builder->add('password', 'repeated', array(
			'type' => 'password',
            'error_bubbling'=>true,
			'invalid_message' => 'Las claves deben coincidir',
		));
		$builder->add('phone', 'text',  array(
            'error_bubbling'=>true
        ));
		$builder->add('celphone', 'text',  array(
            'error_bubbling'=>true
        ));
        $builder->add('groups', 'entity', array(
            'class' => 'WamadminCoreBundle:ClientGroup',
            'property' => 'name',
            'expanded' => false,
            'multiple' => true,
            'error_bubbling'=>true,
        ));
		$builder->add('db_connect', 'text',  array(
            'error_bubbling'=>true
        ));
		$builder->add('is_active', 'choice', array(
			'choices'   => array(1 => 'Activo', 0 => 'Inactivo'),
		));
    }

    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'Wamadmin\CoreBundle\Entity\ClientUser',
            'cascade_validation' => true,
        ));
    }

    public function getName()
    {
        return 'ClientUser';
    }
}