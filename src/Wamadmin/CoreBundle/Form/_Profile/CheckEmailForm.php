<?php

namespace Wamadmin\CoreBundle\Form\_Profile;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Validator\Constraints\NotBlank;
use Symfony\Component\Validator\Constraints\Email;

class CheckEmailForm extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
		$builder->add('email', 'email', array(
			'constraints' => array(
				new NotBlank(array('message'=>'Ingrese su correo')), 
				new Email(array('message'=>'El correo no es valido')), 
			),
            'error_bubbling'=>true
		));
    }

    public function getName()
    {
        return 'GuardUser';
    }
}