<?php

namespace Wamadmin\CoreBundle\Form\_Profile;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Validator\Constraints\NotBlank;

class RecoverPasswordForm extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
		$builder->add('new_password', 'repeated', array(
			'constraints' => array(
				new NotBlank(array('message'=>'Ingrese una nueva clave')), 
			),
			'type' => 'password',
			'invalid_message' => 'Las claves deben coincidir.',
            'error_bubbling'=>true
		));
    }

    public function getName()
    {
        return 'GuardUser';
    }
}