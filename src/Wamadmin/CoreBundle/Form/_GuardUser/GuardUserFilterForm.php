<?php

namespace Wamadmin\CoreBundle\Form\_GuardUser;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;

class GuardUserFilterForm extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
		$builder->add('email', 'email',  array(
            'label'=>'Email',
            'required'=>false,
        ));
		$builder->add('username', 'text',  array(
            'label'=>'Usuario',
            'required'=>false,
        ));
		$builder->add('groups', 'choice', array(
			'choices'   => array(1 => 'Administrador', 2 => 'Asistente'),
			'empty_value'   => '-- Todos los grupos -- ',
            'label'=>'Grupo',
            'required'=>false,
		));
		$builder->add('is_active', 'choice', array(
			'choices'   => array(1 => 'Activo', 'false' => 'Inactivo'),
			'empty_value'   => '-- Todos los estados -- ',
            'label'=>'Esta activo',
            'required'=>false,
		));
    }
	
    public function getName()
    {
        return 'GuardUser';
    }
}