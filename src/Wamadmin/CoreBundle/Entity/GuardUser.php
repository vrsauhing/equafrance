<?php
// src/Wamadmin/CoreBundle/Entity/GuardUser.php
namespace Wamadmin\CoreBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Component\Security\Core\User\AdvancedUserInterface;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;

/**
 * @ORM\Entity(repositoryClass="Wamadmin\CoreBundle\Entity\GuardUserRepository")
 * @UniqueEntity(fields={"email"}, message="El 'email' ya ha sido ingresado")
 * @UniqueEntity(fields={"username"}, message="El 'nombre de usuario' ya ha sido ingresado")
 * @ORM\Table(name="GuardUser")
 */
 
class GuardUser implements AdvancedUserInterface, \Serializable
{
    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;
	
    /**
     * @Assert\NotBlank(message = "Ingrese el 'nombre' del usuario.")
     * @ORM\Column(type="string", length=255)
     */
    protected $name;

    /**
     * @Assert\NotBlank(message = "Ingrese el 'apellido' del usuario.")
     * @ORM\Column(type="string", length=255)
     */
    protected $lastname;

    /**
     * @Assert\NotBlank(message = "Ingrese el 'email' del usuario.")
     * @Assert\Email(message = "Ingrese un 'email' valido" )
     * @ORM\Column(type="string", length=100, unique=true )
     */
    protected $email;
	
    /**
     * @Assert\NotBlank(message = "Ingrese un 'nombre de usuario'.")
     * @ORM\Column(type="string", length=25, unique=true )
     */
    protected $username;
	
    /**
     * @ORM\Column(type="string", length=32 )
     */
    protected $salt;
	
    /**
     * @Assert\NotBlank(message = "Ingrese un 'clave de usuario'.")
     * @ORM\Column(type="string", length=40 )
     */
    protected $password;
	
    /**
     * @ORM\Column(name="is_active", type="boolean")
     */
    protected $is_active;
		
    /**
     * @ORM\Column(type="string", length=20,  nullable=true )
     */
    protected $phone;
	
    /**
     * @ORM\Column(type="string", length=20, nullable=true )
     */
    protected $celphone;
	
    /**
     * @Assert\Count(
     *      min = "1",
     *      max = "1",
     *      exactMessage = "Solo se puede asignar un grupo al usuario"
     * )
     * @ORM\ManyToMany(targetEntity="GuardGroup", inversedBy="users")
     */
    private $groups;
	
    public function __construct()
    {
        $this->is_active = true;
        $this->salt = md5(uniqid(null, true));
        $this->groups = new ArrayCollection();
    }
	
    /**
     * @inheritDoc
     */
    public function getUsername()
    {
        return $this->username;
    }

    /**
     * @inheritDoc
     */
    public function getSalt()
    {
        return $this->salt;
    }

    /**
     * @inheritDoc
     */
    public function getPassword()
    {
        return $this->password;
    }

    public function getRoles()
    {
        return $this->groups->toArray();
    }

    /**
     * @inheritDoc
     */
    public function eraseCredentials()
    {
    }

    /**
     * @inheritDoc
     */
    //public function equals(UserInterface $user)
    //{
       // return $this->id === $user->getId();
    //}

    /**
     * @see \Serializable::serialize()
     */
    public function serialize()
    {
        return serialize(array(
            $this->id,
        ));
    }

    /**
     * @see \Serializable::unserialize()
     */
    public function unserialize($serialized)
    {
        list (
            $this->id,
        ) = unserialize($serialized);
    }

    public function isAccountNonExpired()
    {
        return true;
    }

    public function isAccountNonLocked()
    {
        return true;
    }

    public function isCredentialsNonExpired()
    {
        return true;
    }
    
    public function isEnabled()
    {
        return $this->is_active;
    }

    /**
     * Set id
     *
     * @param integer $id
     * @return GuardUser
     */
    public function setId($id)
    {
        $this->id = $id;
    
        return $this;
    }
    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     * @return GuardUser
     */
    public function setName($name)
    {
        $this->name = $name;
    
        return $this;
    }

    /**
     * Get name
     *
     * @return string 
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set lastname
     *
     * @param string $lastname
     * @return GuardUser
     */
    public function setLastname($lastname)
    {
        $this->lastname = $lastname;
    
        return $this;
    }

    /**
     * Get lastname
     *
     * @return string 
     */
    public function getLastname()
    {
        return $this->lastname;
    }

    /**
     * Set email
     *
     * @param string $email
     * @return GuardUser
     */
    public function setEmail($email)
    {
        $this->email = $email;
    
        return $this;
    }

    /**
     * Get email
     *
     * @return string 
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * Set username
     *
     * @param string $username
     * @return GuardUser
     */
    public function setUsername($username)
    {
        $this->username = $username;
    
        return $this;
    }

    /**
     * Set salt
     *
     * @param string $salt
     * @return GuardUser
     */
    public function setSalt($salt)
    {
        $this->salt = $salt;
    
        return $this;
    }

    /**
     * Set password
     *
     * @param string $password
     * @return GuardUser
     */
    public function setPassword($password)
    {
        $this->password = $password;
    
        return $this;
    }

    /**
     * Set is_active
     *
     * @param boolean $isActive
     * @return GuardUser
     */
    public function setIsActive($isActive)
    {
        $this->is_active = $isActive;
    
        return $this;
    }

    /**
     * Get is_active
     *
     * @return boolean 
     */
    public function getIsActive()
    {
        return $this->is_active;
    }
	

    /**
     * Set phone
     *
     * @param string $phone
     * @return GuardUser
     */
    public function setPhone($phone)
    {
        $this->phone = $phone;

        return $this;
    }

    /**
     * Get phone
     *
     * @return string 
     */
    public function getPhone()
    {
        return $this->phone;
    }

    /**
     * Set celphone
     *
     * @param string $celphone
     * @return GuardUser
     */
    public function setCelphone($celphone)
    {
        $this->celphone = $celphone;

        return $this;
    }

    /**
     * Get celphone
     *
     * @return string 
     */
    public function getCelphone()
    {
        return $this->celphone;
    }


    /**
     * Add groups
     *
     * @param \Wamadmin\CoreBundle\Entity\GuardGroup $groups
     * @return GuardUser
     */
    public function addGroup(\Wamadmin\CoreBundle\Entity\GuardGroup $groups)
    {
        $this->groups[] = $groups;

        return $this;
    }

    /**
     * Remove groups
     *
     * @param \Wamadmin\CoreBundle\Entity\GuardGroup $groups
     */
    public function removeGroup(\Wamadmin\CoreBundle\Entity\GuardGroup $groups)
    {
        $this->groups->removeElement($groups);
    }

    /**
     * Get groups
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getGroups()
    {
        return $this->groups;
    }
}
