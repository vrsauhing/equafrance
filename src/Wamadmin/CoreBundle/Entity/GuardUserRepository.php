<?php

namespace Wamadmin\CoreBundle\Entity;

use Doctrine\ORM\EntityRepository;


class GuardUserRepository extends EntityRepository
{
    public function findGuardUsers($FilterArray, $OrderArray)
    {		
		//INIT FILTER VARIABLES FOR QUERY
		$FilterQuery='WHERE p.username IS NOT null';
		if($FilterArray):
			foreach($FilterArray as  $key=>$value)
			{
				if($value):
					switch($key):
						case 'username':
							$FilterQuery=$FilterQuery." AND p.username LIKE '%".$value."%'";
							break;
						case 'email':
							$FilterQuery=$FilterQuery." AND p.email LIKE '%".$value."%'";
							break;
						case 'groups':
							$FilterQuery=$FilterQuery." AND c.id =".$value."";
							break;
						case 'is_active':
							$FilterQuery=$FilterQuery." AND p.is_active =".$value."";
							break;
					endswitch;
				endif;
			}
		endif;
		
		//INIT ORDER VARIABLES FOR QUERY
		$OrderQuery='p.name ASC';
		if($OrderArray):
			$OrderQuery='p.'.$OrderArray['Row'].' '.$OrderArray['Order'];
			if($OrderArray['Row']=='groups'):
				$OrderQuery='c.name '.$OrderArray['Order'];
			endif;
		endif;
		
		//RETURNING QUERY WITH ENTERED VARIABLES
        return $this->getEntityManager()
            ->createQuery(
                'SELECT p FROM WamadminCoreBundle:GuardUser p JOIN p.groups c '.$FilterQuery.' ORDER BY '.$OrderQuery
            );
	}
}
