<?php

namespace Wamglobal\ServiceBundle\Controller;

//LIBRARIES
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Security\Core\SecurityContext;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\JsonResponse;

class GlobalController extends Controller
{
    public function AjaxClientsAction(Request $request)
    {
		$response = new JsonResponse();
        $Clients = array(); 
				
		if($request->isXmlHttpRequest())
		{				
			$EntityManager = str_replace('"', '', json_encode($request->query->get('db')));
			$Repository= $this->getDoctrine()->getRepository('WamclientCoreBundle:Client', $EntityManager);
			
			$Results= $Repository->findAjaxClients(json_encode($request->query->get('term')))->getResult();
			
			foreach($Results as $Item):
				$Ci=$Item->getCi();
				if($Ci==''):$Ci='N.A.';endif;
				$Clients[] = array( 
					'name' =>$Item->getLastName().', '.$Item->getName(),
					'ci'=>$Ci,
					'id'=>$Item->getId(),
				);
			endforeach;
			
			return $response->setData(json_encode($Clients) );
		}		
	}
	
    public function AjaxServicesAction(Request $request)
    {
		$response = new JsonResponse();
        $Services = array(); 
				
		if($request->isXmlHttpRequest())
		{				
			$EntityManager = str_replace('"', '', json_encode($request->query->get('db')));
			$Repository= $this->getDoctrine()->getRepository('WamclientCoreBundle:Service', $EntityManager);
			
			$Results= $Repository->findAjaxServices(json_encode($request->query->get('term')))->getResult();
			
			foreach($Results as $Item):
				$Services[] = array( 
					'id' =>$Item->getId(),
					'name' =>$Item->getName(),
					'price'=>$Item->getPrice(),
				);
			endforeach;
			
			return $response->setData(json_encode($Services) );
		}		
	}
	
    public function OrderbyAction(Request $request, $NameSession, $Row)
    {
		//INIT SESSION VARIABLES	
		$session = $request->getSession();
		$OrderSession=$session->get($NameSession);
		
		//ORDER VARIABLES TO ORDER SESSION
		if($OrderSession['Row']==$Row)
		{
			$OrderToSet='ASC';
			
			if($OrderSession['Order']==$OrderToSet):
				$OrderToSet='DESC';
			endif;			
			
			$session->set($NameSession, array( 'Row'=>$Row, 'Order'=>$OrderToSet));
		}
		else
		{
			$session->set($NameSession, array( 'Row'=>$Row, 'Order'=>'ASC'));
		}	
		
		//RETURNING TO REFERER
        return $this->redirect( $request->headers->get('referer') );
    }

    public function ResetfilterAction(Request $request, $FilterName)
    {
		//INIT SESSION VARIABLES	
		$session = $request->getSession();
		$session->remove($FilterName);
		
		$this->get('session')->getFlashBag()->add('success', 'El filtro ha sido removido');
				
		//RETURNING TO REFERER
        return $this->redirect( $request->headers->get('referer') );
    }
}
