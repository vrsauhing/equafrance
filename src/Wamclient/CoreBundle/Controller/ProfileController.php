<?php

namespace Wamclient\CoreBundle\Controller;

//LIBRARIES
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Security\Core\SecurityContext;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\JsonResponse;

//ENTITIES
use Wamadmin\CoreBundle\Entity\GuardUser;

//FORMS
use Wamclient\CoreBundle\Form\_Profile\EditProfileForm;
use Wamclient\CoreBundle\Form\_Profile\ChangePasswordForm;
use Wamclient\CoreBundle\Form\_Profile\CheckEmailForm;
use Wamclient\CoreBundle\Form\_Profile\RecoverPasswordForm;

//SERVICES

class ProfileController extends Controller
{
    public function EditAction(Request $request)
    {
    	$Repository = $this->getDoctrine()->getRepository('WamadminCoreBundle:ClientUser')->find($this->getUser()->getId());
        $Form = $this->createForm(new EditProfileForm(), $Repository);
    	
		$GroupLayout='WamclientAdmin.html.twig';
		$Groups= $Repository->getGroups();
		foreach($Groups as $Group):
			switch($Group->getId()):
				case(2):
					$GroupLayout='WamclientAssistant.html.twig';
					break;
			endswitch;
		endforeach;		
		
		if ($request->isMethod('POST')):
			$Form->bind($request);

			if ($Form->isValid()):
				$em = $this->getDoctrine()->getManager();
				$em->persist($Repository);
				$em->flush();
				
				$this->get('session')->getFlashBag()->add('success', 'El perfil ha sido actualizado correctamente');
				
				return $this->redirect($this->generateUrl('Wamclient_profile_edit'));
			else:		
				foreach($Form->getErrors() as $error):
					$this->get('session')->getFlashBag()->add('error', $error->getMessage() );
				endforeach;
			endif;
		endif;
		
		return $this->render('WamclientCoreBundle:Security:EditProfile.html.twig', array(
			'form' => $Form->createView(), 'Repository'=>$Repository, 'GroupLayout' => $GroupLayout,
        ));
    }
		
    public function ChangePasswordAction(Request $request)
    {
    	$Repository = $this->getDoctrine()->getRepository('WamadminCoreBundle:ClientUser')->find($this->getUser()->getId());
        $Form = $this->createForm(new ChangePasswordForm());
		   	
		$GroupLayout='WamclientAdmin.html.twig';
		$Groups= $Repository->getGroups();
		foreach($Groups as $Group):
			switch($Group->getId()):
				case(2):
					$GroupLayout='WamclientAssistant.html.twig';
					break;
			endswitch;
		endforeach;		
		
		if ($request->isMethod('POST')):
			$Form->bind($request);
			if ($Form->isValid()):
				$data = $Form->getData();//recogemos la informacion del formulario
				
				$encoder = $this->get('security.encoder_factory')->getEncoder($Repository);//password encoder
				if($encoder->encodePassword( $data['password'], $Repository->getSalt()) != $Repository->getPassword()):
					$this->get('session')->getFlashBag()->add('error', 'La clave actual ingresada es incorrecta.' );
				else:
					$Repository->setPassword( $encoder->encodePassword( $data['new_password'], $Repository->getSalt()) );
					
					$em = $this->getDoctrine()->getManager();
					$em->persist($Repository);
					$em->flush();
					
					$this->get('session')->getFlashBag()->add('success', 'La clave ha sido cambiada correctamente');
					
					return $this->redirect($this->generateUrl('wamclient_profile_changepassword'));
				endif;
			else: 
				foreach($Form->getErrors() as $error):
					$this->get('session')->getFlashBag()->add('error', $error->getMessage() );
				endforeach;
			endif;
		endif;
		
		return $this->render('WamclientCoreBundle:Profile:ChangePassword.html.twig', array(
			'form' => $Form->createView(), 'GroupLayout' => $GroupLayout,
        ));
    }
		
    public function RecoverPasswordAction(Request $request)
    {
		$RecoverType='NoPost';
        $Form = $this->createForm(new CheckEmailForm());
		if ($request->isMethod('POST')):
			$Form->bind($request);

			if ($Form->isValid()):
				$data = $Form->getData();//recogemos la informacion del formulario
				$CheckEmail = $this->getDoctrine()->getRepository('WamadminCoreBundle:GuardUser')->findOneByEmail($data['email']);
				
				if($CheckEmail==null):
					$this->get('session')->getFlashBag()->add('error', 'Correo no registrado');
				else:					
					$Crypt = $this->get('Crypt');
					$Crypt->init("W4m$%7@");		
					
					$Token=strtotime(date('YmdHis')).'-'.$CheckEmail->getId();
					$EncodeToken = urlencode($Crypt->encrypt($Token));
					$UrlToken='http://wamstore.rai'.$this->generateUrl('global_setpassword').'?token='.$EncodeToken;
					
					$message = \Swift_Message::newInstance()
						->setSubject('Recuperacion de clave')
						->setFrom('vrsauhing@webandmkt.com')
						->setTo($CheckEmail->getEmail())
						->setBody(
							$this->renderView('WamclientCoreBundle::_RecoverPasswordTemplate.html.twig',	array('UrlToken' => $UrlToken)	)
						);
					$this->get('mailer')->send($message);
					
					$this->get('session')->getFlashBag()->add('success', 'Por favor revise su correo!');
					$RecoverType='Post';
				endif;				
			else: //SI EL FORMULARIO NO ES VALIDO
				foreach($Form->getErrors() as $error):
					$this->get('session')->getFlashBag()->add('error', $error->getMessage());
				endforeach;
			endif;
		endif;
		
		return $this->render('WamclientCoreBundle::RecoverPassword.html.twig', array(
			'form' => $Form->createView(),
			'RecoverType' => $RecoverType,
		));
    }
	
    public function SetPasswordAction(Request $request)
    {
        $Form = $this->createForm(new RecoverPasswordForm());
		$RecoverType='Show_Enable';
		
		$Crypt = $this->get('Crypt');
		$Crypt->init("W4m$%7@");		
				
		$EncodeToken = $request->query->get('token');
		$DecodeToken = explode('-',$Crypt->decrypt($EncodeToken));
		
		if($EncodeToken):
			if($DecodeToken[0]):
				$Enable=strtotime(date('YmdHis'))-$DecodeToken[0];
				if($Enable<36000):						
					$Form = $this->createForm(new RecoverPasswordForm());
					
					if ($request->isMethod('POST')):
						$Form->bind($request);

						if ($Form->isValid()):
						
							$data = $Form->getData();//recogemos la informacion del formulario
							
							$Repository = $this->getDoctrine()->getRepository('WamadminCoreBundle:GuardUser')->find($DecodeToken[1]);
							$encoder = $this->get('security.encoder_factory')->getEncoder($Repository);//password encoder
							
							$Repository->setPassword( $encoder->encodePassword( $data['new_password'], $Repository->getSalt()) );
								
							$em = $this->getDoctrine()->getManager();
							$em->persist($Repository);
							$em->flush();
								
							$this->get('session')->getFlashBag()->add('success', 'La clave ha sido cambiada correctamente');
							return $this->redirect($this->generateUrl('login'));
							
						else: //SI EL FORMULARIO NO ES VALIDO
							foreach($Form->getErrors() as $error):
								$this->get('session')->getFlashBag()->add('error', $error->getMessage());
							endforeach;
						endif;
						
					endif;
					
				else:
					$RecoverType='Show_NotEnable';
				endif;
			else:
				$RecoverType='Show_DecodingError';
			endif;
		endif;
		
		
		
		return $this->render('WamclientCoreBundle::SetPassword.html.twig', array(
			'form' => $Form->createView(),
			'RecoverType'=>$RecoverType,
			'EncodeToken'=>urlencode($EncodeToken),
		));
	}
}
