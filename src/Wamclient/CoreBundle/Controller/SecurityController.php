<?php

namespace Wamclient\CoreBundle\Controller;

//LIBRARIES
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Security\Core\SecurityContext;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;

//ENTITIES
use Wamadmin\CoreBundle\Entity\ClientUser;

class SecurityController extends Controller
{
    public function loginAction()
    {
        $request = $this->getRequest(); $session = $request->getSession(); $ErrorMessage=null;

        if ($request->attributes->has(SecurityContext::AUTHENTICATION_ERROR)):
            $error = $request->attributes->get( SecurityContext::AUTHENTICATION_ERROR  );
		else:
            $error = $session->get(SecurityContext::AUTHENTICATION_ERROR);
            $session->remove(SecurityContext::AUTHENTICATION_ERROR);
        endif;

        if($error)
        {
	        if($error->getMessage()=='Bad credentials'):
	        	$ErrorMessage='Credenciales incorrectas.';
	        endif;

	        if($error->getMessage()=='User account is disabled.'):
	        	$ErrorMessage='La cuenta esta inactiva.';
	        endif;
	    }

        return $this->render( 'WamclientCoreBundle:Security:Login.html.twig', array(
            'ErrorMessage' => $ErrorMessage,
		));
    }
	
    public function ChooseAppAction(Request $request)
    {
		if(!$this->getUser()):
			return $this->redirect($this->generateUrl('wamclient_login'));
		else:
			$Repository = $this->getDoctrine()->getRepository('WamadminCoreBundle:ClientUser')->find($this->getUser()->getId());
			
			return $this->render('WamclientCoreBundle:Security:ChooseApp.html.twig', array(
				'Repository'=>$Repository,
			));
		endif;
    }
	
    public function logoutAction()
    {
    }
}
