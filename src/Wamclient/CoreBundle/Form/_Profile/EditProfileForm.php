<?php

namespace Wamclient\CoreBundle\Form\_Profile;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class EditProfileForm extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('id', 'hidden');
		$builder->add('name', 'text', array(
            'error_bubbling'=>true
        ));
		$builder->add('lastname', 'text',  array(
            'error_bubbling'=>true
        ));
		$builder->add('email', 'email',  array(
            'error_bubbling'=>true
        ));
		$builder->add('username', 'text',  array(
            'error_bubbling'=>true
        ));
		$builder->add('phone', 'text',  array(
            'error_bubbling'=>true
        ));
		$builder->add('celphone', 'text',  array(
            'error_bubbling'=>true
        ));
    }

    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'Wamadmin\CoreBundle\Entity\ClientUser',
            'cascade_validation' => true,
        ));
    }

    public function getName()
    {
        return 'ClientUser';
    }
}