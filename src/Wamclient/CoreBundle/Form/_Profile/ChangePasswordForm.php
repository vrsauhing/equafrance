<?php

namespace Wamclient\CoreBundle\Form\_Profile;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Validator\Constraints\NotBlank;

class ChangePasswordForm extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
		$builder->add('password', 'password', array(
			'constraints' => array(
				new NotBlank(array('message'=>'Ingrese su clave actual')), 
			),
            'error_bubbling'=>true
		));
		$builder->add('new_password', 'repeated', array(
			'constraints' => array(
				new NotBlank(array('message'=>'Ingrese una nueva clave')), 
			),
			'type' => 'password',
			'invalid_message' => 'Las claves deben coincidir.',
            'error_bubbling'=>true
		));
    }

    public function getName()
    {
        return 'ClientUser';
    }
}