<?php

namespace Wamclient\CoreBundle\Form\_Statistic;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;


class ServiceYearlyForm extends AbstractType
{
    private $YearArray;
	
    public function __construct(array $YearArray)
    {
        $this->YearArray = array();
		if(count($YearArray)>0):
			foreach($YearArray as $Year):
				$this->YearArray[$Year['year']]=$Year['year'];
			endforeach;
		else:
			$this->YearArray[0]='No data';
		endif;
    }
		
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
		$builder->add('year_selector', 'choice', array(
			'choices'   => $this->YearArray,
            'required'=>false,
			'empty_value' => false,
		));
    }

    public function getName()
    {
        return 'StatisticService';
    }
}