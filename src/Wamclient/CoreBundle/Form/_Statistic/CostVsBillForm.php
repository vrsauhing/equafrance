<?php

namespace Wamclient\CoreBundle\Form\_Statistic;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;


class CostVsBillForm extends AbstractType
{
    private $MonthArray;
	
    public function __construct(array $MonthArray)
    {
        $this->MonthArray = array();
		
		if(count($MonthArray['cost'])>0 || count($MonthArray['bill'])>0 ):
			$spn_month= array('0', 'Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo', 'Junio', 'Julio', 'Agosto', 'Septiembre', 'Octubre', 'Noviembre', 'Diciembre');
			foreach($MonthArray['bill'] as $Month):
				$this->MonthArray[$Month['year'].'_'.$Month['month']]=$spn_month[$Month['month']].' '.$Month['year'];
			endforeach;
			foreach($MonthArray['cost'] as $Month):
				$this->MonthArray[$Month['year'].'_'.$Month['month']]=$spn_month[$Month['month']].' '.$Month['year'];
			endforeach;
		else:
			$this->MonthArray[0]='No data';
		endif;
    }
	
    public function buildForm(FormBuilderInterface $builder, array $options)
    {	
		$builder->add('month_selector', 'choice', array(
			'choices'   => $this->MonthArray,
            'required'=>false,
			'empty_value' => false,
		));
    }

    public function getName()
    {
        return 'StatisticCost';
    }
}