<?php

namespace Wamclient\CoreBundle\Form\_Statistic;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;


class CostDailyForm extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
		$month= array('0', 'Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo', 'Junio', 'Julio', 'Agosto', 'Septiembre', 'Octubre', 'Noviembre', 'Diciembre');
		$ThisMonth='Este mes: '.$month[date('n')];
		$LastMonth='El Mes Pasado: '.$month[date('n')-1];
	
		$builder->add('fast_date_selector', 'choice', array(
            'label'=>'Fechas Rápidas',
			'choices'   => array(1 => 'Últimos 7 días', 2 => 'Últimos 15 días', 3 => 'Últimos 30 días', 4 => $ThisMonth, 5 => $LastMonth ),
            'required'=>false,
			'empty_value' => false,
		));
    }

    public function getName()
    {
        return 'StatisticCost';
    }
}