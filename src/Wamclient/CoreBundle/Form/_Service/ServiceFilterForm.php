<?php

namespace Wamclient\CoreBundle\Form\_Service;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;


class ServiceFilterForm extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
		$builder->add('name', 'text',  array(
            'label'=>'Nombre',
            'required'=>false,
        ));
		$builder->add('price', 'money', array(
            'label'=>'Precio',
            'required'=>false,
            'currency'=>'USD',
            'precision'=>2,
		));
		$builder->add('status', 'choice', array(
			'choices'   => array(1 => 'Inactivo', 2=> 'Activo'),
            'required'=>false,
            'label'=>'Estado',
			'empty_value'   => '-- Todos los estados -- ',
		));
    }

    public function getName()
    {
        return 'Service';
    }
}