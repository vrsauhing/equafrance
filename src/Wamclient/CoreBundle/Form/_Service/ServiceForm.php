<?php

namespace Wamclient\CoreBundle\Form\_Service;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class ServiceForm extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('id', 'hidden');
		$builder->add('name', 'text', array(
            'error_bubbling'=>true,
        ));
		$builder->add('price', 'money', array(
            'error_bubbling'=>true,
            'currency'=>'USD',
            'precision'=>2,
			'invalid_message' => "El 'precio' ingresado es incorrecto",
		));
		$builder->add('status', 'choice', array(
			'choices'   => array(2=> 'Activo', 1 => 'Inactivo'),
		));
    }

    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'Wamclient\CoreBundle\Entity\Service',
        ));
    }

    public function getName()
    {
        return 'Service';
    }
}