<?php

namespace Wamclient\CoreBundle\Form\_Cost;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

use Doctrine\ORM\EntityManager;
use Wamclient\CoreBundle\Form\FormType\ClientType;

class CostForm extends AbstractType
{
    private $em;
	
    public function __construct(EntityManager $em)
    {
        $this->em = $em;
    }
	
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('id', 'hidden');
		$builder->add('cost_date', 'text', array(
			'attr'=> array('class'=>'datepicker'),
            'error_bubbling'=>true
		));
		$builder->add('provider', 'text', array(
            'error_bubbling'=>true,
        ));
        $builder->add('description', 'textarea', array(
			'attr'=> array('rows'=> 5,'class'=>'ckeditor'),
            'error_bubbling'=>true,
		));
		$builder->add('subtotal', 'money', array(
            'error_bubbling'=>true,
            'currency'=>'USD',
            'precision'=>2,
			'invalid_message' => "El 'subtotal' ingresado es incorrecto",
		));
		$builder->add('iva', 'money', array(
            'error_bubbling'=>true,
            'currency'=>'USD',
            'precision'=>2,
			'invalid_message' => "El 'IVA' ingresado es incorrecto",
		));
		$builder->add('total', 'money', array(
            'error_bubbling'=>true,
            'currency'=>'USD',
            'precision'=>2,
			'invalid_message' => "El 'total' ingresado es incorrecto",
		));
		$builder->add('locked', 'choice', array(
			'choices'   => array(1 => 'Si', 2=> 'No'),
		));
    }

    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'Wamclient\CoreBundle\Entity\Cost',
        ));
    }

    public function getName()
    {
        return 'Cost';
    }
}