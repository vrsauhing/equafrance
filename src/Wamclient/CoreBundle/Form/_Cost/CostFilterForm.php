<?php

namespace Wamclient\CoreBundle\Form\_Cost;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;


class CostFilterForm extends AbstractType
{
    private $WamUsers;
	
    public function __construct($WamUsers)
    {
        $this->WamUsers = $WamUsers;
    }
	
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
		$builder->add('provider', 'text',  array(
            'label'=>'Proveedor',
            'required'=>false,
        ));
		$builder->add('created_at_begin', 'text', array(
            'label'=>'Fecha de Inicio',
			'attr'=> array('class'=>'datepicker'),
            'required'=>false,
		));
		$builder->add('created_at_end', 'text', array(
            'label'=>'Fecha de Fin',
			'attr'=> array('class'=>'datepicker'),
            'required'=>false,
		));
		$builder->add('canceled', 'choice', array(
			'choices'   => array(1 => 'Vigente', 2=> 'Anulado'),
			'empty_value' => '-- Escoja una opción --',
            'label'=>'Estado?',
            'required'=>false,
		));
		$builder->add('locked', 'choice', array(
			'choices'   => array(1 => 'Si', 2=> 'No'),
			'empty_value' => '-- Escoja una opción --',
            'label'=>'Editable?',
            'required'=>false,
		));
		$builder->add('wam_user', 'choice', array(
			'choices'   => $this->WamUsers,
			'empty_value' => '-- Escoja un usuario --',
            'label'=>'Usuario WAM',
            'required'=>false,
		));
    }

    public function getName()
    {
        return 'Cost';
    }
}