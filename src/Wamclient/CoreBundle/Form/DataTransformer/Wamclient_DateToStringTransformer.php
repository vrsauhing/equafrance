<?php 
namespace Application\CoreBundle\Form\DataTransformer;

use Symfony\Component\Form\DataTransformerInterface;
use Symfony\Component\Form\Exception\TransformationFailedException;
use Doctrine\Common\Persistence\ObjectManager;
use Application\CoreBundle\Entity\Appointment;

class DateToStringTransformer implements DataTransformerInterface
{
    /**
     * @var ObjectManager
     */
    private $om;

    /**
     * @param ObjectManager $om
     */
    public function __construct(ObjectManager $om)
    {
        $this->om = $om;
    }

    /**
     * Transforms an object (Date) to a string (stringdate).
     *
     * @param  Date|null $Date
     * @return string
     */
    public function transform($Date)
    {
        if (null === $Date) {
            return "";
        }
		
		$stringdate = date_create_from_format('Y-m-d', $Date);
		$result = $stringdate->format('Y-m-d');
		return $result;
    }

    /**
     * Transforms a string (stringdate) to an object (Date).
     *
     * @param  string $stringdate
     *
     * @return Date|null
     *
     * @throws TransformationFailedException if object (Date) is not found.
     */
    public function reverseTransform($stringdate)
    {
		
        if (!$stringdate):
            return null;
		endif;

		$Date = date_create_from_format('Y-m-d', $stringdate);
		
        if (null === $Date) {
            throw new TransformationFailedException(sprintf(
                'An Patient with Id "%s" does not exist!',
                $stringdate
            ));
        }
        return $Date;
    }
}