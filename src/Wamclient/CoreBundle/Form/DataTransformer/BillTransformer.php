<?php 
namespace Wamclient\CoreBundle\Form\DataTransformer;

use Symfony\Component\Form\DataTransformerInterface;
use Symfony\Component\Form\Exception\TransformationFailedException;

use Doctrine\ORM\EntityManager;
use Wamclient\CoreBundle\Entity\Bill;

class BillTransformer implements DataTransformerInterface
{
    /**
     * @var EntityManager
     */
    private $em;

    /**
     * @param EntityManager $em
     */
    public function __construct(EntityManager $em)
    {
        $this->em = $em;
    }

    /**
     * Transforms an object (Bill) to a string (id).
     *
     * @param  Bill|null $Bill
     * @return string
     */
    public function transform($Bill)
    {
        if (null === $Bill) {
            return null;
        }

        return $Bill->getId();
    }

    /**
     * Transforms a string (name) to an object (Bill).
     *
     * @param  string $name
     *
     * @return Bill|null
     *
     * @throws TransformationFailedException if object (Bill) is not found.
     */
    public function reverseTransform($id)
    {
        if (!$id):
            return null;
		endif;
		
		$Bill = $this->em->getRepository('WamclientCoreBundle:Bill')->findOneBy(array('id' => $id));

		if (null === $Bill):
			throw new TransformationFailedException(sprintf('An Bill with Id "%s" does not exist!',$id	));
		endif;

		return $Bill;
		
    }
}