<?php 
namespace Wamclient\CoreBundle\Form\DataTransformer;

use Symfony\Component\Form\DataTransformerInterface;
use Symfony\Component\Form\Exception\TransformationFailedException;

use Doctrine\ORM\EntityManager;

class CiTransformer implements DataTransformerInterface
{
    /**
     * @var EntityManager
     */
    private $em;

    /**
     * @param EntityManager $em
     */
    public function __construct(EntityManager $em)
    {
        $this->em = $em;
    }

    /**
     * Transforms an string (Ci) to a string (ci).
     *
     * @param  Ci|null $Ci
     * @return string
     */
    public function transform($Ci)
    {
        if (null === $Ci || $Ci=='') :
            return "";
		else:
			if(!is_numeric($Ci)):
				throw new TransformationFailedException(sprintf('El "Ci" no es numérico.'));
			endif;
			
			if(strlen($Ci)==10 || strlen($Ci) ==13):
				return $Ci;
			else:
				throw new TransformationFailedException(sprintf('El "Ci" no contiene 10 o 13 dígitos.'));
			endif;
        endif;
    }

    /**
     * Transforms a string (ci) to an object (Ci).
     *
     * @param  string $name
     *
     * @return Ci|null
     *
     * @throws TransformationFailedException if object (Ci) is not found.
     */
    public function reverseTransform($Ci)
    {
        if (null === $Ci) :
            return "";
		else:
			if(!is_numeric($Ci)):
				throw new TransformationFailedException(sprintf('El "Ci" no es numérico.'));
			endif;
			
			if(strlen($Ci)==10 || strlen($Ci) ==13):
				return $Ci;
			else:
				throw new TransformationFailedException(sprintf('El "Ci" no contiene 10 o 13 dígitos.'));
			endif;
        endif;
		
    }
}