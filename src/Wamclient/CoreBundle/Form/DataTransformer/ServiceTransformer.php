<?php 
namespace Wamclient\CoreBundle\Form\DataTransformer;

use Symfony\Component\Form\DataTransformerInterface;
use Symfony\Component\Form\Exception\TransformationFailedException;

use Doctrine\ORM\EntityManager;
use WamService\CoreBundle\Entity\Service;

class ServiceTransformer implements DataTransformerInterface
{
    /**
     * @var EntityManager
     */
    private $em;

    /**
     * @param EntityManager $em
     */
    public function __construct(EntityManager $em)
    {
        $this->em = $em;
    }

    /**
     * Transforms an object (Service) to a integer (id).
     *
     * @param  Service|null $Service
     * @return integer
     */
    public function transform($Service)
    {
        if (null === $Service) {
            return "";
        }

        return $Service->getId();
    }

    /**
     * Transforms a integer (id) to an object (Service).
     *
     * @param  integer $id
     *
     * @return Service|null
     *
     * @throws TransformationFailedException if object (Service) is not found.
     */
    public function reverseTransform($id)
    {
        if (!$id):
			$id = 1;
		endif;
	
		$Service = $this->em->getRepository('WamclientCoreBundle:Service')->findOneBy(array('id' => $id));

		if (null === $Service):
			throw new TransformationFailedException(sprintf('An Service with Id "%s" does not exist!',$id	));
		endif;

		return $Service;		
    }
}