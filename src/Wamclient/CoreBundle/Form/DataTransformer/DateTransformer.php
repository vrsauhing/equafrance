<?php 
namespace Wamclient\CoreBundle\Form\DataTransformer;

use Symfony\Component\Form\DataTransformerInterface;
use Symfony\Component\Form\Exception\TransformationFailedException;

use Doctrine\ORM\EntityManager;

class DateTransformer implements DataTransformerInterface
{
    /**
     * @var EntityManager
     */
    private $em;

    /**
     * @param EntityManager $em
     */
    public function __construct(EntityManager $em)
    {
        $this->em = $em;
    }

    /**
     * Transforms an object (Date) to a string (stringdate).
     *
     * @param  Date|null $Date
     * @return string
     */
    public function transform($Date)
    {
        if (null === $Date) {
            return "";
        }
		
		$result = $Date->format('Y-m-d');
		return $result;
    }

    /**
     * Transforms a string (stringdate) to an object (Date).
     *
     * @param  string $stringdate
     *
     * @return Date|null
     *
     * @throws TransformationFailedException if object (Date) is not found.
     */
    public function reverseTransform($stringdate)
    {
		
        if (!$stringdate):
            return null;
		endif;

		$Date = date_create_from_format('Y-m-d', $stringdate);
		
        if (null === $Date) {
            throw new TransformationFailedException(sprintf(
                'An Patient with Id "%s" does not exist!',
                $stringdate
            ));
        }
        return $Date;
    }
}