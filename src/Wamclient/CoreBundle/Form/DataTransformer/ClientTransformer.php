<?php 
namespace Wamclient\CoreBundle\Form\DataTransformer;

use Symfony\Component\Form\DataTransformerInterface;
use Symfony\Component\Form\Exception\TransformationFailedException;

use Doctrine\ORM\EntityManager;
use Wamclient\CoreBundle\Entity\Client;

class ClientTransformer implements DataTransformerInterface
{
    /**
     * @var EntityManager
     */
    private $em;

    /**
     * @param EntityManager $em
     */
    public function __construct(EntityManager $em)
    {
        $this->em = $em;
    }

    /**
     * Transforms an object (Client) to a string (id).
     *
     * @param  Client|null $Client
     * @return string
     */
    public function transform($Client)
    {
        if (null === $Client) {
            return "";
        }

        return $Client->getId();
    }

    /**
     * Transforms a string (id) to an object (Client).
     *
     * @param  string $id
     *
     * @return Client|null
     *
     * @throws TransformationFailedException if object (Client) is not found.
     */
    public function reverseTransform($id)
    {
        if (!$id):
			throw new TransformationFailedException(sprintf('An Client with Id "%s" does not exist!',$id	));
		endif;
		
		$Client = $this->em->getRepository('WamclientCoreBundle:Client')->findOneBy(array('id' => $id));

		if (null === $Client):
			throw new TransformationFailedException(sprintf('An Client with Id "%s" does not exist!',$id	));
		endif;

		return $Client;
    }
}