<?php

namespace Wamclient\CoreBundle\Form\_Client;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;


class ClientFilterForm extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
		$builder->add('ci', 'text',  array(
            'label'=>'Cédula / RUC',
            'required'=>false,
        ));
		$builder->add('lastname', 'text',  array(
            'label'=>'Apellidos / Razón Social',
            'required'=>false,
        ));
		$builder->add('name', 'text',  array(
            'label'=>'Nombres / Propietario',
            'required'=>false,
        ));
		$builder->add('account', 'choice', array(
            'label'=>'Saldo',
            'required'=>false,
			'empty_value'   => '-- Todos los saldos -- ',
			'choices'   => array(1 => 'Saldos a favor', 2=> 'Deudas Pendientes', 3=>'Sin deudas ($ 0.00)'),
		));
    }

    public function getName()
    {
        return 'Client';
    }
}