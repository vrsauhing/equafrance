<?php

namespace Wamclient\CoreBundle\Form\_Client;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

use Doctrine\ORM\EntityManager;
use Wamclient\CoreBundle\Form\FormType\CiType;

class ClientForm extends AbstractType
{
    private $em;
	
    public function __construct(EntityManager $em)
    {
        $this->em = $em;
    }
	
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('id', 'hidden');
		$builder->add('ci', new CiType($this->em), array(
			'attr'=> array('class'=>'AjaxClients'),
            'error_bubbling'=>true,
        ));
		$builder->add('name', 'text', array(
			'attr'=> array('class'=>'AjaxClients'),
            'label'=>'Nombres',
            'error_bubbling'=>true,
        ));
		$builder->add('lastname', 'text', array(
			'attr'=> array('class'=>'AjaxClients'),
            'label'=>'Apellidos',
            'error_bubbling'=>true,
        ));
		$builder->add('email', 'text', array(
            'label'=>'Email',
            'error_bubbling'=>true,
        ));
		$builder->add('phone', 'text', array(
            'label'=>'Teléfono',
            'error_bubbling'=>true,
        ));
		$builder->add('celphone', 'text', array(
            'label'=>'Celular',
            'error_bubbling'=>true,
        ));
        $builder->add('address', 'textarea', array(
			'attr'=> array('rows'=> 5,'class'=>'ckeditor'),
			'required'=>false,
		));
    }

    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'Wamclient\CoreBundle\Entity\Client',
        ));
    }

    public function getName()
    {
        return 'Client';
    }
}