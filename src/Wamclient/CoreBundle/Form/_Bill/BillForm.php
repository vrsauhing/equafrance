<?php

namespace Wamclient\CoreBundle\Form\_Bill;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

use Doctrine\ORM\EntityManager;
use Wamclient\CoreBundle\Form\FormType\ClientType;

class BillForm extends AbstractType
{
    private $em;
	
    public function __construct(EntityManager $em)
    {
        $this->em = $em;
    }
	
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('id', 'hidden');
		$builder->add('id_number', 'text', array(
            'error_bubbling'=>true,
        ));
		$builder->add('bill_date', 'text', array(
			'attr'=> array('class'=>'datepicker'),
            'error_bubbling'=>true
		));
		$builder->add('client', new ClientType($this->em), array(
            'error_bubbling'=>true,
        ));
		$builder->add('locked', 'choice', array(
			'choices'   => array(1 => 'Si', 2=> 'No'),
		));
		$builder->add('has_iva', 'choice', array(
			'choices'   => array(1 => 'IVA 12% (agregar al precio)', 2 => 'IVA 12% (descontar del precio)', 3=> 'IVA 0%'),
		));
    }

    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'Wamclient\CoreBundle\Entity\Bill',
        ));
    }

    public function getName()
    {
        return 'Bill';
    }
}