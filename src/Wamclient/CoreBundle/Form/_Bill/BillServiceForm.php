<?php

namespace Wamclient\CoreBundle\Form\_Bill;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

use Doctrine\ORM\EntityManager;
use Wamclient\CoreBundle\Form\FormType\ServiceType;

class BillServiceForm extends AbstractType
{
    private $em;
	
    public function __construct(EntityManager $em)
    {
        $this->em = $em;
    }
	
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('id', 'hidden');
		$builder->add('service', new ServiceType($this->em), array(
            'error_bubbling'=>true,
        ));
		$builder->add('description', 'text', array(
			'attr'=> array('class'=>'AjaxServices'),
            'error_bubbling'=>true,
        ));
		$builder->add('quantity', 'integer', array(
            'error_bubbling'=>true,
			'attr'=> array('class'=>'ServiceQuantity'),
		));
		$builder->add('price', 'money', array(
            'error_bubbling'=>true,
            'currency'=>'USD',
            'precision'=>2,
			'invalid_message' => "El 'precio' ingresado es incorrecto",
		));
    }

    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'Wamclient\CoreBundle\Entity\BillService',
        ));
    }

    public function getName()
    {
        return 'BillService';
    }
}