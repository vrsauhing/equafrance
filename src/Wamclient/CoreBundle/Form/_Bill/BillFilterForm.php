<?php

namespace Wamclient\CoreBundle\Form\_Bill;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;


class BillFilterForm extends AbstractType
{
    private $WamUsers;
	
    public function __construct($WamUsers)
    {
        $this->WamUsers = $WamUsers;
    }
	
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
		$builder->add('id', 'text',  array(
            'label'=>'Factura Id',
            'required'=>false,
        ));
		$builder->add('id_number', 'text',  array(
            'label'=>'Factura #',
            'required'=>false,
        ));
		
		$builder->add('client_ci', 'text',  array(
            'label'=>'Cédula del Cliente',
            'required'=>false,
        ));
		$builder->add('client_lastname', 'text',  array(
            'label'=>'Apellido del Cliente',
            'required'=>false,
        ));
		$builder->add('client_name', 'text',  array(
            'label'=>'Nombre del Cliente',
            'required'=>false,
        ));
		$builder->add('created_at_begin', 'text', array(
            'label'=>'Fecha de Inicio',
			'attr'=> array('class'=>'datepicker'),
            'required'=>false,
		));
		$builder->add('created_at_end', 'text', array(
            'label'=>'Fecha de Fin',
			'attr'=> array('class'=>'datepicker'),
            'required'=>false,
		));
		$builder->add('has_iva', 'choice', array(
            'label'=>'Tarifa?',
			'empty_value' => '-- Escoja una opción --',
			'choices'   => array(1 => 'IVA 12% (agregar al precio)', 2 => 'IVA 12% (descontar del precio)', 3=> 'IVA 0%'),
            'required'=>false,
		));
		$builder->add('canceled', 'choice', array(
			'choices'   => array(1 => 'Vigente', 2=> 'Anulado'),
			'empty_value' => '-- Escoja una opción --',
            'label'=>'Estado?',
            'required'=>false,
		));
		$builder->add('paid', 'choice', array(
			'choices'   => array(1 => 'Si', 2=> 'No'),
			'empty_value' => '-- Escoja una opción --',
            'label'=>'Pagado?',
            'required'=>false,
		));
		$builder->add('locked', 'choice', array(
			'choices'   => array(1 => 'Si', 2=> 'No'),
			'empty_value' => '-- Escoja una opción --',
            'label'=>'Editable?',
            'required'=>false,
		));
		$builder->add('wam_user', 'choice', array(
			'choices'   => $this->WamUsers,
			'empty_value' => '-- Escoja un usuario --',
            'label'=>'Usuario WAM',
            'required'=>false,
		));
    }

    public function getName()
    {
        return 'Bill';
    }
}