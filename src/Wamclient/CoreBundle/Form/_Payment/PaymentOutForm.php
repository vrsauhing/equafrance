<?php

namespace Wamclient\CoreBundle\Form\_Payment;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;

use Symfony\Component\OptionsResolver\OptionsResolverInterface;
use Symfony\Component\Validator\Constraints\NotBlank;

use Doctrine\ORM\EntityManager;
use Wamclient\CoreBundle\Form\FormType\ClientType;

class PaymentOutForm extends AbstractType
{
    private $em;
	
    public function __construct(EntityManager $em)
    {
        $this->em = $em;
    }
	
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('id', 'hidden');
		$builder->add('client', new ClientType($this->em), array(
			'attr'=> array('class'=>'ajaxcomplete'),
            'error_bubbling'=>true,
        ));
		$builder->add('outcome', 'money', array(
            'error_bubbling'=>true,
            'currency'=>'USD',
            'precision'=>2,
			'constraints' => array(
				new NotBlank(array('message' => "Debe agregar el 'valor' del Egreso")),
			),
			'invalid_message' => "El 'valor' ingresado es incorrecto",
		));
        $builder->add('description', 'textarea', array(
            'error_bubbling'=>true,
			'constraints' => array(
				new NotBlank(array('message' => "Debe agregar la 'descripción' del Ingreso")),
			),
		));
    }

    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'Wamclient\CoreBundle\Entity\Payment',
        ));
    }

    public function getName()
    {
        return 'Payment';
    }
}