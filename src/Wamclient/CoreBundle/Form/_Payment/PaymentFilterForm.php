<?php

namespace Wamclient\CoreBundle\Form\_Payment;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;


class PaymentFilterForm extends AbstractType
{
    private $WamUsers;
	
    public function __construct($WamUsers)
    {
        $this->WamUsers = $WamUsers;
    }
	
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
		$builder->add('client_ci', 'text',  array(
            'label'=>'Cédula del Cliente',
            'required'=>false,
        ));
		$builder->add('client_lastname', 'text',  array(
            'label'=>'Apellido del Cliente',
            'required'=>false,
        ));
		$builder->add('client_name', 'text',  array(
            'label'=>'Nombre del Cliente',
            'required'=>false,
        ));
		$builder->add('created_at_begin', 'text', array(
            'label'=>'Fecha de Inicio',
			'attr'=> array('class'=>'datepicker'),
            'required'=>false,
		));
		$builder->add('created_at_end', 'text', array(
            'label'=>'Fecha de Fin',
			'attr'=> array('class'=>'datepicker'),
            'required'=>false,
		));
		$builder->add('value_type', 'choice', array(
			'choices'   => array(1 => 'Ingresos', 2=> 'Egresos'),
			'empty_value' => '-- Escoja una opción --',
            'label'=>'Ingreso / Egreso',
            'required'=>false,
		));
		$builder->add('type', 'choice', array(
			'choices'   => array(1 => 'Sección Factura', 2=> 'Anulado de Facturas', 3=>'Ingresos sección Cliente', 4=>'Egresos sección Cliente'),
			'empty_value' => '-- Escoja una opción --',
            'label'=>'Origen del Pago',
            'required'=>false,
		));
		$builder->add('wam_user', 'choice', array(
			'choices'   => $this->WamUsers,
			'empty_value' => '-- Escoja un usuario --',
            'label'=>'Usuario WAM',
            'required'=>false,
		));
    }

    public function getName()
    {
        return 'Payment';
    }
}