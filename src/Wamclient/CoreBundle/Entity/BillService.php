<?php
// src/Wamclient/CoreBundle/Entity/AmbImage.php
namespace Wamclient\CoreBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Entity(repositoryClass="Wamclient\CoreBundle\Entity\BillServiceRepository")
 * @ORM\Table(name="BillService")
 */
 
class BillService
{
    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;
		
    /**
	 * @ORM\ManyToOne(targetEntity="Bill", inversedBy="bill_services")
	 * @ORM\JoinColumn(name="bill_id", referencedColumnName="id", onDelete="CASCADE")
     */
    private $bill;
	
    /**
     * @Assert\NotBlank(message="Debe escoger un 'servicio'")
     * @ORM\ManyToOne(targetEntity="Service")
     * @ORM\JoinColumn(name="service_id", referencedColumnName="id")
     */
    private $service;
	
    /**
     * @Assert\NotBlank(message = "Ingrese el 'servicio' de la factura.")
     * @ORM\Column(type="string", length=100)
     */
    private $description;
	
    /**
     * @Assert\Range(
     *      min = 1,
     *      minMessage = "Debe ingresar al menos 1 unidad del servicio"
     * )
     * @Assert\NotBlank(message="Debe ingresar la 'cantidad' del servicio")
     * @ORM\Column(type="integer")
     */
    private $quantity;
	
    /**
     * @Assert\NotBlank(message="Debe ingresar el 'precio' del servicio")
     * @ORM\Column(type="decimal", scale=2)
     */
    private $price;
	
    /**
     * @ORM\Column(type="decimal", scale=2)
     */
    private $total;
	
    /**
     * @ORM\Column(type="integer")
     */
    private $status;
	
    /**
     * @ORM\Column(type="datetime")
     */
    private $created_at;

    public function __construct()
    {		
        $this->bill = new \Doctrine\Common\Collections\ArrayCollection();
        $this->status = 1;
		
		$DateTime= new \DateTime('now');
        $this->created_at = $DateTime;
    }

    /**
     * Set id
     *
     * @param string $id
     * @return BillService
     */
    public function setId($id)
    {
        $this->id = $id;

        return $this;
    }

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set quantity
     *
     * @param integer $quantity
     * @return BillService
     */
    public function setQuantity($quantity)
    {
        $this->quantity = $quantity;

        return $this;
    }

    /**
     * Get quantity
     *
     * @return integer 
     */
    public function getQuantity()
    {
        return $this->quantity;
    }

    /**
     * Set total
     *
     * @param float $total
     * @return BillService
     */
    public function setTotal($total)
    {
        $this->total = $total;

        return $this;
    }

    /**
     * Get total
     *
     * @return float 
     */
    public function getTotal()
    {
        return $this->total;
    }

    /**
     * Set status
     *
     * @param integer $status
     * @return BillService
     */
    public function setStatus($status)
    {
        $this->status = $status;

        return $this;
    }

    /**
     * Get status
     *
     * @return integer 
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * Set created_at
     *
     * @param \DateTime $createdAt
     * @return BillService
     */
    public function setCreatedAt($createdAt)
    {
        $this->created_at = $createdAt;

        return $this;
    }

    /**
     * Get created_at
     *
     * @return \DateTime 
     */
    public function getCreatedAt()
    {
        return $this->created_at;
    }

    /**
     * Set service
     *
     * @param \Wamclient\CoreBundle\Entity\Service $service
     * @return BillService
     */
    public function setService(\Wamclient\CoreBundle\Entity\Service $service = null)
    {
        $this->service = $service;

        return $this;
    }

    /**
     * Get service
     *
     * @return \Wamclient\CoreBundle\Entity\Service 
     */
    public function getService()
    {
        return $this->service;
    }

    /**
     * Set bill
     *
     * @param \Wamclient\CoreBundle\Entity\Bill $bill
     * @return BillService
     */
    public function setBill(\Wamclient\CoreBundle\Entity\Bill $bill = null)
    {
        $this->bill = $bill;

        return $this;
    }

    /**
     * Get bill
     *
     * @return \Wamclient\CoreBundle\Entity\Bill 
     */
    public function getBill()
    {
        return $this->bill;
    }

    /**
     * Set description
     *
     * @param string $description
     * @return BillService
     */
    public function setDescription($description)
    {
        $this->description = $description;

        return $this;
    }

    /**
     * Get description
     *
     * @return string 
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * Set price
     *
     * @param string $price
     * @return BillService
     */
    public function setPrice($price)
    {
        $this->price = $price;

        return $this;
    }

    /**
     * Get price
     *
     * @return string 
     */
    public function getPrice()
    {
        return $this->price;
    }
}
