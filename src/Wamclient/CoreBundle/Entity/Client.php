<?php

namespace Wamclient\CoreBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Entity(repositoryClass="Wamclient\CoreBundle\Entity\ClientRepository")
 * @ORM\Table(name="Client")
 */
 
class Client
{
    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;
	
    /**
     * @ORM\Column(type="string", length=13, nullable=true)
     */
    private $ci;
	
    /**
     * @Assert\NotBlank(message = "Ingrese el 'nombre' del cliente.")
     * @ORM\Column(type="string", length=100)
     */
    private $name;

    /**
     * @Assert\NotBlank(message = "Ingrese el 'apellido' del cliente.")
     * @ORM\Column(type="string", length=100)
     */
    private $lastname;
	
	/**
     * @Assert\Email(message = "Ingrese un 'email' válido" )
     * @ORM\Column(type="string", length=100, nullable=true)
     */
    private $email;
		
    /**
     * @ORM\Column(type="text", nullable=true)
     */
    private $address;

    /**
     * @ORM\Column(type="string", length=100, nullable=true)
     */
    private $phone;

    /**
     * @ORM\Column(type="string", length=100, nullable=true)
     */
    private $celphone;
		
    /**
     * @ORM\Column(type="decimal", scale=2)
     */
    private $account;
		
	/**
     * @ORM\Column(type="datetime")
     */
    private $created_at;
	
    /**
     * @ORM\OneToMany(targetEntity="Bill", mappedBy="client")
     * @ORM\JoinColumn(name="bill_id", referencedColumnName="id", onDelete="CASCADE")
     */
    private $bills;
	
    /**
     * @ORM\OneToMany(targetEntity="Payment", mappedBy="client")
     * @ORM\JoinColumn(name="payment_id", referencedColumnName="id", onDelete="CASCADE")
     */
    private $payments;
	
    public function __construct()
    {				
		$DateTime= new \DateTime('now');
		$this->created_at = $DateTime;
		$this->account = 0;
    }

    /**
     * Set id
     *
     * @param string $id
     * @return Client
     */
    public function setId($id)
    {
        $this->id = $id;

        return $this;
    }



    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set ci
     *
     * @param string $ci
     * @return Client
     */
    public function setCi($ci)
    {
        $this->ci = $ci;

        return $this;
    }

    /**
     * Get ci
     *
     * @return string 
     */
    public function getCi()
    {
        return $this->ci;
    }

    /**
     * Set name
     *
     * @param string $name
     * @return Client
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string 
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set lastname
     *
     * @param string $lastname
     * @return Client
     */
    public function setLastname($lastname)
    {
        $this->lastname = $lastname;

        return $this;
    }

    /**
     * Get lastname
     *
     * @return string 
     */
    public function getLastname()
    {
        return $this->lastname;
    }

    /**
     * Set email
     *
     * @param string $email
     * @return Client
     */
    public function setEmail($email)
    {
        $this->email = $email;

        return $this;
    }

    /**
     * Get email
     *
     * @return string 
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * Set address
     *
     * @param string $address
     * @return Client
     */
    public function setAddress($address)
    {
        $this->address = $address;

        return $this;
    }

    /**
     * Get address
     *
     * @return string 
     */
    public function getAddress()
    {
        return $this->address;
    }

    /**
     * Set phone
     *
     * @param string $phone
     * @return Client
     */
    public function setPhone($phone)
    {
        $this->phone = $phone;

        return $this;
    }

    /**
     * Get phone
     *
     * @return string 
     */
    public function getPhone()
    {
        return $this->phone;
    }

    /**
     * Set celphone
     *
     * @param string $celphone
     * @return Client
     */
    public function setCelphone($celphone)
    {
        $this->celphone = $celphone;

        return $this;
    }

    /**
     * Get celphone
     *
     * @return string 
     */
    public function getCelphone()
    {
        return $this->celphone;
    }

    /**
     * Set account
     *
     * @param float $account
     * @return Client
     */
    public function setAccount($account)
    {
        $this->account = $account;

        return $this;
    }

    /**
     * Get account
     *
     * @return float 
     */
    public function getAccount()
    {
        return $this->account;
    }

    /**
     * Set created_at
     *
     * @param \DateTime $createdAt
     * @return Client
     */
    public function setCreatedAt($createdAt)
    {
        $this->created_at = $createdAt;

        return $this;
    }

    /**
     * Get created_at
     *
     * @return \DateTime 
     */
    public function getCreatedAt()
    {
        return $this->created_at;
    }

    /**
     * Add bills
     *
     * @param \Wamclient\CoreBundle\Entity\Bill $bills
     * @return Client
     */
    public function addBill(\Wamclient\CoreBundle\Entity\Bill $bills)
    {
        $this->bills[] = $bills;

        return $this;
    }

    /**
     * Remove bills
     *
     * @param \Wamclient\CoreBundle\Entity\Bill $bills
     */
    public function removeBill(\Wamclient\CoreBundle\Entity\Bill $bills)
    {
        $this->bills->removeElement($bills);
    }

    /**
     * Get bills
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getBills()
    {
        return $this->bills;
    }

    /**
     * Add payments
     *
     * @param \Wamclient\CoreBundle\Entity\Payment $payments
     * @return Client
     */
    public function addPayment(\Wamclient\CoreBundle\Entity\Payment $payments)
    {
        $this->payments[] = $payments;

        return $this;
    }

    /**
     * Remove payments
     *
     * @param \Wamclient\CoreBundle\Entity\Payment $payments
     */
    public function removePayment(\Wamclient\CoreBundle\Entity\Payment $payments)
    {
        $this->payments->removeElement($payments);
    }

    /**
     * Get payments
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getPayments()
    {
        return $this->payments;
    }
}
