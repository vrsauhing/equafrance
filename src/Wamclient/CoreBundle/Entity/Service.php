<?php
// src/Wamclient/CoreBundle/Entity/AmbImage.php
namespace Wamclient\CoreBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Entity(repositoryClass="Wamclient\CoreBundle\Entity\ServiceRepository")
 * @ORM\Table(name="Service")
 */
 
class Service
{
    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;
	
    /**
     * @Assert\NotBlank(message = "Ingrese el 'nombre' del servicio.")
     * @ORM\Column(type="string", length=100)
     */
    private $name;
	
    /**
     * @Assert\NotBlank(message = "Ingrese el 'precio' del servicio.")
     * @ORM\Column(type="decimal", scale=2)
     */
    private $price;
	
    /**
     * @ORM\Column(type="integer")
     */
    private $status;


    /**
     * Set id
     *
     * @param string $id
     * @return Service
     */
    public function setId($id)
    {
        $this->id = $id;

        return $this;
    }


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     * @return Service
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string 
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set price
     *
     * @param float $price
     * @return Service
     */
    public function setPrice($price)
    {
        $this->price = $price;

        return $this;
    }

    /**
     * Get price
     *
     * @return float 
     */
    public function getPrice()
    {
        return $this->price;
    }

    /**
     * Set status
     *
     * @param integer $status
     * @return Service
     */
    public function setStatus($status)
    {
        $this->status = $status;

        return $this;
    }

    /**
     * Get status
     *
     * @return integer 
     */
    public function getStatus()
    {
        return $this->status;
    }
}
