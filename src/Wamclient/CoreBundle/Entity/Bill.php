<?php

namespace Wamclient\CoreBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Entity(repositoryClass="Wamclient\CoreBundle\Entity\BillRepository")
 * @ORM\Table(name="Bill")
 */
 
class Bill
{
    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;
	
    /**
     * @ORM\Column(type="string", length=20, nullable=true)
     */
    private $id_number;

    /**
     * @Assert\NotBlank(message = "Ingrese la 'fecha' de la correctamente.")
     * @Assert\Date(message = "La 'fecha' ingresada no es válida.")
     * @ORM\Column(type="date")
     */
    private $bill_date;
	
    /**
     * @ORM\Column(type="integer")
     */
    private $wam_user;
	
    /**
	 * @ORM\ManyToOne(targetEntity="Client", inversedBy="bills")
	 * @ORM\JoinColumn(name="client_id", referencedColumnName="id",  onDelete="SET NULL")
     */
    private $client;
				
    /**
     * @ORM\Column(type="decimal", scale=2)
     */
    private $subtotal;
				
    /**
     * @ORM\Column(type="decimal", scale=2)
     */
    private $iva;
				
    /**
     * @ORM\Column(type="decimal", scale=2)
     */
    private $total;
				
    /**
     * @ORM\Column(type="decimal", scale=2)
     */
    private $paid;
    
	/**
     * @ORM\Column(type="integer")
     */
    private $has_iva;
    
	/**
     * @ORM\Column(type="integer")
     */
    private $locked;
    
	/**
     * @ORM\Column(type="integer")
     */
    private $canceled;
	
    /**
     * @ORM\Column(type="datetime")
     */
    private $created_at;
	
    /**
     * @ORM\OneToMany(targetEntity="BillService", mappedBy="bill")
     * @ORM\JoinColumn(name="bill_services_id", referencedColumnName="id", onDelete="CASCADE")
     */
    private $bill_services;
	
    /**
     * @ORM\OneToMany(targetEntity="Payment", mappedBy="bill")
     * @ORM\JoinColumn(name="payments_id", referencedColumnName="id", onDelete="CASCADE")
     */
    private $payments;

    public function __construct()
    {				
		$DateTime= new \DateTime('now');
		$this->subtotal = 0;
		$this->iva = 0;
		$this->total = 0;
		$this->paid = 0;
		$this->canceled = 1;
		$this->created_at = $DateTime;
    }

    /**
     * Set id
     *
     * @param string $id
     * @return Bill
     */
    public function setId($id)
    {
        $this->id = $id;

        return $this;
    }

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set id_number
     *
     * @param string $idNumber
     * @return Bill
     */
    public function setIdNumber($idNumber)
    {
        $this->id_number = $idNumber;

        return $this;
    }

    /**
     * Get id_number
     *
     * @return string 
     */
    public function getIdNumber()
    {
        return $this->id_number;
    }

    /**
     * Set bill_date
     *
     * @param \DateTime $billDate
     * @return Bill
     */
    public function setBillDate($billDate)
    {
		$DateObject = date_create_from_format('M d, Y', $billDate);
	
        $this->bill_date = $DateObject;

        return $this;
    }

    /**
     * Get bill_date
     *
     * @return \DateTime 
     */
    public function getBillDate()
    {
		if( is_object($this->bill_date)):
			$DateObject = $this->bill_date->format('M d, Y');
			return $DateObject;
		else:
			return $this->bill_date;
		endif;
    }

    /**
     * Set wam_user
     *
     * @param integer $wamUser
     * @return Bill
     */
    public function setWamUser($wamUser)
    {
        $this->wam_user = $wamUser;

        return $this;
    }

    /**
     * Get wam_user
     *
     * @return integer 
     */
    public function getWamUser()
    {
        return $this->wam_user;
    }

    /**
     * Set subtotal
     *
     * @param string $subtotal
     * @return Bill
     */
    public function setSubtotal($subtotal)
    {
        $this->subtotal = $subtotal;

        return $this;
    }

    /**
     * Get subtotal
     *
     * @return string 
     */
    public function getSubtotal()
    {
        return $this->subtotal;
    }

    /**
     * Set iva
     *
     * @param string $iva
     * @return Bill
     */
    public function setIva($iva)
    {
        $this->iva = $iva;

        return $this;
    }

    /**
     * Get iva
     *
     * @return string 
     */
    public function getIva()
    {
        return $this->iva;
    }

    /**
     * Set total
     *
     * @param string $total
     * @return Bill
     */
    public function setTotal($total)
    {
        $this->total = $total;

        return $this;
    }

    /**
     * Get total
     *
     * @return string 
     */
    public function getTotal()
    {
        return $this->total;
    }

    /**
     * Set paid
     *
     * @param string $paid
     * @return Bill
     */
    public function setPaid($paid)
    {
        $this->paid = $paid;

        return $this;
    }

    /**
     * Get paid
     *
     * @return string 
     */
    public function getPaid()
    {
        return $this->paid;
    }

    /**
     * Set has_iva
     *
     * @param integer $hasIva
     * @return Bill
     */
    public function setHasIva($hasIva)
    {
        $this->has_iva = $hasIva;

        return $this;
    }

    /**
     * Get has_iva
     *
     * @return integer 
     */
    public function getHasIva()
    {
        return $this->has_iva;
    }

    /**
     * Set locked
     *
     * @param integer $locked
     * @return Bill
     */
    public function setLocked($locked)
    {
        $this->locked = $locked;

        return $this;
    }

    /**
     * Get locked
     *
     * @return integer 
     */
    public function getLocked()
    {
        return $this->locked;
    }

    /**
     * Set canceled
     *
     * @param integer $canceled
     * @return Bill
     */
    public function setCanceled($canceled)
    {
        $this->canceled = $canceled;

        return $this;
    }

    /**
     * Get canceled
     *
     * @return integer 
     */
    public function getCanceled()
    {
        return $this->canceled;
    }

    /**
     * Set created_at
     *
     * @param \DateTime $createdAt
     * @return Bill
     */
    public function setCreatedAt($createdAt)
    {
        $this->created_at = $createdAt;

        return $this;
    }

    /**
     * Get created_at
     *
     * @return \DateTime 
     */
    public function getCreatedAt()
    {
        return $this->created_at;
    }

    /**
     * Set client
     *
     * @param \Wamclient\CoreBundle\Entity\Client $client
     * @return Bill
     */
    public function setClient(\Wamclient\CoreBundle\Entity\Client $client = null)
    {
        $this->client = $client;

        return $this;
    }

    /**
     * Get client
     *
     * @return \Wamclient\CoreBundle\Entity\Client 
     */
    public function getClient()
    {
        return $this->client;
    }

    /**
     * Add bill_services
     *
     * @param \Wamclient\CoreBundle\Entity\BillService $billServices
     * @return Bill
     */
    public function addBillService(\Wamclient\CoreBundle\Entity\BillService $billServices)
    {
        $this->bill_services[] = $billServices;

        return $this;
    }

    /**
     * Remove bill_services
     *
     * @param \Wamclient\CoreBundle\Entity\BillService $billServices
     */
    public function removeBillService(\Wamclient\CoreBundle\Entity\BillService $billServices)
    {
        $this->bill_services->removeElement($billServices);
    }

    /**
     * Get bill_services
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getBillServices()
    {
        return $this->bill_services;
    }

    /**
     * Add payments
     *
     * @param \Wamclient\CoreBundle\Entity\Payment $payments
     * @return Bill
     */
    public function addPayment(\Wamclient\CoreBundle\Entity\Payment $payments)
    {
        $this->payments[] = $payments;

        return $this;
    }

    /**
     * Remove payments
     *
     * @param \Wamclient\CoreBundle\Entity\Payment $payments
     */
    public function removePayment(\Wamclient\CoreBundle\Entity\Payment $payments)
    {
        $this->payments->removeElement($payments);
    }

    /**
     * Get payments
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getPayments()
    {
        return $this->payments;
    }
}
