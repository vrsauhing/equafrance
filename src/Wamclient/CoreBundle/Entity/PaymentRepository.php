<?php

namespace Wamclient\CoreBundle\Entity;

use Doctrine\ORM\EntityRepository;

/**
 * PaymentRepository
 *
 * This class was generated by the Doctrine ORM. Add your own custom
 * repository methods below.
 */
class PaymentRepository extends EntityRepository
{
    public function findPayments($FilterArray, $OrderArray)
    {		
		//INIT FILTER VARIABLES FOR QUERY
		$FilterQuery='WHERE c.lastname IS NOT null';
		if($FilterArray):
			foreach($FilterArray as  $key=>$value)
			{
				if($value):
					switch($key):
						case 'client_ci':
							$FilterQuery=$FilterQuery." AND c.ci ='".$value."'";
							break;
						case 'client_lastname':
							$FilterQuery=$FilterQuery." AND c.lastname LIKE '".$value."%'";
							break;
						case 'client_name':
							$FilterQuery=$FilterQuery." AND c.name LIKE '".$value."%'";
							break;
						case 'created_at_begin':
							$FilterQuery=$FilterQuery." AND p.created_at >='".$value." 00:00:00' ";
							break;
						case 'created_at_end':
							$FilterQuery=$FilterQuery." AND p.created_at <='".$value." 23:59:59'";
							break;
						case 'value_type':
							if($value==1):
								$FilterQuery=$FilterQuery." AND p.income > 0";
							elseif($value==2):
								$FilterQuery=$FilterQuery." AND p.outcome > 0";					
							endif;							
							break;
						case 'type':
							$FilterQuery=$FilterQuery." AND p.type =".$value."";
							break;
						case 'wam_user':
							$FilterQuery=$FilterQuery." AND p.wam_user =".$value."";
							break;
					endswitch;
				endif;
			}
		endif;
		
		//INIT ORDER VARIABLES FOR QUERY
		$OrderQuery='p.id DESC';
		if($OrderArray):
			$OrderQuery='p.'.$OrderArray['Row'].' '.$OrderArray['Order'];
			if($OrderArray['Row']=='client_ci'):
				$OrderQuery='c.ci '.$OrderArray['Order'];
			endif;
			if($OrderArray['Row']=='client_lastname'):
				$OrderQuery='c.lastname '.$OrderArray['Order'];
			endif;
		endif;
		
		//RETURNING QUERY WITH ENTERED VARIABLES
        return $this->getEntityManager()
            ->createQuery(
                'SELECT p FROM WamclientCoreBundle:Payment p JOIN p.client c '.$FilterQuery.' ORDER BY '.$OrderQuery
            );
	}
    public function findClientPayments($ClientId)
    {
		//RETURNING QUERY WITH ENTERED VARIABLES
        return $this->getEntityManager()
            ->createQuery(
                'SELECT p 
				FROM WamclientCoreBundle:Payment p 
				WHERE p.client='.$ClientId.'
				ORDER BY p.id DESC'
            )->setMaxResults(15)->getResult();
	}
	public function findBillPayments($BillId)
    {
		//RETURNING QUERY WITH ENTERED VARIABLES
        return $this->getEntityManager()
            ->createQuery(
                'SELECT p 
				FROM WamclientCoreBundle:Payment p 
				WHERE p.bill='.$BillId.'
				ORDER BY p.id DESC'
            )->setMaxResults(15)->getResult();
	}
}
