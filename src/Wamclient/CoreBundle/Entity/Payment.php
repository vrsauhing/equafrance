<?php

namespace Wamclient\CoreBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Entity(repositoryClass="Wamclient\CoreBundle\Entity\PaymentRepository")
 * @ORM\Table(name="Payment")
 */
 
class Payment
{
    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;
	
    /**
	 * @ORM\ManyToOne(targetEntity="Bill", inversedBy="payments")
	 * @ORM\JoinColumn(name="bill_id", referencedColumnName="id",  onDelete="SET NULL", nullable=true)
     */
    private $bill;
	
    /**
	 * @ORM\ManyToOne(targetEntity="Client", inversedBy="payments")
	 * @ORM\JoinColumn(name="client_id", referencedColumnName="id",  onDelete="SET NULL")
     */
    private $client;
	
    /**
     * @ORM\Column(type="integer")
     */
    private $wam_user;
				
    /**
     * @Assert\NotBlank(message = "Ingrese la 'descripción' del pago.")
     * @ORM\Column(type="text", nullable=true)
     */
    private $description;
				
    /**
     * @ORM\Column(type="decimal", scale=2, nullable=true)
     */
    private $income;
				
    /**
     * @ORM\Column(type="decimal", scale=2, nullable=true)
     */
    private $outcome;
				
    /**
     * @ORM\Column(type="decimal", scale=2)
     */
    private $total;
		
	/**
     * @ORM\Column(type="integer")
     */
    private $type;
	
    /**
     * @ORM\Column(type="datetime")
     */
    private $created_at;

    public function __construct()
    {				
		$DateTime= new \DateTime('now');
		$this->created_at = $DateTime;
		$this->type = 1;
    }

    /**
     * Set id
     *
     * @param string $id
     * @return Payment
     */
    public function setId($id)
    {
        $this->id = $id;

        return $this;
    }

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set wam_user
     *
     * @param integer $wamUser
     * @return Payment
     */
    public function setWamUser($wamUser)
    {
        $this->wam_user = $wamUser;

        return $this;
    }

    /**
     * Get wam_user
     *
     * @return integer 
     */
    public function getWamUser()
    {
        return $this->wam_user;
    }

    /**
     * Set description
     *
     * @param string $description
     * @return Payment
     */
    public function setDescription($description)
    {
        $this->description = $description;

        return $this;
    }

    /**
     * Get description
     *
     * @return string 
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * Set income
     *
     * @param string $income
     * @return Payment
     */
    public function setIncome($income)
    {
        $this->income = $income;

        return $this;
    }

    /**
     * Get income
     *
     * @return string 
     */
    public function getIncome()
    {
        return $this->income;
    }

    /**
     * Set outcome
     *
     * @param string $outcome
     * @return Payment
     */
    public function setOutcome($outcome)
    {
        $this->outcome = $outcome;

        return $this;
    }

    /**
     * Get outcome
     *
     * @return string 
     */
    public function getOutcome()
    {
        return $this->outcome;
    }

    /**
     * Set total
     *
     * @param string $total
     * @return Payment
     */
    public function setTotal($total)
    {
        $this->total = $total;

        return $this;
    }

    /**
     * Get total
     *
     * @return string 
     */
    public function getTotal()
    {
        return $this->total;
    }

    /**
     * Set type
     *
     * @param integer $type
     * @return Payment
     */
    public function setType($type)
    {
        $this->type = $type;

        return $this;
    }

    /**
     * Get type
     *
     * @return integer 
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * Set created_at
     *
     * @param \DateTime $createdAt
     * @return Payment
     */
    public function setCreatedAt($createdAt)
    {
        $this->created_at = $createdAt;

        return $this;
    }

    /**
     * Get created_at
     *
     * @return \DateTime 
     */
    public function getCreatedAt()
    {
        return $this->created_at;
    }

    /**
     * Set bill
     *
     * @param \Wamclient\CoreBundle\Entity\Bill $bill
     * @return Payment
     */
    public function setBill(\Wamclient\CoreBundle\Entity\Bill $bill = null)
    {
        $this->bill = $bill;

        return $this;
    }

    /**
     * Get bill
     *
     * @return \Wamclient\CoreBundle\Entity\Bill 
     */
    public function getBill()
    {
        return $this->bill;
    }

    /**
     * Set client
     *
     * @param \Wamclient\CoreBundle\Entity\Client $client
     * @return Payment
     */
    public function setClient(\Wamclient\CoreBundle\Entity\Client $client = null)
    {
        $this->client = $client;

        return $this;
    }

    /**
     * Get client
     *
     * @return \Wamclient\CoreBundle\Entity\Client 
     */
    public function getClient()
    {
        return $this->client;
    }
}
