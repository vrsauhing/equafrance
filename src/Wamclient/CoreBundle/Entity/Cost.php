<?php

namespace Wamclient\CoreBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Entity(repositoryClass="Wamclient\CoreBundle\Entity\CostRepository")
 * @ORM\Table(name="Cost")
 */
 
class Cost
{
    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @Assert\NotBlank(message = "Ingrese la 'fecha' del gasto correctamente.")
     * @Assert\Date(message = "La 'fecha' ingresada no es válida.")
     * @ORM\Column(type="date")
     */
    private $cost_date;
	
    /**
     * @ORM\Column(type="integer")
     */
    private $wam_user;
	
    /**
     * @Assert\NotBlank(message = "Ingrese el 'nombre' del proveedor.")
     * @ORM\Column(type="string", length=100)
     */
    private $provider;
	
    /**
     * @Assert\NotBlank(message = "Ingrese el 'descripción' del gasto.")
     * @ORM\Column(type="text")
     */
    private $description;
				
    /**
     * @ORM\Column(type="decimal", scale=2)
     */
    private $subtotal;
				
    /**
     * @ORM\Column(type="decimal", scale=2)
     */
    private $iva;
				
    /**
     * @ORM\Column(type="decimal", scale=2)
     */
    private $total;	
    
	/**
     * @ORM\Column(type="integer")
     */
    private $locked;
    
	/**
     * @ORM\Column(type="integer")
     */
    private $canceled;
	
    /**
     * @ORM\Column(type="datetime")
     */
    private $created_at;

    public function __construct()
    {				
		$DateTime= new \DateTime('now');
		$this->subtotal = 0;
		$this->iva = 0;
		$this->total = 0;
		$this->paid = 0;
		$this->canceled = 1;
		$this->created_at = $DateTime;
    }

    /**
     * Set id
     *
     * @param string $id
     * @return Cost
     */
    public function setId($id)
    {
        $this->id = $id;

        return $this;
    }

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set cost_date
     *
     * @param \DateTime $costDate
     * @return Cost
     */
    public function setCostDate($costDate)
    {
		$DateObject = date_create_from_format('M d, Y', $costDate);
	
        $this->cost_date = $DateObject;

        return $this;
    }

    /**
     * Get cost_date
     *
     * @return \DateTime 
     */
    public function getCostDate()
    {
		if( is_object($this->cost_date)):
			$DateObject = $this->cost_date->format('M d, Y');
			return $DateObject;
		else:
			return $this->cost_date;
		endif;
    }

    /**
     * Set wam_user
     *
     * @param integer $wamUser
     * @return Cost
     */
    public function setWamUser($wamUser)
    {
        $this->wam_user = $wamUser;

        return $this;
    }

    /**
     * Get wam_user
     *
     * @return integer 
     */
    public function getWamUser()
    {
        return $this->wam_user;
    }

    /**
     * Set provider
     *
     * @param string $provider
     * @return Cost
     */
    public function setProvider($provider)
    {
        $this->provider = $provider;

        return $this;
    }

    /**
     * Get provider
     *
     * @return string 
     */
    public function getProvider()
    {
        return $this->provider;
    }

    /**
     * Set description
     *
     * @param string $description
     * @return Cost
     */
    public function setDescription($description)
    {
        $this->description = $description;

        return $this;
    }

    /**
     * Get description
     *
     * @return string 
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * Set subtotal
     *
     * @param string $subtotal
     * @return Cost
     */
    public function setSubtotal($subtotal)
    {
        $this->subtotal = $subtotal;

        return $this;
    }

    /**
     * Get subtotal
     *
     * @return string 
     */
    public function getSubtotal()
    {
        return $this->subtotal;
    }

    /**
     * Set iva
     *
     * @param string $iva
     * @return Cost
     */
    public function setIva($iva)
    {
        $this->iva = $iva;

        return $this;
    }

    /**
     * Get iva
     *
     * @return string 
     */
    public function getIva()
    {
        return $this->iva;
    }

    /**
     * Set total
     *
     * @param string $total
     * @return Cost
     */
    public function setTotal($total)
    {
        $this->total = $total;

        return $this;
    }

    /**
     * Get total
     *
     * @return string 
     */
    public function getTotal()
    {
        return $this->total;
    }

    /**
     * Set locked
     *
     * @param integer $locked
     * @return Cost
     */
    public function setLocked($locked)
    {
        $this->locked = $locked;

        return $this;
    }

    /**
     * Get locked
     *
     * @return integer 
     */
    public function getLocked()
    {
        return $this->locked;
    }

    /**
     * Set canceled
     *
     * @param integer $canceled
     * @return Cost
     */
    public function setCanceled($canceled)
    {
        $this->canceled = $canceled;

        return $this;
    }

    /**
     * Get canceled
     *
     * @return integer 
     */
    public function getCanceled()
    {
        return $this->canceled;
    }

    /**
     * Set created_at
     *
     * @param \DateTime $createdAt
     * @return Cost
     */
    public function setCreatedAt($createdAt)
    {
        $this->created_at = $createdAt;

        return $this;
    }

    /**
     * Get created_at
     *
     * @return \DateTime 
     */
    public function getCreatedAt()
    {
        return $this->created_at;
    }
}
