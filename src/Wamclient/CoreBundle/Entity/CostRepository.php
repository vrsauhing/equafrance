<?php

namespace Wamclient\CoreBundle\Entity;

use Doctrine\ORM\EntityRepository;

/**
 * OrderRepository
 *
 * This class was generated by the Doctrine ORM. Add your own custom
 * repository methods below.
 */
class CostRepository extends EntityRepository
{
    public function findCosts($FilterArray, $OrderArray, $Wamuser)
    {		
		//INIT FILTER VARIABLES FOR QUERY
		$FilterQuery='WHERE p.id IS NOT null';
		if($Wamuser!=''):
			$FilterQuery='WHERE p.wam_user ='.$Wamuser;
		endif;
		if($FilterArray):
			foreach($FilterArray as  $key=>$value)
			{
				if($value):
					switch($key):
						case 'provider':
							$FilterQuery=$FilterQuery." AND c.provider LIKE '%".$value."%'";
							break;
						case 'created_at_begin':
							$FilterQuery=$FilterQuery." AND p.bill_date >='".$value." 00:00:00' ";
							break;
						case 'created_at_end':
							$FilterQuery=$FilterQuery." AND p.bill_date <='".$value." 23:59:59'";
							break;
						case 'canceled':
							$FilterQuery=$FilterQuery." AND p.canceled =".$value."";
							break;
						case 'locked':
							$FilterQuery=$FilterQuery." AND p.locked =".$value."";
							break;
						case 'wam_user':
							$FilterQuery=$FilterQuery." AND p.wam_user =".$value."";
							break;
					endswitch;
				endif;
			}
		endif;
		
		//INIT ORDER VARIABLES FOR QUERY
		$OrderQuery='p.created_at DESC';
		if($OrderArray):
			$OrderQuery='p.'.$OrderArray['Row'].' '.$OrderArray['Order'];
		endif;
		
		//RETURNING QUERY WITH ENTERED VARIABLES
        return $this->getEntityManager()
            ->createQuery(
                'SELECT p FROM WamclientCoreBundle:Cost p '.$FilterQuery.' ORDER BY '.$OrderQuery
            );
	}
    
	public function findClientBills($ClientId)
	{
		//RETURNING QUERY WITH ENTERED VARIABLES
        return $this->getEntityManager()
            ->createQuery(
                'SELECT p 
				FROM WamclientCoreBundle:Bill p 
				WHERE p.client='.$ClientId.' AND p.locked = 2
				ORDER BY p.bill_date DESC'
            )->setMaxResults(20)->getResult();
	}
	
	public function findBillsNoPaid($ClientId)
	{
		//RETURNING QUERY WITH ENTERED VARIABLES
        return $this->getEntityManager()
            ->createQuery(
                'SELECT p 
				FROM WamclientCoreBundle:Bill p 
				WHERE p.client='.$ClientId.' AND p.locked = 2 AND p.canceled=1 AND p.paid < p.total
				ORDER BY p.bill_date ASC'
            )->getResult();
	}
}
