<?php

namespace Wamclient\CoreBundle\Entity;

use Doctrine\ORM\EntityRepository;


class ClientRepository extends EntityRepository
{
    public function findClients($FilterArray, $OrderArray)
    {		
		//INIT FILTER VARIABLES FOR QUERY
		$FilterQuery='WHERE p.id > 1';
		if($FilterArray):
			foreach($FilterArray as  $key=>$value)
			{
				if($value):
					switch($key):
						case 'ci':
							$FilterQuery=$FilterQuery." AND p.ci ='".$value."'";
							break;
						case 'lastname':
							$FilterQuery=$FilterQuery." AND p.lastname LIKE '".$value."%'";
							break;
						case 'name':
							$FilterQuery=$FilterQuery." AND p.name LIKE '".$value."%'";
							break;
						case 'account':
							if($value==1):
								$FilterQuery=$FilterQuery." AND p.account > 0";
							elseif($value==2):
								$FilterQuery=$FilterQuery." AND p.account < 0";							
							elseif($value==3):
								$FilterQuery=$FilterQuery." AND p.account = 0";							
							endif;							
							break;
					endswitch;
				endif;
			}
		endif;
		
		//INIT ORDER VARIABLES FOR QUERY
		$OrderQuery='p.id DESC';
		if($OrderArray):
			$OrderQuery='p.'.$OrderArray['Row'].' '.$OrderArray['Order'];
		endif;
		
		//RETURNING QUERY WITH ENTERED VARIABLES
        return $this->getEntityManager()
            ->createQuery(
                'SELECT p FROM WamclientCoreBundle:Client p '.$FilterQuery.' ORDER BY '.$OrderQuery
            );
	}
	
	public function findAjaxClients($SearchString)
    {	
		$SearchString=str_replace('"', '', $SearchString);
		
		//INIT FILTER VARIABLES FOR QUERY
		$SearchQuery="WHERE (p.name LIKE '".$SearchString."%' OR p.lastname LIKE '".$SearchString."%' OR p.ci LIKE'".$SearchString."%')";
		
		//RETURNING QUERY WITH ENTERED VARIABLES
        return $this->getEntityManager()
            ->createQuery(
                'SELECT p FROM WamclientCoreBundle:Client p '.$SearchQuery.' ORDER BY p.lastname ASC'
            )->setMaxResults(5);
	}
}
