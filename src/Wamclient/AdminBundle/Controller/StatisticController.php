<?php

namespace Wamclient\AdminBundle\Controller;

//LIBRARIES
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;

//FORMS
use Wamclient\CoreBundle\Form\_Statistic\BillDailyForm;
use Wamclient\CoreBundle\Form\_Statistic\BillMonthlyForm;
use Wamclient\CoreBundle\Form\_Statistic\BillYearlyForm;
use Wamclient\CoreBundle\Form\_Statistic\CostDailyForm;
use Wamclient\CoreBundle\Form\_Statistic\CostMonthlyForm;
use Wamclient\CoreBundle\Form\_Statistic\CostYearlyForm;
use Wamclient\CoreBundle\Form\_Statistic\CostVsBillForm;
use Wamclient\CoreBundle\Form\_Statistic\ClientDailyForm;
use Wamclient\CoreBundle\Form\_Statistic\ClientMonthlyForm;
use Wamclient\CoreBundle\Form\_Statistic\ClientYearlyForm;
use Wamclient\CoreBundle\Form\_Statistic\ServiceDailyForm;
use Wamclient\CoreBundle\Form\_Statistic\ServiceMonthlyForm;
use Wamclient\CoreBundle\Form\_Statistic\ServiceYearlyForm;

class StatisticController extends Controller
{
    public function ReviewAction(Request $request)
    {		
		$EntityManager=$this->getUser()->getDbConnect();
		$em = $this->getDoctrine()->getManager($EntityManager);
		$connection = $em->getConnection();
		
		$CurrentYear=date('Y');
		$DateToday=date('Y-m-d');
		$DateYesterday=date('Y-m-d',strtotime('-1 days'));
		$DateThisMonthStart=date('Y-m-1');
		$DateThisMonthEnd=date('Y-m-t');
		$DateLastMonthStart=date('Y-m-1',strtotime("first day of previous month"));
		$DateLastMonthEnd=date('Y-m-t',strtotime("first day of previous month"));
		
		/*BILLCHART*/
		$BillChart= array();	
		$statement = $connection->prepare("
			SELECT has_iva, COUNT(*) AS total FROM  Bill
			WHERE locked = 2 AND canceled = 1
			AND  bill_date >= '".$DateThisMonthStart." 00:00:00'
			AND  bill_date <= '".$DateThisMonthEnd." 23:59:59' 
			GROUP BY has_iva");
		$statement->execute();
		$results = $statement->fetchAll();
		foreach($results as $result):
			$IvaLabel= 'Tarifa 0%';
			if($result['has_iva']==1):$IvaLabel= 'Tarifa 12% (+)';
			elseif($result['has_iva']==2):$IvaLabel= 'Tarifa 12% (-)';endif;
		
			$BillChart[]=array($IvaLabel, $result['total'] );
		endforeach;
		
		/*BILLED*/
		$Bill= array();	$Income= array(); $IncomeChart= array();
		/*today*/
		$statement = $connection->prepare("
			SELECT COUNT(*) AS bills, COALESCE(SUM( total ),0) AS invoiced FROM  Bill
			WHERE locked = 2 AND canceled = 1
			AND  bill_date >= '".$DateToday." 00:00:00'
			AND  bill_date <= '".$DateToday." 23:59:59' ");
		$statement->execute();
		$results = $statement->fetchAll();
		
		$Bill['today']=$results[0]['bills'];
		$Income['today']=$results[0]['invoiced'];
		/*ayer*/
		$statement = $connection->prepare("
			SELECT COUNT(*) AS bills, COALESCE(SUM( total ),0) AS invoiced FROM Bill
			WHERE locked = 2 AND canceled = 1
			AND  bill_date >= '".$DateYesterday." 00:00:00'
			AND  bill_date <= '".$DateYesterday." 23:59:59' ");
		$statement->execute();
		$results = $statement->fetchAll();
		
		$Bill['yesterday']=$results[0]['bills'];
		$Income['yesterday']=$results[0]['invoiced'];
		/*este mes*/
		$statement = $connection->prepare("
			SELECT COUNT(*) AS bills, COALESCE(SUM( subtotal ),0) AS subtotal, COALESCE(SUM( iva ),0) AS iva, COALESCE(SUM( total ),0) AS invoiced
			FROM  Bill WHERE locked = 2 AND canceled = 1
			AND  bill_date >= '".$DateThisMonthStart." 00:00:00'
			AND  bill_date <= '".$DateThisMonthEnd." 23:59:59' ");
		$statement->execute();
		$results = $statement->fetchAll();
		
		$Bill['this_month']=$results[0]['bills'];
		$Income['this_month']=$results[0]['invoiced'];	
		$IncomeChart['this_month_iva']=$results[0]['iva'];	
		$IncomeChart['this_month_subtotal']=$results[0]['subtotal'];	
		/*el mes pasado*/
		$statement = $connection->prepare("
			SELECT COUNT(*) AS bills, COALESCE(SUM( total ),0) AS invoiced FROM  Bill
			WHERE locked = 2 AND canceled = 1
			AND  bill_date >= '".$DateLastMonthStart." 00:00:00'
			AND  bill_date <= '".$DateLastMonthEnd." 23:59:59' ");
		$statement->execute();
		$results = $statement->fetchAll();
		
		$Bill['last_month']=$results[0]['bills'];
		$Income['last_month']=$results[0]['invoiced'];	
				
		/*INFORMATIVE*/
		$Informative= array();
		
		/*valores por cobrar*/
		$statement = $connection->prepare("
			SELECT count(*) AS client, COALESCE(SUM( account ),0) AS total
			FROM  Client WHERE account < 0");
		$statement->execute();
		$results = $statement->fetchAll();
		
		$Informative['client_to_collect']=$results[0]['client'];	
		$Informative['to_collect']=$results[0]['total'];	
		
		/*saldos a favor*/
		$statement = $connection->prepare("
			SELECT count(*) AS client, COALESCE(SUM( account ),0) AS total
			FROM  Client WHERE account > 0");
		$statement->execute();
		$results = $statement->fetchAll();
		
		$Informative['client_in_favor']=$results[0]['client'];	
		$Informative['in_favor']=$results[0]['total'];	
		
		/*ordenes editables */
		$statement = $connection->prepare("
			SELECT COUNT(*) AS total
			FROM  Bill WHERE locked=1");
		$statement->execute();
		$results = $statement->fetchAll();
		
		$Informative['to_lock']=$results[0]['total'];	
				
		/*facturas del año*/
		$statement = $connection->prepare("
			SELECT COUNT(*) AS bill_count, COALESCE(SUM( subtotal ),0) AS subtotal, COALESCE(SUM( iva ),0) AS iva, COALESCE(SUM( total ),0) AS total
			FROM  Bill
			WHERE locked = 2 AND canceled = 1
			AND YEAR(bill_date) =".$CurrentYear);
		$statement->execute();
		$results = $statement->fetchAll();
		
		$Informative['bill_count']=$results[0]['bill_count'];			
		$Informative['bill_subtotal']=$results[0]['subtotal'];	
		$Informative['bill_iva']=$results[0]['iva'];	
		$Informative['bill_total']=$results[0]['total'];	
				
		//rendering template
		return $this->render('WamclientAdminBundle:Statistic:Review.html.twig',array(
			'Income'=>$Income,			
			'IncomeChart'=>$IncomeChart,			
			'Bill'=>$Bill,			
			'BillChart'=>$BillChart,		
			'Informative'=>$Informative,	
			'CurrentYear'=>$CurrentYear,	
		));
	}
	
    public function BillDailyAction(Request $request)
    {
		//init list session
		$session = $request->getSession();
		$StatisticSession='StatisticAdminBillDaily';
		$EntityManager=$this->getUser()->getDbConnect();
		$em = $this->getDoctrine()->getManager($EntityManager);
		$connection = $em->getConnection();
		
		//preparing filter form 
    	$StatisticForm = $this->createForm(new BillDailyForm(), $session->get($StatisticSession) );
		
		//check if list has filter form
		if ($request->isMethod('POST')):
			$StatisticForm->bind($request);
			$this->ProcessFilter($StatisticForm, $StatisticSession);//processing filter POST
		endif;
		$DateSession=$session->get($StatisticSession) ;
		
		
		/******* STABLISH DATE PERIOD - DAILY *******/		
		$DateStart=date('Y-m-d',strtotime('-6 days'));
		$DateEnd=date('Y-m-d'); $FastDate='Últimos 7 días';
		switch ($DateSession['fast_date_selector']) {
			case 2:
				$DateStart=date('Y-m-d',strtotime('-14 days'));
				$DateEnd=date('Y-m-d'); $FastDate='Últimos 15 días';
				break;
			case 3:
				$DateStart=date('Y-m-d',strtotime('-29 days'));
				$DateEnd=date('Y-m-d'); $FastDate='Últimos 30 días';
				break;
			case 4:
				$DateStart=date('Y-m-1');
				$DateEnd=date('Y-m-d'); $FastDate='Este mes';
				break;
			case 5:
				$DateStart=date('Y-m-1',strtotime("first day of previous month"));
				$DateEnd=date('Y-m-t',strtotime("first day of previous month")); $FastDate='El mes pasado';
				break;
		}
		$EndPeriod=new \DateTime($DateEnd); $EndPeriod = $EndPeriod->modify( '+1 day' );
		$DatePeriod = new \DatePeriod(new \DateTime($DateStart), new \DateInterval('P1D'), $EndPeriod);
					
		$BillValues_Line= array();
		$BillsByDay_Table=array();
		foreach($DatePeriod as $date):
			$BillValues_Line[$date->format("Y-m-d")]=array('label'=>$date->format('j M'), 'count'=>0, 'subtotal'=>0, 'iva'=>0, 'total'=>0 );
			$BillsByDay_Table[$date->format('Y-m-d')]=array(	'bill_date' => $date->format('Y-m-d'), 'bill_count'=>0, 'subtotal'=>0, 'iva'=>0, 'total'=>0);
		endforeach;
		
		
		/*** START | BILL VALUES - LINE CHART ***/
		$statement = $connection->prepare("
			SELECT YEAR(bill_date) AS year, MONTH(bill_date) AS month, DAY(bill_date) AS day, COUNT(*) AS bill_count, COALESCE(SUM( subtotal ),0) AS subtotal, COALESCE(SUM( iva ),0) AS iva, COALESCE(SUM( total ),0) AS total
			FROM  Bill
			WHERE locked = 2
			AND canceled = 1
			AND  bill_date >= '".$DateStart." 00:00:00'
			AND  bill_date <= '".$DateEnd." 23:59:59' 
			GROUP BY MONTH(bill_date), DAY(bill_date)");
		$statement->execute();
		$results = $statement->fetchAll();
				
		if(count($results)>0):
			foreach($results as $result):
				$date_key=new \DateTime($result['year'].'-'.$result['month'].'-'.$result['day']);
				
				$BillValues_Line[$date_key->format('Y-m-d')]=array('label'=>$date_key->format('j M'), 'count'=>$result['bill_count'], 'subtotal'=>$result['subtotal'], 'iva'=>$result['iva'], 'total'=>$result['total'] );
			endforeach;
		endif;
		/*** END | BILL VALUES - LINE CHART ***/
		
		/*** START | BILL VALUES - PIE CHART ***/
		$BillValues_Pie= array();
		$statement = $connection->prepare("
			SELECT COUNT(*) AS bill_count, COALESCE(SUM( subtotal ),0) AS subtotal, COALESCE(SUM( iva ),0) AS iva, COALESCE(SUM( total ),0) AS total
			FROM  Bill
			WHERE locked = 2
			AND canceled = 1
			AND  bill_date >= '".$DateStart." 00:00:00'
			AND  bill_date <= '".$DateEnd." 23:59:59' ");
		$statement->execute();
		$results = $statement->fetchAll();
		
		if($results[0]['bill_count']>0):
			$BillValues_Pie['bill_count']=$results[0]['bill_count'];	
			$BillValues_Pie['bill_subtotal']=$results[0]['subtotal'];	
			$BillValues_Pie['bill_iva']=$results[0]['iva'];	
			$BillValues_Pie['bill_total']=$results[0]['total'];	
		endif;
		/*** END | BILL VALUES - PIE CHART ***/
		
		/*** START | BILL TYPES - PIE CHART ***/
		$BillTypes_Pie= array();
		$statement = $connection->prepare("
			SELECT has_iva, COUNT(*) AS bill_count, COALESCE(SUM( subtotal ),0) AS subtotal, COALESCE(SUM( iva ),0) AS iva, COALESCE(SUM( total ),0) AS total
			FROM  Bill
			WHERE locked = 2
			AND canceled = 1
			AND  bill_date >= '".$DateStart." 00:00:00'
			AND  bill_date <= '".$DateEnd." 23:59:59' 
			GROUP BY has_iva");
		$statement->execute();
		$results = $statement->fetchAll();
		
		if(count($results)>0):
			foreach($results as $result):
				$IvaLabel= 'Tarifa 0%';
				if($result['has_iva']==1):$IvaLabel= 'Tarifa 12% (+)';
				elseif($result['has_iva']==2):$IvaLabel= 'Tarifa 12% (-)';endif;
			
				$BillTypes_Pie[]=array($IvaLabel, $result['bill_count'], $result['subtotal'], $result['iva'], $result['total']);
			endforeach;
		endif;
		
		/*** END | BILL TYPES - PIE CHART ***/
		
		/*** START - BILLS BY DAY - TABLE ORDERED ***/
		$statement = $connection->prepare("
			SELECT 
				YEAR(b.bill_date) AS year, MONTH(b.bill_date) AS month, DAY(b.bill_date) AS day,
				COUNT(*) AS bill_count, 
				COALESCE(SUM( b.subtotal ),0) AS subtotal,  COALESCE(SUM( b.iva ),0) AS iva,  COALESCE(SUM( b.total ),0) AS total
			FROM Bill b
			WHERE b.locked = 2
			AND b.canceled = 1
			AND b.bill_date >= '".$DateStart." 00:00:00'
			AND b.bill_date <= '".$DateEnd." 23:59:59' 
			GROUP BY MONTH(bill_date), DAY(bill_date)
			ORDER BY bill_date DESC");
		$statement->execute();
		$results = $statement->fetchAll();
		
		$Subtotal=0; $IVA=0; $Total=0; $BillCount=0; 
		$BillsByDay_Total=array('subtotal'=> 0, 'iva'=> 0, 'total'=> 0, 'bill_count'=> 0, 'count'=> 0);
				
		if(count($results)>0):
			foreach($results as $result):
				$Subtotal=$Subtotal+$result['subtotal']; $IVA=$IVA+$result['iva']; 
				$Total=$Total+$result['total']; $BillCount=$BillCount+$result['bill_count']; 
				$date_key=new \DateTime($result['year'].'-'.$result['month'].'-'.$result['day']);
								
				$BillsByDay_Table[$date_key->format('Y-m-d')]=array(
					'bill_date' => $date_key->format('Y-m-d'),
					'bill_count'=>$result['bill_count'],
					'subtotal'=>$result['subtotal'],
					'iva'=>$result['iva'],
					'total'=>$result['total'],
				);
			endforeach;
			$BillsByDay_Total['subtotal']= $Subtotal;
			$BillsByDay_Total['iva']= $IVA;
			$BillsByDay_Total['total']= $Total;
			$BillsByDay_Total['bill_count']= $BillCount;
			$BillsByDay_Total['count']= count($BillsByDay_Table);
		endif;
		/*** END - RECURRENT CLIENT - TABLE ORDERED ***/
		
		//rendering template
		return $this->render('WamclientAdminBundle:Statistic:BillDaily.html.twig',array(
        	'StatisticForm'=>$StatisticForm->createView(),
        	'BillValues_Pie'=>$BillValues_Pie, 
        	'BillTypes_Pie'=>$BillTypes_Pie, 
        	'BillValues_Line'=>$BillValues_Line, 
        	'BillsByDay_Table'=>$BillsByDay_Table, 
        	'BillsByDay_Total'=>$BillsByDay_Total, 
			'FastDate'=>$FastDate,
        ));
		
	}
	
    public function BillMonthlyAction(Request $request)
    {
		//init list session
		$session = $request->getSession();
		$StatisticSession='StatisticAdminBillMontly';
		$EntityManager=$this->getUser()->getDbConnect();
		$em = $this->getDoctrine()->getManager($EntityManager);
		$connection = $em->getConnection();
		
		$statement = $connection->prepare("
			SELECT  YEAR(bill_date) AS year, MONTH(bill_date) AS month FROM  Bill
			WHERE canceled = 1 AND locked= 2 GROUP BY YEAR(bill_date), MONTH(bill_date)  ORDER BY bill_date DESC");
		$statement->execute();
		$MonthArray = $statement->fetchAll();
		
		//preparing filter form 
    	$StatisticForm = $this->createForm(new BillMonthlyForm($MonthArray), $session->get($StatisticSession) );
		
		//check if list has filter form
		if ($request->isMethod('POST')):
			$StatisticForm->bind($request);
			$this->ProcessFilter($StatisticForm, $StatisticSession);//processing filter POST
		endif;
		$DateSession=$session->get($StatisticSession) ;
		
		
		/******* STABLISH DATE PERIOD - MONTHLY *******/		
		$spn_month= array('0', 'Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo', 'Junio', 'Julio', 'Agosto', 'Septiembre', 'Octubre', 'Noviembre', 'Diciembre');
		
		$DateStart=date('Y-m-1',strtotime('now'));
		$DateEnd=date('Y-m-d',strtotime('now'));
			
		$FastDate= $spn_month[date('n')].', '.date('Y');
		
		if($DateSession['month_selector']) {
			$MonthSelected=explode('_', $DateSession['month_selector']);
			
			$DateStart=date('Y-m-1',strtotime($MonthSelected[0].'-'.$MonthSelected[1].'-01'));
			if(date('n')==$MonthSelected[1] && date('Y')==$MonthSelected[0]):
				$DateEnd=date('Y-m-d',strtotime('now'));
			else:
				$DateEnd=date('Y-m-t',strtotime($MonthSelected[0].'-'.$MonthSelected[1].'-01'));
			endif;
			$FastDate= $spn_month[$MonthSelected[1]].', '.$MonthSelected[0];
		}
		$EndPeriod=new \DateTime($DateEnd); $EndPeriod = $EndPeriod->modify( '+1 day' );
		$DatePeriod = new \DatePeriod(new \DateTime($DateStart), new \DateInterval('P1D'), $EndPeriod);
					
		$BillValues_Line= array();
		$BillsByDay_Table=array();
		foreach($DatePeriod as $date):
			$BillValues_Line[$date->format("Y-m-d")]=array('label'=>$date->format('j M'), 'count'=>0, 'subtotal'=>0, 'iva'=>0, 'total'=>0 );
			$BillsByDay_Table[$date->format('Y-m-d')]=array('bill_date'=>$date->format('Y-m-d'), 'bill_count'=>0, 'subtotal'=>0, 'iva'=>0, 'total'=>0);
		endforeach;
		
		
		/*** START | BILL VALUES - LINE CHART ***/
		$statement = $connection->prepare("
			SELECT YEAR(bill_date) AS year, MONTH(bill_date) AS month, DAY(bill_date) AS day, COUNT(*) AS bill_count, COALESCE(SUM( subtotal ),0) AS subtotal, COALESCE(SUM( iva ),0) AS iva, COALESCE(SUM( total ),0) AS total
			FROM  Bill
			WHERE locked = 2
			AND canceled = 1
			AND  bill_date >= '".$DateStart." 00:00:00'
			AND  bill_date <= '".$DateEnd." 23:59:59' 
			GROUP BY MONTH(bill_date), DAY(bill_date)");
		$statement->execute();
		$results = $statement->fetchAll();
				
		if(count($results)>0):
			foreach($results as $result):
				$date_key=new \DateTime($result['year'].'-'.$result['month'].'-'.$result['day']);
				
				$BillValues_Line[$date_key->format('Y-m-d')]=array('label'=>$date_key->format('j M'), 'count'=>$result['bill_count'], 'subtotal'=>$result['subtotal'], 'iva'=>$result['iva'], 'total'=>$result['total'] );
			endforeach;
		endif;
		/*** END | BILL VALUES - LINE CHART ***/
		
		/*** START | BILL VALUES - PIE CHART ***/
		$BillValues_Pie= array();
		$statement = $connection->prepare("
			SELECT COUNT(*) AS bill_count, COALESCE(SUM( subtotal ),0) AS subtotal, COALESCE(SUM( iva ),0) AS iva, COALESCE(SUM( total ),0) AS total
			FROM  Bill
			WHERE locked = 2
			AND canceled = 1
			AND  bill_date >= '".$DateStart." 00:00:00'
			AND  bill_date <= '".$DateEnd." 23:59:59' ");
		$statement->execute();
		$results = $statement->fetchAll();
		
		if($results[0]['bill_count']>0):
			$BillValues_Pie['bill_count']=$results[0]['bill_count'];	
			$BillValues_Pie['bill_subtotal']=$results[0]['subtotal'];	
			$BillValues_Pie['bill_iva']=$results[0]['iva'];	
			$BillValues_Pie['bill_total']=$results[0]['total'];	
		endif;
		/*** END | BILL VALUES - PIE CHART ***/
		
		/*** START | BILL TYPES - PIE CHART ***/
		$BillTypes_Pie= array();
		$statement = $connection->prepare("
			SELECT has_iva, COUNT(*) AS bill_count, COALESCE(SUM( subtotal ),0) AS subtotal, COALESCE(SUM( iva ),0) AS iva, COALESCE(SUM( total ),0) AS total
			FROM  Bill
			WHERE locked = 2
			AND canceled = 1
			AND  bill_date >= '".$DateStart." 00:00:00'
			AND  bill_date <= '".$DateEnd." 23:59:59' 
			GROUP BY has_iva");
		$statement->execute();
		$results = $statement->fetchAll();
		
		if(count($results)>0):
			foreach($results as $result):
				$IvaLabel= 'Tarifa 0%';
				if($result['has_iva']==1):$IvaLabel= 'Tarifa 12% (+)';
				elseif($result['has_iva']==2):$IvaLabel= 'Tarifa 12% (-)';endif;
			
				$BillTypes_Pie[]=array($IvaLabel, $result['bill_count'], $result['subtotal'], $result['iva'], $result['total']);
			endforeach;
		endif;
		/*** END | BILL TYPES - PIE CHART ***/
		
		/*** START - BILLS BY DAY - TABLE ORDERED ***/
		$statement = $connection->prepare("
			SELECT 
				YEAR(b.bill_date) AS year, MONTH(b.bill_date) AS month, DAY(b.bill_date) AS day,
				COUNT(*) AS bill_count, 
				COALESCE(SUM( b.subtotal ),0) AS subtotal,  COALESCE(SUM( b.iva ),0) AS iva,  COALESCE(SUM( b.total ),0) AS total
			FROM Bill b
			WHERE b.locked = 2
			AND b.canceled = 1
			AND b.bill_date >= '".$DateStart." 00:00:00'
			AND b.bill_date <= '".$DateEnd." 23:59:59' 
			GROUP BY MONTH(bill_date), DAY(bill_date)
			ORDER BY bill_date DESC");
		$statement->execute();
		$results = $statement->fetchAll();
		
		$Subtotal=0; $IVA=0; $Total=0; $BillCount=0; 
		$BillsByDay_Total=array('subtotal'=> 0, 'iva'=> 0, 'total'=> 0, 'bill_count'=> 0, 'count'=> 0);
				
		if(count($results)>0):
			foreach($results as $result):
				$Subtotal=$Subtotal+$result['subtotal']; $IVA=$IVA+$result['iva']; 
				$Total=$Total+$result['total']; $BillCount=$BillCount+$result['bill_count']; 
				$date_key=new \DateTime($result['year'].'-'.$result['month'].'-'.$result['day']);
								
				$BillsByDay_Table[$date_key->format('Y-m-d')]=array(
					'bill_date' => $date_key->format('Y-m-d'),
					'bill_count'=>$result['bill_count'],
					'subtotal'=>$result['subtotal'],
					'iva'=>$result['iva'],
					'total'=>$result['total'],
				);
			endforeach;
			$BillsByDay_Total['subtotal']= $Subtotal;
			$BillsByDay_Total['iva']= $IVA;
			$BillsByDay_Total['total']= $Total;
			$BillsByDay_Total['bill_count']= $BillCount;
			$BillsByDay_Total['count']= count($BillsByDay_Table);
		endif;
		/*** END - RECURRENT CLIENT - TABLE ORDERED ***/
		
		//rendering template
		return $this->render('WamclientAdminBundle:Statistic:BillMonthly.html.twig',array(
        	'StatisticForm'=>$StatisticForm->createView(),
        	'BillValues_Pie'=>$BillValues_Pie, 
        	'BillTypes_Pie'=>$BillTypes_Pie, 
        	'BillValues_Line'=>$BillValues_Line, 
        	'BillsByDay_Table'=>$BillsByDay_Table, 
        	'BillsByDay_Total'=>$BillsByDay_Total, 
			'FastDate'=>$FastDate,
        ));
				
	}
	
	public function BillYearlyAction(Request $request)
    {
		//init list session
		$session = $request->getSession();
		$StatisticSession='StatisticAdminBillYearly';
		$EntityManager=$this->getUser()->getDbConnect();
		$em = $this->getDoctrine()->getManager($EntityManager);
		$connection = $em->getConnection();
		
		$statement = $connection->prepare("
			SELECT  YEAR(bill_date) AS year FROM  Bill
			WHERE canceled = 1 AND locked= 2 GROUP BY YEAR(bill_date) ORDER BY YEAR(bill_date) DESC");
		$statement->execute();
		$YearArray = $statement->fetchAll();
				
		//preparing filter form 
    	$StatisticForm = $this->createForm(new BillYearlyForm($YearArray), $session->get($StatisticSession) );
		
		//check if list has filter form
		if ($request->isMethod('POST')):
			$StatisticForm->bind($request);
			$this->ProcessFilter($StatisticForm, $StatisticSession);//processing filter POST
		endif;
		$DateSession=$session->get($StatisticSession) ;
		
		/******* STABLISH DATE PERIOD - MONTLY BY YEAR *******/		
		
		$YearStart=date('Y-01-1');
		$YearEnd=date('Y-m-d'); $YearDate='Periodo '.date('Y');
		
		if($DateSession['year_selector'] && $DateSession['year_selector'] != date('Y') ):
			$YearStart=date($DateSession['year_selector'].'-01-1');
			$YearEnd=date($DateSession['year_selector'].'-12-31'); $YearDate='Periodo '.$DateSession['year_selector'] ;
		endif;
		
		$YearEndPeriod=new \DateTime($YearEnd);
		$YearPeriod = new \DatePeriod(new \DateTime($YearStart), new \DateInterval('P1M'), $YearEndPeriod);
				
		$BillYear_Line= array();
		$BillsByMonth_Table= array();
		foreach($YearPeriod as $month):
			$BillYear_Line[$month->format("m")]=array('label'=>$month->format('F'), 'count'=>0, 'subtotal'=>0, 'iva'=>0, 'total'=>0 );
			$BillsByMonth_Table[$month->format('m')]=array('bill_date'=>$month->format('Y-m'), 'bill_count'=>0, 'subtotal'=>0, 'iva'=>0, 'total'=>0);
		endforeach;
		
		/*** BILL YEAR - LINE CHART ***/
		$statement = $connection->prepare("
			SELECT YEAR(bill_date) AS year, MONTH(bill_date) AS month, COUNT(*) AS bill_count, COALESCE(SUM( subtotal ),0) AS subtotal, COALESCE(SUM( iva ),0) AS iva, COALESCE(SUM( total ),0) AS total
			FROM  Bill
			WHERE locked = 2
			AND canceled = 1
			AND  bill_date >= '".$YearStart." 00:00:00'
			AND  bill_date <= '".$YearEnd." 23:59:59' 
			GROUP BY YEAR(bill_date), MONTH(bill_date)");
		$statement->execute();
		$results = $statement->fetchAll();
				
		if(count($results)>0):
			foreach($results as $result):
				$month_key=new \DateTime($result['year'].'-'.$result['month']);
				$BillYear_Line[$month_key->format('m')]=array('label'=>$month_key->format('F'), 'count'=>$result['bill_count'], 'subtotal'=>$result['subtotal'], 'iva'=>$result['iva'], 'total'=>$result['total'] );
			endforeach;
		endif;
				
		/*** BILL VALUES - PIE CHART ***/
		$BillValues_Pie= array();
		$statement = $connection->prepare("
			SELECT COUNT(*) AS bill_count, COALESCE(SUM( subtotal ),0) AS subtotal, COALESCE(SUM( iva ),0) AS iva, COALESCE(SUM( total ),0) AS total
			FROM  Bill
			WHERE locked = 2
			AND canceled = 1
			AND  bill_date >= '".$YearStart." 00:00:00'
			AND  bill_date <= '".$YearEnd." 23:59:59' ");
		$statement->execute();
		$results = $statement->fetchAll();
		
		if($results[0]['bill_count']>0):
			$BillValues_Pie['bill_count']=$results[0]['bill_count'];	
			$BillValues_Pie['bill_subtotal']=$results[0]['subtotal'];	
			$BillValues_Pie['bill_iva']=$results[0]['iva'];	
			$BillValues_Pie['bill_total']=$results[0]['total'];	
		endif;
		
		/*** BILL TYPES - PIE CHART ***/
		$BillTypes_Pie= array();
		$statement = $connection->prepare("
			SELECT has_iva, COUNT(*) AS bill_count, COALESCE(SUM( subtotal ),0) AS subtotal, COALESCE(SUM( iva ),0) AS iva, COALESCE(SUM( total ),0) AS total
			FROM  Bill
			WHERE locked = 2
			AND canceled = 1
			AND  bill_date >= '".$YearStart." 00:00:00'
			AND  bill_date <= '".$YearEnd." 23:59:59' 
			GROUP BY has_iva");
		$statement->execute();
		$results = $statement->fetchAll();
		
		if(count($results)>0):
			foreach($results as $result):
				$IvaLabel= 'Tarifa 0%';
				if($result['has_iva']==1):$IvaLabel= 'Tarifa 12% (+)';
				elseif($result['has_iva']==2):$IvaLabel= 'Tarifa 12% (-)';endif;
			
				$BillTypes_Pie[]=array($IvaLabel, $result['bill_count'], $result['subtotal'], $result['iva'], $result['total']);
			endforeach;
		endif;
				
		/*** START - BILLS BY MONTH - TABLE ORDERED ***/
		$statement = $connection->prepare("
			SELECT 
				YEAR(b.bill_date) AS year, MONTH(b.bill_date) AS month,
				COUNT(*) AS bill_count, 
				COALESCE(SUM( b.subtotal ),0) AS subtotal,  COALESCE(SUM( b.iva ),0) AS iva,  COALESCE(SUM( b.total ),0) AS total
			FROM Bill b
			WHERE b.locked = 2
			AND b.canceled = 1
			AND b.bill_date >= '".$YearStart." 00:00:00'
			AND b.bill_date <= '".$YearEnd." 23:59:59' 
			GROUP BY YEAR(bill_date), MONTH(bill_date)
			ORDER BY bill_date DESC");
		$statement->execute();
		$results = $statement->fetchAll();
		
		$Subtotal=0; $IVA=0; $Total=0; $BillCount=0; 
		$BillsByMonth_Total=array('subtotal'=> 0, 'iva'=> 0, 'total'=> 0, 'bill_count'=> 0, 'count'=> 0);
				
		if(count($results)>0):
			foreach($results as $result):
				$Subtotal=$Subtotal+$result['subtotal']; $IVA=$IVA+$result['iva']; 
				$Total=$Total+$result['total']; $BillCount=$BillCount+$result['bill_count']; 
				$date_key=new \DateTime($result['year'].'-'.$result['month'].'-1');
								
				$BillsByMonth_Table[$date_key->format('m')]=array(
					'bill_date' => $date_key->format('Y-m'),
					'bill_count'=>$result['bill_count'],
					'subtotal'=>$result['subtotal'],
					'iva'=>$result['iva'],
					'total'=>$result['total'],
				);
			endforeach;
			$BillsByMonth_Total['subtotal']= $Subtotal;
			$BillsByMonth_Total['iva']= $IVA;
			$BillsByMonth_Total['total']= $Total;
			$BillsByMonth_Total['bill_count']= $BillCount;
			$BillsByMonth_Total['count']= count($BillsByMonth_Table);
		endif;
		/*** END - RECURRENT CLIENT - TABLE ORDERED ***/
		
		//rendering template
		return $this->render('WamclientAdminBundle:Statistic:BillYearly.html.twig',array(
        	'StatisticForm'=>$StatisticForm->createView(),
        	'BillValues_Pie'=>$BillValues_Pie, 
        	'BillTypes_Pie'=>$BillTypes_Pie, 
			'BillYear_Line'=>$BillYear_Line,
        	'BillsByMonth_Table'=>$BillsByMonth_Table, 
        	'BillsByMonth_Total'=>$BillsByMonth_Total, 
			'YearDate'=> $YearDate
        ));
		
	}
    	
    public function CostDailyAction(Request $request)
    {
		//init list session
		$session = $request->getSession();
		$StatisticSession='StatisticAdminCostDaily';
		$EntityManager=$this->getUser()->getDbConnect();
		$em = $this->getDoctrine()->getManager($EntityManager);
		$connection = $em->getConnection();
		
		//preparing filter form 
    	$StatisticForm = $this->createForm(new CostDailyForm(), $session->get($StatisticSession) );
		
		//check if list has filter form
		if ($request->isMethod('POST')):
			$StatisticForm->bind($request);
			$this->ProcessFilter($StatisticForm, $StatisticSession);//processing filter POST
		endif;
		$DateSession=$session->get($StatisticSession) ;
		
		
		/******* STABLISH DATE PERIOD - DAILY *******/		
		$DateStart=date('Y-m-d',strtotime('-6 days'));
		$DateEnd=date('Y-m-d'); $FastDate='Últimos 7 días';
		switch ($DateSession['fast_date_selector']) {
			case 2:
				$DateStart=date('Y-m-d',strtotime('-14 days'));
				$DateEnd=date('Y-m-d'); $FastDate='Últimos 15 días';
				break;
			case 3:
				$DateStart=date('Y-m-d',strtotime('-29 days'));
				$DateEnd=date('Y-m-d'); $FastDate='Últimos 30 días';
				break;
			case 4:
				$DateStart=date('Y-m-1');
				$DateEnd=date('Y-m-d'); $FastDate='Este mes';
				break;
			case 5:
				$DateStart=date('Y-m-1',strtotime("first day of previous month"));
				$DateEnd=date('Y-m-t',strtotime("first day of previous month")); $FastDate='El mes pasado';
				break;
		}
		$EndPeriod=new \DateTime($DateEnd); $EndPeriod = $EndPeriod->modify( '+1 day' );
		$DatePeriod = new \DatePeriod(new \DateTime($DateStart), new \DateInterval('P1D'), $EndPeriod);
					
		$CostValues_Line= array();
		$CostsByDay_Table=array();
		foreach($DatePeriod as $date):
			$CostValues_Line[$date->format("Y-m-d")]=array('label'=>$date->format('j M'), 'count'=>0, 'subtotal'=>0, 'iva'=>0, 'total'=>0 );
			$CostsByDay_Table[$date->format('Y-m-d')]=array(	'cost_date' => $date->format('Y-m-d'), 'cost_count'=>0, 'subtotal'=>0, 'iva'=>0, 'total'=>0);
		endforeach;
		
		
		/*** START | Cost VALUES - LINE CHART ***/
		$statement = $connection->prepare("
			SELECT YEAR(cost_date) AS year, MONTH(cost_date) AS month, DAY(cost_date) AS day, COUNT(*) AS cost_count, COALESCE(SUM( subtotal ),0) AS subtotal, COALESCE(SUM( iva ),0) AS iva, COALESCE(SUM( total ),0) AS total
			FROM  Cost
			WHERE locked = 2
			AND canceled = 1
			AND  cost_date >= '".$DateStart." 00:00:00'
			AND  cost_date <= '".$DateEnd." 23:59:59' 
			GROUP BY MONTH(cost_date), DAY(cost_date)");
		$statement->execute();
		$results = $statement->fetchAll();
				
		if(count($results)>0):
			foreach($results as $result):
				$date_key=new \DateTime($result['year'].'-'.$result['month'].'-'.$result['day']);
				
				$CostValues_Line[$date_key->format('Y-m-d')]=array('label'=>$date_key->format('j M'), 'count'=>$result['cost_count'], 'subtotal'=>$result['subtotal'], 'iva'=>$result['iva'], 'total'=>$result['total'] );
			endforeach;
		endif;
		/*** END | Cost VALUES - LINE CHART ***/
		
		/*** START | Cost VALUES - PIE CHART ***/
		$CostValues_Pie= array();
		$statement = $connection->prepare("
			SELECT COUNT(*) AS cost_count, COALESCE(SUM( subtotal ),0) AS subtotal, COALESCE(SUM( iva ),0) AS iva, COALESCE(SUM( total ),0) AS total
			FROM  Cost
			WHERE locked = 2
			AND canceled = 1
			AND  cost_date >= '".$DateStart." 00:00:00'
			AND  cost_date <= '".$DateEnd." 23:59:59' ");
		$statement->execute();
		$results = $statement->fetchAll();
		
		if($results[0]['cost_count']>0):
			$CostValues_Pie['cost_count']=$results[0]['cost_count'];	
			$CostValues_Pie['cost_subtotal']=$results[0]['subtotal'];	
			$CostValues_Pie['cost_iva']=$results[0]['iva'];	
			$CostValues_Pie['cost_total']=$results[0]['total'];	
		endif;
		/*** END | Cost VALUES - PIE CHART ***/
		
		/*** START | Cost TYPES - PIE CHART ***/
		$CostTypes_Pie= array();
		$statement = $connection->prepare("
			SELECT COUNT(*) AS cost_count, COALESCE(SUM( subtotal ),0) AS subtotal, COALESCE(SUM( iva ),0) AS iva, COALESCE(SUM( total ),0) AS total
			FROM  Cost
			WHERE locked = 2
			AND canceled = 1
			AND  cost_date >= '".$DateStart." 00:00:00'
			AND  cost_date <= '".$DateEnd." 23:59:59' ");
		$statement->execute();
		$results = $statement->fetchAll();
		
		if(count($results)>0):
			foreach($results as $result):
				$IvaLabel= 'Gastos';
			
				$CostTypes_Pie[]=array($IvaLabel, $result['cost_count'], $result['subtotal'], $result['iva'], $result['total']);
			endforeach;
		endif;
		
		/*** END | Cost TYPES - PIE CHART ***/
		
		/*** START - CostS BY DAY - TABLE ORDERED ***/
		$statement = $connection->prepare("
			SELECT 
				YEAR(b.cost_date) AS year, MONTH(b.cost_date) AS month, DAY(b.cost_date) AS day,
				COUNT(*) AS cost_count, 
				COALESCE(SUM( b.subtotal ),0) AS subtotal,  COALESCE(SUM( b.iva ),0) AS iva,  COALESCE(SUM( b.total ),0) AS total
			FROM Cost b
			WHERE b.locked = 2
			AND b.canceled = 1
			AND b.cost_date >= '".$DateStart." 00:00:00'
			AND b.cost_date <= '".$DateEnd." 23:59:59' 
			GROUP BY MONTH(cost_date), DAY(cost_date)
			ORDER BY cost_date DESC");
		$statement->execute();
		$results = $statement->fetchAll();
		
		$Subtotal=0; $IVA=0; $Total=0; $CostCount=0; 
		$CostsByDay_Total=array('subtotal'=> 0, 'iva'=> 0, 'total'=> 0, 'cost_count'=> 0, 'count'=> 0);
				
		if(count($results)>0):
			foreach($results as $result):
				$Subtotal=$Subtotal+$result['subtotal']; $IVA=$IVA+$result['iva']; 
				$Total=$Total+$result['total']; $CostCount=$CostCount+$result['cost_count']; 
				$date_key=new \DateTime($result['year'].'-'.$result['month'].'-'.$result['day']);
								
				$CostsByDay_Table[$date_key->format('Y-m-d')]=array(
					'cost_date' => $date_key->format('Y-m-d'),
					'cost_count'=>$result['cost_count'],
					'subtotal'=>$result['subtotal'],
					'iva'=>$result['iva'],
					'total'=>$result['total'],
				);
			endforeach;
			$CostsByDay_Total['subtotal']= $Subtotal;
			$CostsByDay_Total['iva']= $IVA;
			$CostsByDay_Total['total']= $Total;
			$CostsByDay_Total['cost_count']= $CostCount;
			$CostsByDay_Total['count']= count($CostsByDay_Table);
		endif;
		/*** END - RECURRENT CLIENT - TABLE ORDERED ***/
		
		//rendering template
		return $this->render('WamclientAdminBundle:Statistic:CostDaily.html.twig',array(
        	'StatisticForm'=>$StatisticForm->createView(),
        	'CostValues_Pie'=>$CostValues_Pie, 
        	'CostTypes_Pie'=>$CostTypes_Pie, 
        	'CostValues_Line'=>$CostValues_Line, 
        	'CostsByDay_Table'=>$CostsByDay_Table, 
        	'CostsByDay_Total'=>$CostsByDay_Total, 
			'FastDate'=>$FastDate,
        ));
		
	}
	
	public function CostMonthlyAction(Request $request)
    {
		//init list session
		$session = $request->getSession();
		$StatisticSession='StatisticAdminCostMontly';
		$EntityManager=$this->getUser()->getDbConnect();
		$em = $this->getDoctrine()->getManager($EntityManager);
		$connection = $em->getConnection();
		
		$statement = $connection->prepare("
			SELECT  YEAR(cost_date) AS year, MONTH(cost_date) AS month FROM  Cost
			WHERE canceled = 1 AND locked= 2 GROUP BY YEAR(cost_date), MONTH(cost_date)  ORDER BY cost_date DESC");
		$statement->execute();
		$MonthArray = $statement->fetchAll();
		
		//preparing filter form 
    	$StatisticForm = $this->createForm(new CostMonthlyForm($MonthArray), $session->get($StatisticSession) );
		
		//check if list has filter form
		if ($request->isMethod('POST')):
			$StatisticForm->bind($request);
			$this->ProcessFilter($StatisticForm, $StatisticSession);//processing filter POST
		endif;
		$DateSession=$session->get($StatisticSession) ;
		
		
		/******* STABLISH DATE PERIOD - MONTHLY *******/		
		$spn_month= array('0', 'Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo', 'Junio', 'Julio', 'Agosto', 'Septiembre', 'Octubre', 'Noviembre', 'Diciembre');
		
		$DateStart=date('Y-m-1',strtotime('now'));
		$DateEnd=date('Y-m-d',strtotime('now'));
			
		$FastDate= $spn_month[date('n')].', '.date('Y');
		
		if($DateSession['month_selector']) {
			$MonthSelected=explode('_', $DateSession['month_selector']);
			
			$DateStart=date('Y-m-1',strtotime($MonthSelected[0].'-'.$MonthSelected[1].'-01'));
			if(date('n')==$MonthSelected[1] && date('Y')==$MonthSelected[0]):
				$DateEnd=date('Y-m-d',strtotime('now'));
			else:
				$DateEnd=date('Y-m-t',strtotime($MonthSelected[0].'-'.$MonthSelected[1].'-01'));
			endif;
			$FastDate= $spn_month[$MonthSelected[1]].', '.$MonthSelected[0];
		}
		$EndPeriod=new \DateTime($DateEnd); $EndPeriod = $EndPeriod->modify( '+1 day' );
		$DatePeriod = new \DatePeriod(new \DateTime($DateStart), new \DateInterval('P1D'), $EndPeriod);
					
		$CostValues_Line= array();
		$CostsByDay_Table=array();
		foreach($DatePeriod as $date):
			$CostValues_Line[$date->format("Y-m-d")]=array('label'=>$date->format('j M'), 'count'=>0, 'subtotal'=>0, 'iva'=>0, 'total'=>0 );
			$CostsByDay_Table[$date->format('Y-m-d')]=array('cost_date'=>$date->format('Y-m-d'), 'cost_count'=>0, 'subtotal'=>0, 'iva'=>0, 'total'=>0);
		endforeach;
		
		
		/*** START | Cost VALUES - LINE CHART ***/
		$statement = $connection->prepare("
			SELECT YEAR(cost_date) AS year, MONTH(cost_date) AS month, DAY(cost_date) AS day, COUNT(*) AS cost_count, COALESCE(SUM( subtotal ),0) AS subtotal, COALESCE(SUM( iva ),0) AS iva, COALESCE(SUM( total ),0) AS total
			FROM  Cost
			WHERE locked = 2
			AND canceled = 1
			AND  cost_date >= '".$DateStart." 00:00:00'
			AND  cost_date <= '".$DateEnd." 23:59:59' 
			GROUP BY MONTH(cost_date), DAY(cost_date)");
		$statement->execute();
		$results = $statement->fetchAll();
				
		if(count($results)>0):
			foreach($results as $result):
				$date_key=new \DateTime($result['year'].'-'.$result['month'].'-'.$result['day']);
				
				$CostValues_Line[$date_key->format('Y-m-d')]=array('label'=>$date_key->format('j M'), 'count'=>$result['cost_count'], 'subtotal'=>$result['subtotal'], 'iva'=>$result['iva'], 'total'=>$result['total'] );
			endforeach;
		endif;
		/*** END | Cost VALUES - LINE CHART ***/
		
		/*** START | Cost VALUES - PIE CHART ***/
		$CostValues_Pie= array();
		$statement = $connection->prepare("
			SELECT COUNT(*) AS cost_count, COALESCE(SUM( subtotal ),0) AS subtotal, COALESCE(SUM( iva ),0) AS iva, COALESCE(SUM( total ),0) AS total
			FROM  Cost
			WHERE locked = 2
			AND canceled = 1
			AND  cost_date >= '".$DateStart." 00:00:00'
			AND  cost_date <= '".$DateEnd." 23:59:59' ");
		$statement->execute();
		$results = $statement->fetchAll();
		
		if($results[0]['cost_count']>0):
			$CostValues_Pie['cost_count']=$results[0]['cost_count'];	
			$CostValues_Pie['cost_subtotal']=$results[0]['subtotal'];	
			$CostValues_Pie['cost_iva']=$results[0]['iva'];	
			$CostValues_Pie['cost_total']=$results[0]['total'];	
		endif;
		/*** END | Cost VALUES - PIE CHART ***/
		
		/*** START | Cost TYPES - PIE CHART ***/
		$CostTypes_Pie= array();
		$statement = $connection->prepare("
			SELECT COUNT(*) AS cost_count, COALESCE(SUM( subtotal ),0) AS subtotal, COALESCE(SUM( iva ),0) AS iva, COALESCE(SUM( total ),0) AS total
			FROM  Cost
			WHERE locked = 2
			AND canceled = 1
			AND  cost_date >= '".$DateStart." 00:00:00'
			AND  cost_date <= '".$DateEnd." 23:59:59' ");
		$statement->execute();
		$results = $statement->fetchAll();
		
		if(count($results)>0):
			foreach($results as $result):
				$IvaLabel= 'Gastos';
			
				$CostTypes_Pie[]=array($IvaLabel, $result['cost_count'], $result['subtotal'], $result['iva'], $result['total']);
			endforeach;
		endif;
		/*** END | Cost TYPES - PIE CHART ***/
		
		/*** START - CostS BY DAY - TABLE ORDERED ***/
		$statement = $connection->prepare("
			SELECT 
				YEAR(b.cost_date) AS year, MONTH(b.cost_date) AS month, DAY(b.cost_date) AS day,
				COUNT(*) AS cost_count, 
				COALESCE(SUM( b.subtotal ),0) AS subtotal,  COALESCE(SUM( b.iva ),0) AS iva,  COALESCE(SUM( b.total ),0) AS total
			FROM Cost b
			WHERE b.locked = 2
			AND b.canceled = 1
			AND b.cost_date >= '".$DateStart." 00:00:00'
			AND b.cost_date <= '".$DateEnd." 23:59:59' 
			GROUP BY MONTH(cost_date), DAY(cost_date)
			ORDER BY cost_date DESC");
		$statement->execute();
		$results = $statement->fetchAll();
		
		$Subtotal=0; $IVA=0; $Total=0; $CostCount=0; 
		$CostsByDay_Total=array('subtotal'=> 0, 'iva'=> 0, 'total'=> 0, 'cost_count'=> 0, 'count'=> 0);
				
		if(count($results)>0):
			foreach($results as $result):
				$Subtotal=$Subtotal+$result['subtotal']; $IVA=$IVA+$result['iva']; 
				$Total=$Total+$result['total']; $CostCount=$CostCount+$result['cost_count']; 
				$date_key=new \DateTime($result['year'].'-'.$result['month'].'-'.$result['day']);
								
				$CostsByDay_Table[$date_key->format('Y-m-d')]=array(
					'cost_date' => $date_key->format('Y-m-d'),
					'cost_count'=>$result['cost_count'],
					'subtotal'=>$result['subtotal'],
					'iva'=>$result['iva'],
					'total'=>$result['total'],
				);
			endforeach;
			$CostsByDay_Total['subtotal']= $Subtotal;
			$CostsByDay_Total['iva']= $IVA;
			$CostsByDay_Total['total']= $Total;
			$CostsByDay_Total['cost_count']= $CostCount;
			$CostsByDay_Total['count']= count($CostsByDay_Table);
		endif;
		/*** END - RECURRENT CLIENT - TABLE ORDERED ***/
		
		//rendering template
		return $this->render('WamclientAdminBundle:Statistic:CostMonthly.html.twig',array(
        	'StatisticForm'=>$StatisticForm->createView(),
        	'CostValues_Pie'=>$CostValues_Pie, 
        	'CostTypes_Pie'=>$CostTypes_Pie, 
        	'CostValues_Line'=>$CostValues_Line, 
        	'CostsByDay_Table'=>$CostsByDay_Table, 
        	'CostsByDay_Total'=>$CostsByDay_Total, 
			'FastDate'=>$FastDate,
        ));
				
	}
		
	public function CostYearlyAction(Request $request)
    {
		//init list session
		$session = $request->getSession();
		$StatisticSession='StatisticAdminCostYearly';
		$EntityManager=$this->getUser()->getDbConnect();
		$em = $this->getDoctrine()->getManager($EntityManager);
		$connection = $em->getConnection();
		
		$statement = $connection->prepare("
			SELECT  YEAR(cost_date) AS year FROM  Cost
			WHERE canceled = 1 AND locked= 2 GROUP BY YEAR(cost_date) ORDER BY YEAR(cost_date) DESC");
		$statement->execute();
		$YearArray = $statement->fetchAll();
				
		//preparing filter form 
    	$StatisticForm = $this->createForm(new CostYearlyForm($YearArray), $session->get($StatisticSession) );
		
		//check if list has filter form
		if ($request->isMethod('POST')):
			$StatisticForm->bind($request);
			$this->ProcessFilter($StatisticForm, $StatisticSession);//processing filter POST
		endif;
		$DateSession=$session->get($StatisticSession) ;
		
		/******* STABLISH DATE PERIOD - MONTLY BY YEAR *******/		
		
		$YearStart=date('Y-01-1');
		$YearEnd=date('Y-m-d'); $YearDate='Periodo '.date('Y');
		
		if($DateSession['year_selector'] && $DateSession['year_selector'] != date('Y') ):
			$YearStart=date($DateSession['year_selector'].'-01-1');
			$YearEnd=date($DateSession['year_selector'].'-12-31'); $YearDate='Periodo '.$DateSession['year_selector'] ;
		endif;
		
		$YearEndPeriod=new \DateTime($YearEnd);
		$YearPeriod = new \DatePeriod(new \DateTime($YearStart), new \DateInterval('P1M'), $YearEndPeriod);
				
		$CostYear_Line= array();
		$CostsByMonth_Table= array();
		foreach($YearPeriod as $month):
			$CostYear_Line[$month->format("m")]=array('label'=>$month->format('F'), 'count'=>0, 'subtotal'=>0, 'iva'=>0, 'total'=>0 );
			$CostsByMonth_Table[$month->format('m')]=array('cost_date'=>$month->format('Y-m'), 'cost_count'=>0, 'subtotal'=>0, 'iva'=>0, 'total'=>0);
		endforeach;
		
		/*** Cost YEAR - LINE CHART ***/
		$statement = $connection->prepare("
			SELECT YEAR(cost_date) AS year, MONTH(cost_date) AS month, COUNT(*) AS cost_count, COALESCE(SUM( subtotal ),0) AS subtotal, COALESCE(SUM( iva ),0) AS iva, COALESCE(SUM( total ),0) AS total
			FROM  Cost
			WHERE locked = 2
			AND canceled = 1
			AND  cost_date >= '".$YearStart." 00:00:00'
			AND  cost_date <= '".$YearEnd." 23:59:59' 
			GROUP BY YEAR(cost_date), MONTH(cost_date)");
		$statement->execute();
		$results = $statement->fetchAll();
				
		if(count($results)>0):
			foreach($results as $result):
				$month_key=new \DateTime($result['year'].'-'.$result['month']);
				$CostYear_Line[$month_key->format('m')]=array('label'=>$month_key->format('F'), 'count'=>$result['cost_count'], 'subtotal'=>$result['subtotal'], 'iva'=>$result['iva'], 'total'=>$result['total'] );
			endforeach;
		endif;
				
		/*** Cost VALUES - PIE CHART ***/
		$CostValues_Pie= array();
		$statement = $connection->prepare("
			SELECT COUNT(*) AS cost_count, COALESCE(SUM( subtotal ),0) AS subtotal, COALESCE(SUM( iva ),0) AS iva, COALESCE(SUM( total ),0) AS total
			FROM  Cost
			WHERE locked = 2
			AND canceled = 1
			AND  cost_date >= '".$YearStart." 00:00:00'
			AND  cost_date <= '".$YearEnd." 23:59:59' ");
		$statement->execute();
		$results = $statement->fetchAll();
		
		if($results[0]['cost_count']>0):
			$CostValues_Pie['cost_count']=$results[0]['cost_count'];	
			$CostValues_Pie['cost_subtotal']=$results[0]['subtotal'];	
			$CostValues_Pie['cost_iva']=$results[0]['iva'];	
			$CostValues_Pie['cost_total']=$results[0]['total'];	
		endif;
		
		/*** Cost TYPES - PIE CHART ***/
		$CostTypes_Pie= array();
		$statement = $connection->prepare("
			SELECT COUNT(*) AS cost_count, COALESCE(SUM( subtotal ),0) AS subtotal, COALESCE(SUM( iva ),0) AS iva, COALESCE(SUM( total ),0) AS total
			FROM  Cost
			WHERE locked = 2
			AND canceled = 1
			AND  cost_date >= '".$YearStart." 00:00:00'
			AND  cost_date <= '".$YearEnd." 23:59:59' ");
		$statement->execute();
		$results = $statement->fetchAll();
		
		if(count($results)>0):
			foreach($results as $result):
				$IvaLabel= 'Gastos';			
				$CostTypes_Pie[]=array($IvaLabel, $result['cost_count'], $result['subtotal'], $result['iva'], $result['total']);
			endforeach;
		endif;
				
		/*** START - CostS BY MONTH - TABLE ORDERED ***/
		$statement = $connection->prepare("
			SELECT 
				YEAR(b.cost_date) AS year, MONTH(b.cost_date) AS month,
				COUNT(*) AS cost_count, 
				COALESCE(SUM( b.subtotal ),0) AS subtotal,  COALESCE(SUM( b.iva ),0) AS iva,  COALESCE(SUM( b.total ),0) AS total
			FROM Cost b
			WHERE b.locked = 2
			AND b.canceled = 1
			AND b.cost_date >= '".$YearStart." 00:00:00'
			AND b.cost_date <= '".$YearEnd." 23:59:59' 
			GROUP BY YEAR(cost_date), MONTH(cost_date)
			ORDER BY cost_date DESC");
		$statement->execute();
		$results = $statement->fetchAll();
		
		$Subtotal=0; $IVA=0; $Total=0; $CostCount=0; 
		$CostsByMonth_Total=array('subtotal'=> 0, 'iva'=> 0, 'total'=> 0, 'cost_count'=> 0, 'count'=> 0);
				
		if(count($results)>0):
			foreach($results as $result):
				$Subtotal=$Subtotal+$result['subtotal']; $IVA=$IVA+$result['iva']; 
				$Total=$Total+$result['total']; $CostCount=$CostCount+$result['cost_count']; 
				$date_key=new \DateTime($result['year'].'-'.$result['month'].'-1');
								
				$CostsByMonth_Table[$date_key->format('m')]=array(
					'cost_date' => $date_key->format('Y-m'),
					'cost_count'=>$result['cost_count'],
					'subtotal'=>$result['subtotal'],
					'iva'=>$result['iva'],
					'total'=>$result['total'],
				);
			endforeach;
			$CostsByMonth_Total['subtotal']= $Subtotal;
			$CostsByMonth_Total['iva']= $IVA;
			$CostsByMonth_Total['total']= $Total;
			$CostsByMonth_Total['cost_count']= $CostCount;
			$CostsByMonth_Total['count']= count($CostsByMonth_Table);
		endif;
		/*** END - RECURRENT CLIENT - TABLE ORDERED ***/
		
		//rendering template
		return $this->render('WamclientAdminBundle:Statistic:CostYearly.html.twig',array(
        	'StatisticForm'=>$StatisticForm->createView(),
        	'CostValues_Pie'=>$CostValues_Pie, 
        	'CostTypes_Pie'=>$CostTypes_Pie, 
			'CostYear_Line'=>$CostYear_Line,
        	'CostsByMonth_Table'=>$CostsByMonth_Table, 
        	'CostsByMonth_Total'=>$CostsByMonth_Total, 
			'YearDate'=> $YearDate
        ));
		
	}
	
	public function CostVsBillAction(Request $request)
    {
		//init list session
		$session = $request->getSession();
		$StatisticSession='StatisticAdminBillMontly';
		$EntityManager=$this->getUser()->getDbConnect();
		$em = $this->getDoctrine()->getManager($EntityManager);
		$connection = $em->getConnection();
		
		$statement = $connection->prepare("
			SELECT  YEAR(bill_date) AS year, MONTH(bill_date) AS month FROM  Bill
			WHERE canceled = 1 AND locked= 2 GROUP BY YEAR(bill_date), MONTH(bill_date)  ORDER BY bill_date DESC");
		$statement->execute();
		$MonthArray['bill'] = $statement->fetchAll();
		
		$statement = $connection->prepare("
			SELECT  YEAR(cost_date) AS year, MONTH(cost_date) AS month FROM  Cost
			WHERE canceled = 1 AND locked= 2 GROUP BY YEAR(cost_date), MONTH(cost_date)  ORDER BY cost_date DESC");
		$statement->execute();
		$MonthArray['cost'] = $statement->fetchAll();
		
		//preparing filter form 
    	$StatisticForm = $this->createForm(new CostVsBillForm($MonthArray), $session->get($StatisticSession) );
		
		//check if list has filter form
		if ($request->isMethod('POST')):
			$StatisticForm->bind($request);
			$this->ProcessFilter($StatisticForm, $StatisticSession);//processing filter POST
		endif;
		$DateSession=$session->get($StatisticSession) ;
		
		
		/******* STABLISH DATE PERIOD - MONTHLY *******/		
		$spn_month= array('0', 'Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo', 'Junio', 'Julio', 'Agosto', 'Septiembre', 'Octubre', 'Noviembre', 'Diciembre');
		
		$DateStart=date('Y-m-1',strtotime('now'));
		$DateEnd=date('Y-m-d',strtotime('now'));
			
		$FastDate= $spn_month[date('n')].', '.date('Y');
		
		if($DateSession['month_selector']) {
			$MonthSelected=explode('_', $DateSession['month_selector']);
			
			$DateStart=date('Y-m-1',strtotime($MonthSelected[0].'-'.$MonthSelected[1].'-01'));
			if(date('n')==$MonthSelected[1] && date('Y')==$MonthSelected[0]):
				$DateEnd=date('Y-m-d',strtotime('now'));
			else:
				$DateEnd=date('Y-m-t',strtotime($MonthSelected[0].'-'.$MonthSelected[1].'-01'));
			endif;
			$FastDate= $spn_month[$MonthSelected[1]].', '.$MonthSelected[0];
		}
		$EndPeriod=new \DateTime($DateEnd); $EndPeriod = $EndPeriod->modify( '+1 day' );
		$DatePeriod = new \DatePeriod(new \DateTime($DateStart), new \DateInterval('P1D'), $EndPeriod);
					
		$BillValues_Line= array();
		$CostValues_Line= array();
		foreach($DatePeriod as $date):
			$BillValues_Line[$date->format("Y-m-d")]=array('label'=>$date->format('j M'), 'count'=>0, 'total'=>0 );
			$CostValues_Line[$date->format("Y-m-d")]=array('label'=>$date->format('j M'), 'count'=>0, 'total'=>0 );
		endforeach;
		
		
		/*** START | BILL VALUES - LINE CHART ***/
		$statement = $connection->prepare("
			SELECT YEAR(bill_date) AS year, MONTH(bill_date) AS month, DAY(bill_date) AS day, COUNT(*) AS bill_count, COALESCE(SUM( total ),0) AS total
			FROM  Bill
			WHERE locked = 2
			AND canceled = 1
			AND  bill_date >= '".$DateStart." 00:00:00'
			AND  bill_date <= '".$DateEnd." 23:59:59' 
			GROUP BY MONTH(bill_date), DAY(bill_date)");
		$statement->execute();
		$results = $statement->fetchAll();
				
		if(count($results)>0):
			foreach($results as $result):
				$date_key=new \DateTime($result['year'].'-'.$result['month'].'-'.$result['day']);
				
				$BillValues_Line[$date_key->format('Y-m-d')]=array('label'=>$date_key->format('j M'), 'count'=>$result['bill_count'], 'total'=>$result['total'] );
			endforeach;
		endif;
		/*** END | BILL VALUES - LINE CHART ***/
		
		/*** START | COST VALUES - LINE CHART ***/
		$statement = $connection->prepare("
			SELECT YEAR(cost_date) AS year, MONTH(cost_date) AS month, DAY(cost_date) AS day, COUNT(*) AS cost_count, COALESCE(SUM( total ),0) AS total
			FROM  Cost
			WHERE locked = 2
			AND canceled = 1
			AND  cost_date >= '".$DateStart." 00:00:00'
			AND  cost_date <= '".$DateEnd." 23:59:59' 
			GROUP BY MONTH(cost_date), DAY(cost_date)");
		$statement->execute();
		$results = $statement->fetchAll();
				
		if(count($results)>0):
			foreach($results as $result):
				$date_key=new \DateTime($result['year'].'-'.$result['month'].'-'.$result['day']);
				
				$CostValues_Line[$date_key->format('Y-m-d')]=array('label'=>$date_key->format('j M'), 'count'=>$result['cost_count'], 'total'=>$result['total'] );
			endforeach;
		endif;
		/*** END | COST VALUES - LINE CHART ***/
				
		
		/*** START | BILL VALUES - PIE CHART ***/
		$BillValues_Pie= array();
		$statement = $connection->prepare("
			SELECT COUNT(*) AS bill_count, COALESCE(SUM( subtotal ),0) AS subtotal, COALESCE(SUM( iva ),0) AS iva, COALESCE(SUM( total ),0) AS total
			FROM  Bill
			WHERE locked = 2
			AND canceled = 1
			AND  bill_date >= '".$DateStart." 00:00:00'
			AND  bill_date <= '".$DateEnd." 23:59:59' ");
		$statement->execute();
		$results = $statement->fetchAll();
		
		if($results[0]['bill_count']>0):
			$BillValues_Pie['bill_count']=$results[0]['bill_count'];	
			$BillValues_Pie['bill_subtotal']=$results[0]['subtotal'];	
			$BillValues_Pie['bill_iva']=$results[0]['iva'];	
			$BillValues_Pie['bill_total']=$results[0]['total'];	
		endif;
		/*** END | BILL VALUES - PIE CHART ***/
				
		/*** START | Cost VALUES - PIE CHART ***/
		$CostValues_Pie= array();
		$statement = $connection->prepare("
			SELECT COUNT(*) AS cost_count, COALESCE(SUM( subtotal ),0) AS subtotal, COALESCE(SUM( iva ),0) AS iva, COALESCE(SUM( total ),0) AS total
			FROM  Cost
			WHERE locked = 2
			AND canceled = 1
			AND  cost_date >= '".$DateStart." 00:00:00'
			AND  cost_date <= '".$DateEnd." 23:59:59' ");
		$statement->execute();
		$results = $statement->fetchAll();
		
		if($results[0]['cost_count']>0):
			$CostValues_Pie['cost_count']=$results[0]['cost_count'];	
			$CostValues_Pie['cost_subtotal']=$results[0]['subtotal'];	
			$CostValues_Pie['cost_iva']=$results[0]['iva'];	
			$CostValues_Pie['cost_total']=$results[0]['total'];	
		endif;
		/*** END | Cost VALUES - PIE CHART ***/
				
				
		//rendering template
		return $this->render('WamclientAdminBundle:Statistic:CostVsBill.html.twig',array(
        	'StatisticForm'=>$StatisticForm->createView(),
        	'BillValues_Line'=>$BillValues_Line, 
        	'CostValues_Line'=>$CostValues_Line, 
        	'BillValues_Pie'=>$BillValues_Pie, 
        	'CostValues_Pie'=>$CostValues_Pie, 
			'FastDate'=>$FastDate,
        ));
				
	}
		
	public function ClientDailyAction(Request $request)
    {
		//init list session
		$session = $request->getSession();
		$StatisticSession='StatisticAdminClientDaily';
		$EntityManager=$this->getUser()->getDbConnect();
		$em = $this->getDoctrine()->getManager($EntityManager);
		$connection = $em->getConnection();
				
		//preparing filter form 
    	$StatisticForm = $this->createForm(new ClientDailyForm(), $session->get($StatisticSession) );
		
		//check if list has filter form
		if ($request->isMethod('POST')):
			$StatisticForm->bind($request);
			$this->ProcessFilter($StatisticForm, $StatisticSession);//processing filter POST
		endif;
		$DateSession=$session->get($StatisticSession) ;
		
		
		/******* STABLISH DATE PERIOD - DAILY *******/		
		$DateStart=date('Y-m-d',strtotime('-6 days'));
		$DateEnd=date('Y-m-d'); $FastDate='Últimos 7 días';
		switch ($DateSession['fast_date_selector']) {
			case 2:
				$DateStart=date('Y-m-d',strtotime('-14 days'));
				$DateEnd=date('Y-m-d'); $FastDate='Últimos 15 días';
				break;
			case 3:
				$DateStart=date('Y-m-d',strtotime('-29 days'));
				$DateEnd=date('Y-m-d'); $FastDate='Últimos 30 días';
				break;
			case 4:
				$DateStart=date('Y-m-1');
				$DateEnd=date('Y-m-d'); $FastDate='Este mes';
				break;
			case 5:
				$DateStart=date('Y-m-1',strtotime("first day of previous month"));
				$DateEnd=date('Y-m-t',strtotime("first day of previous month")); $FastDate='El mes pasado';
				break;
		}	
		$EndPeriod=new \DateTime($DateEnd); $EndPeriod = $EndPeriod->modify( '+1 day' );
		$DatePeriod = new \DatePeriod(new \DateTime($DateStart), new \DateInterval('P1D'), $EndPeriod);
				
		$NewClient_Bar= array();
		$BilledClient_Bar= array();
		foreach($DatePeriod as $date):
			$NewClient_Bar[$date->format("Y-m-d")]=array('label'=>$date->format('j M'), 'count'=>0);
			$BilledClient_Bar[$date->format("Y-m-d")]=array('label'=>$date->format('j M'), 'count'=>0);
		endforeach;
		
		/*** START - CLIENT NEW VS BILLED - BAR CHART ***/
		$statement = $connection->prepare("
			SELECT YEAR(created_at) AS year, MONTH(created_at) AS month, DAY(created_at) AS day, COUNT(*) AS client_count
			FROM Client
			WHERE created_at >= '".$DateStart." 00:00:00'
			AND created_at <= '".$DateEnd." 23:59:59' 
			GROUP BY MONTH(created_at), DAY(created_at)");
		$statement->execute();
		$results = $statement->fetchAll();
				
		if(count($results)>0):
			foreach($results as $result):
				$date_key=new \DateTime($result['year'].'-'.$result['month'].'-'.$result['day']);
				$NewClient_Bar[$date_key->format('Y-m-d')]=array('label'=>$date_key->format('j M'), 'count'=>$result['client_count']);
			endforeach;
		endif;
		
		$statement = $connection->prepare("
			SELECT YEAR(bill_date) AS year, MONTH(bill_date) AS month, DAY(bill_date) AS day, COUNT(*) AS client_count
			FROM Bill
			WHERE locked = 2
			AND canceled = 1
			AND bill_date >= '".$DateStart." 00:00:00'
			AND bill_date <= '".$DateEnd." 23:59:59' 
			GROUP BY MONTH(bill_date), DAY(bill_date)");
		$statement->execute();
		$results = $statement->fetchAll();
				
		if(count($results)>0):
			foreach($results as $result):
				$date_key=new \DateTime($result['year'].'-'.$result['month'].'-'.$result['day']);
				$BilledClient_Bar[$date_key->format('Y-m-d')]=array('label'=>$date_key->format('j M'), 'count'=>$result['client_count']);
			endforeach;
		endif;
		/*** END - CLIENT NEW VS BILLED - BAR CHART ***/
		
		/*** START - CLIENT NEW VS BILLED - PIE CHART ***/
		$NewClient_Pie= array();$NewClient_Pie['client_count']=0;
		$BilledClient_Pie= array();$BilledClient_Pie['client_count']=0;
		
		$statement = $connection->prepare("
			SELECT COUNT(*) AS client_count
			FROM Client
			WHERE created_at >= '".$DateStart." 00:00:00'
			AND created_at <= '".$DateEnd." 23:59:59'");
		$statement->execute();
		$results = $statement->fetchAll();
				
		if(count($results)>0):
			$NewClient_Pie['client_count']=$results[0]['client_count'];	
		endif;
				
		/*** START - RECURRENT CLIENT - TABLE ORDERED ***/
		$statement = $connection->prepare("
			SELECT 
				c.id AS id, c.name AS name, c.lastname AS lastname,
				COUNT(*) AS bill_count, COALESCE(SUM( b.subtotal ),0) AS subtotal,  COALESCE(SUM( b.iva ),0) AS iva,  COALESCE(SUM( b.total ),0) AS total
			FROM Bill b
			JOIN Client c
			WHERE b.locked = 2
			AND b.canceled = 1
			AND b.client_id=c.id
			AND b.bill_date >= '".$DateStart." 00:00:00'
			AND b.bill_date <= '".$DateEnd." 23:59:59' 
			GROUP BY b.client_id
			ORDER BY bill_count DESC, total DESC");
		$statement->execute();
		$results = $statement->fetchAll();
		
		if(count($results)>0):
			$BilledClient_Pie['client_count']=count($results);	
		endif;
		/*** END - CLIENT NEW VS BILLED - PIE CHART ***/
		
		$RecurrentClients=array();
		
		$Subtotal=0; $IVA=0; $Total=0; $BillCount=0; 
		$RecurrentTotal=array('subtotal'=> 0, 'iva'=> 0, 'total'=> 0, 'bill_count'=> 0, 'count'=> 0);
		
		
		if(count($results)>0):
			foreach($results as $result):
				$Subtotal=$Subtotal+$result['subtotal']; $IVA=$IVA+$result['iva']; $Total=$Total+$result['total']; 
				$BillCount=$BillCount+$result['bill_count']; 
								
				$RecurrentClients[]=array(
					'id'=>$result['id'],
					'name'=>$result['lastname'].', '.$result['name'],
					'bill_count'=>$result['bill_count'],
					'subtotal'=>$result['subtotal'],
					'iva'=>$result['iva'],
					'total'=>$result['total'],
				);
			endforeach;
			$RecurrentTotal['subtotal']= $Subtotal;
			$RecurrentTotal['iva']= $IVA;
			$RecurrentTotal['total']= $Total;
			$RecurrentTotal['bill_count']= $BillCount;
			$RecurrentTotal['count']= count($results);
		endif;
		/*** END - RECURRENT CLIENT - TABLE ORDERED ***/
		
		//rendering template
		return $this->render('WamclientAdminBundle:Statistic:ClientDaily.html.twig',array(
			'NewClient_Pie'=>$NewClient_Pie,
			'BilledClient_Pie'=>$BilledClient_Pie,
			'NewClient_Bar'=>$NewClient_Bar,
			'BilledClient_Bar'=>$BilledClient_Bar,
			'FastDate'=>$FastDate,
			'RecurrentClients'=>$RecurrentClients,
			'RecurrentTotal'=>$RecurrentTotal,
        	'StatisticForm'=>$StatisticForm->createView(),
        ));
		
	}
    
	public function ClientMonthlyAction(Request $request)
    {
		//init list session
		$session = $request->getSession();
		$StatisticSession='StatisticAdminClientMonthly';
		$EntityManager=$this->getUser()->getDbConnect();
		$em = $this->getDoctrine()->getManager($EntityManager);
		$connection = $em->getConnection();
		
		$statement = $connection->prepare("
			SELECT  YEAR(bill_date) AS year, MONTH(bill_date) AS month FROM  Bill
			WHERE canceled = 1 AND locked= 2 GROUP BY YEAR(bill_date), MONTH(bill_date)  ORDER BY bill_date DESC");
		$statement->execute();
		$MonthArray = $statement->fetchAll();
				
		//preparing filter form 
    	$StatisticForm = $this->createForm(new ClientMonthlyForm($MonthArray), $session->get($StatisticSession) );
		
		//check if list has filter form
		if ($request->isMethod('POST')):
			$StatisticForm->bind($request);
			$this->ProcessFilter($StatisticForm, $StatisticSession);//processing filter POST
		endif;
		$DateSession=$session->get($StatisticSession) ;
				
		/******* STABLISH DATE PERIOD - MONTHLY *******/		
		$spn_month= array('0', 'Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo', 'Junio', 'Julio', 'Agosto', 'Septiembre', 'Octubre', 'Noviembre', 'Diciembre');
		
		$DateStart=date('Y-m-1',strtotime('now'));
		$DateEnd=date('Y-m-d',strtotime('now'));
			
		$FastDate= $spn_month[date('n')].', '.date('Y');
		
		if($DateSession['month_selector']) {
			$MonthSelected=explode('_', $DateSession['month_selector']);
			
			$DateStart=date('Y-m-1',strtotime($MonthSelected[0].'-'.$MonthSelected[1].'-01'));
			if(date('n')==$MonthSelected[1] && date('Y')==$MonthSelected[0]):
				$DateEnd=date('Y-m-d',strtotime('now'));
			else:
				$DateEnd=date('Y-m-t',strtotime($MonthSelected[0].'-'.$MonthSelected[1].'-01'));
			endif;
			$FastDate= $spn_month[$MonthSelected[1]].', '.$MonthSelected[0];
		}
		$EndPeriod=new \DateTime($DateEnd); $EndPeriod = $EndPeriod->modify( '+1 day' );
		$DatePeriod = new \DatePeriod(new \DateTime($DateStart), new \DateInterval('P1D'), $EndPeriod);
					
		$NewClient_Bar= array();
		$BilledClient_Bar= array();
		foreach($DatePeriod as $date):
			$NewClient_Bar[$date->format("Y-m-d")]=array('label'=>$date->format('j M'), 'count'=>0);
			$BilledClient_Bar[$date->format("Y-m-d")]=array('label'=>$date->format('j M'), 'count'=>0);
		endforeach;
				
		/*** START - CLIENT NEW VS BILLED - BAR CHART ***/
		$statement = $connection->prepare("
			SELECT YEAR(created_at) AS year, MONTH(created_at) AS month, DAY(created_at) AS day, COUNT(*) AS client_count
			FROM Client
			WHERE created_at >= '".$DateStart." 00:00:00'
			AND created_at <= '".$DateEnd." 23:59:59' 
			GROUP BY MONTH(created_at), DAY(created_at)");
		$statement->execute();
		$results = $statement->fetchAll();
				
		if(count($results)>0):
			foreach($results as $result):
				$date_key=new \DateTime($result['year'].'-'.$result['month'].'-'.$result['day']);
				$NewClient_Bar[$date_key->format('Y-m-d')]=array('label'=>$date_key->format('j M'), 'count'=>$result['client_count']);
			endforeach;
		endif;
		
		$statement = $connection->prepare("
			SELECT YEAR(bill_date) AS year, MONTH(bill_date) AS month, DAY(bill_date) AS day, COUNT(*) AS client_count
			FROM Bill
			WHERE locked = 2
			AND canceled = 1
			AND bill_date >= '".$DateStart." 00:00:00'
			AND bill_date <= '".$DateEnd." 23:59:59' 
			GROUP BY MONTH(bill_date), DAY(bill_date)");
		$statement->execute();
		$results = $statement->fetchAll();
				
		if(count($results)>0):
			foreach($results as $result):
				$date_key=new \DateTime($result['year'].'-'.$result['month'].'-'.$result['day']);
				$BilledClient_Bar[$date_key->format('Y-m-d')]=array('label'=>$date_key->format('j M'), 'count'=>$result['client_count']);
			endforeach;
		endif;
		/*** END - CLIENT NEW VS BILLED - BAR CHART ***/
		
		/*** START - CLIENT NEW VS BILLED - PIE CHART ***/
		$NewClient_Pie= array();$NewClient_Pie['client_count']=0;
		$BilledClient_Pie= array();$BilledClient_Pie['client_count']=0;
		
		$statement = $connection->prepare("
			SELECT COUNT(*) AS client_count
			FROM Client
			WHERE created_at >= '".$DateStart." 00:00:00'
			AND created_at <= '".$DateEnd." 23:59:59'");
		$statement->execute();
		$results = $statement->fetchAll();
				
		if(count($results)>0):
			$NewClient_Pie['client_count']=$results[0]['client_count'];	
		endif;
				
		/*** START - RECURRENT CLIENT - TABLE ORDERED ***/
		$statement = $connection->prepare("
			SELECT 
				c.id AS id, c.name AS name, c.lastname AS lastname,
				COUNT(*) AS bill_count, COALESCE(SUM( b.subtotal ),0) AS subtotal,  COALESCE(SUM( b.iva ),0) AS iva,  COALESCE(SUM( b.total ),0) AS total
			FROM Bill b
			JOIN Client c
			WHERE b.locked = 2
			AND b.client_id=c.id
			AND b.canceled = 1
			AND b.bill_date >= '".$DateStart." 00:00:00'
			AND b.bill_date <= '".$DateEnd." 23:59:59' 
			GROUP BY b.client_id
			ORDER BY bill_count DESC, total DESC");
		$statement->execute();
		$results = $statement->fetchAll();
		
		if(count($results)>0):
			$BilledClient_Pie['client_count']=count($results);	
		endif;
		/*** END - CLIENT NEW VS BILLED - PIE CHART ***/
		$RecurrentClients=array();
		
		$Subtotal=0; $IVA=0; $Total=0; $BillCount=0; 
		$RecurrentTotal=array('subtotal'=> 0, 'iva'=> 0, 'total'=> 0, 'bill_count'=> 0, 'count'=> 0);
		
		
		if(count($results)>0):
			foreach($results as $result):
				$Subtotal=$Subtotal+$result['subtotal']; $IVA=$IVA+$result['iva']; $Total=$Total+$result['total']; 
				$BillCount=$BillCount+$result['bill_count']; 
								
				$RecurrentClients[]=array(
					'id'=>$result['id'],
					'name'=>$result['lastname'].', '.$result['name'],
					'bill_count'=>$result['bill_count'],
					'subtotal'=>$result['subtotal'],
					'iva'=>$result['iva'],
					'total'=>$result['total'],
				);
			endforeach;
			$RecurrentTotal['subtotal']= $Subtotal;
			$RecurrentTotal['iva']= $IVA;
			$RecurrentTotal['total']= $Total;
			$RecurrentTotal['bill_count']= $BillCount;
			$RecurrentTotal['count']= count($results);
		endif;
		/*** END - RECURRENT CLIENT - TABLE ORDERED ***/
		
		//rendering template
		return $this->render('WamclientAdminBundle:Statistic:ClientMonthly.html.twig',array(
			'NewClient_Pie'=>$NewClient_Pie,
			'BilledClient_Pie'=>$BilledClient_Pie,
			'NewClient_Bar'=>$NewClient_Bar,
			'BilledClient_Bar'=>$BilledClient_Bar,
			'FastDate'=>$FastDate,
			'RecurrentClients'=>$RecurrentClients,
			'RecurrentTotal'=>$RecurrentTotal,
        	'StatisticForm'=>$StatisticForm->createView(),
        ));
		
	}
	
	public function ClientYearlyAction(Request $request)
    {
		//init list session
		$session = $request->getSession();
		$StatisticSession='StatisticAdminClientYearly';
		$EntityManager=$this->getUser()->getDbConnect();
		$em = $this->getDoctrine()->getManager($EntityManager);
		$connection = $em->getConnection();
		
		$statement = $connection->prepare("
			SELECT  YEAR(bill_date) AS year FROM  Bill
			WHERE canceled = 1 AND locked= 2 GROUP BY YEAR(bill_date) ORDER BY bill_date DESC");
		$statement->execute();
		$YearArray = $statement->fetchAll();
				
		//preparing filter form 
    	$StatisticForm = $this->createForm(new ClientYearlyForm($YearArray), $session->get($StatisticSession) );
		
		//check if list has filter form
		if ($request->isMethod('POST')):
			$StatisticForm->bind($request);
			$this->ProcessFilter($StatisticForm, $StatisticSession);//processing filter POST
		endif;
		$DateSession=$session->get($StatisticSession) ;
				
		/******* STABLISH DATE PERIOD - MONTLY BY YEAR *******/		
		
		$YearStart=date('Y-01-1');
		$YearEnd=date('Y-m-d'); $YearDate='Periodo '.date('Y');
		
		if($DateSession['year_selector'] && $DateSession['year_selector'] != date('Y') ):
			$YearStart=date($DateSession['year_selector'].'-01-1');
			$YearEnd=date($DateSession['year_selector'].'-12-31'); $YearDate='Periodo '.$DateSession['year_selector'] ;
		endif;
		
		$YearEndPeriod=new \DateTime($YearEnd);
		$YearPeriod = new \DatePeriod(new \DateTime($YearStart), new \DateInterval('P1M'), $YearEndPeriod);
				
		$NewClient_Bar= array();
		$BilledClient_Bar= array();
		foreach($YearPeriod as $month):
			$NewClient_Bar[$month->format("m")]=array('label'=>$month->format('F'), 'count'=>0);
			$BilledClient_Bar[$month->format("m")]=array('label'=>$month->format('F'), 'count'=>0);
		endforeach;
										
		/*** START - CLIENT NEW VS BILLED - BAR CHART ***/
		$statement = $connection->prepare("
			SELECT YEAR(created_at) AS year, MONTH(created_at) AS month, COUNT(*) AS client_count
			FROM Client
			WHERE created_at >= '".$YearStart." 00:00:00'
			AND created_at <= '".$YearEnd." 23:59:59' 
			GROUP BY MONTH(created_at)");
		$statement->execute();
		$results = $statement->fetchAll();
				
		if(count($results)>0):
			foreach($results as $result):
				$date_key=new \DateTime($result['year'].'-'.$result['month'].'-1');
				$NewClient_Bar[$date_key->format('m')]=array('label'=>$date_key->format('F'), 'count'=>$result['client_count']);
			endforeach;
		endif;
		
		$statement = $connection->prepare("
			SELECT YEAR(bill_date) AS year, MONTH(bill_date) AS month, COUNT(*) AS client_count
			FROM Bill
			WHERE locked = 2
			AND canceled = 1
			AND bill_date >= '".$YearStart." 00:00:00'
			AND bill_date <= '".$YearEnd." 23:59:59' 
			GROUP BY MONTH(bill_date)");
		$statement->execute();
		$results = $statement->fetchAll();
				
		if(count($results)>0):
			foreach($results as $result):
				$date_key=new \DateTime($result['year'].'-'.$result['month'].'-1');
				$BilledClient_Bar[$date_key->format('m')]=array('label'=>$date_key->format('F'), 'count'=>$result['client_count']);
			endforeach;
		endif;
		/*** END - CLIENT NEW VS BILLED - BAR CHART ***/
		
		/*** START - CLIENT NEW VS BILLED - PIE CHART ***/
		$NewClient_Pie= array();$NewClient_Pie['client_count']=0;
		$BilledClient_Pie= array();$BilledClient_Pie['client_count']=0;
		
		$statement = $connection->prepare("
			SELECT COUNT(*) AS client_count
			FROM Client
			WHERE created_at >= '".$YearStart." 00:00:00'
			AND created_at <= '".$YearEnd." 23:59:59'");
		$statement->execute();
		$results = $statement->fetchAll();
				
		if(count($results)>0):
			$NewClient_Pie['client_count']=$results[0]['client_count'];	
		endif;
				
		/*** START - RECURRENT CLIENT - TABLE ORDERED ***/
		$statement = $connection->prepare("
			SELECT 
				c.id AS id, c.name AS name, c.lastname AS lastname,
				COUNT(*) AS bill_count, COALESCE(SUM( b.subtotal ),0) AS subtotal,  COALESCE(SUM( b.iva ),0) AS iva,  COALESCE(SUM( b.total ),0) AS total
			FROM Bill b
			JOIN Client c
			WHERE b.locked = 2
			AND b.client_id=c.id
			AND b.canceled = 1
			AND b.bill_date >= '".$YearStart." 00:00:00'
			AND b.bill_date <= '".$YearEnd." 23:59:59' 
			GROUP BY b.client_id
			ORDER BY bill_count DESC, total DESC");
		$statement->execute();
		$results = $statement->fetchAll();
		
		if(count($results)>0):
			$BilledClient_Pie['client_count']=count($results);	
		endif;
		/*** END - CLIENT NEW VS BILLED - PIE CHART ***/
		$RecurrentClients=array();
		
		$Subtotal=0; $IVA=0; $Total=0; $BillCount=0; 
		$RecurrentTotal=array('subtotal'=> 0, 'iva'=> 0, 'total'=> 0, 'bill_count'=> 0, 'count'=> 0);
		
		
		if(count($results)>0):
			foreach($results as $result):
				$Subtotal=$Subtotal+$result['subtotal']; $IVA=$IVA+$result['iva']; $Total=$Total+$result['total']; 
				$BillCount=$BillCount+$result['bill_count']; 
								
				$RecurrentClients[]=array(
					'id'=>$result['id'],
					'name'=>$result['lastname'].', '.$result['name'],
					'bill_count'=>$result['bill_count'],
					'subtotal'=>$result['subtotal'],
					'iva'=>$result['iva'],
					'total'=>$result['total'],
				);
			endforeach;
			$RecurrentTotal['subtotal']= $Subtotal;
			$RecurrentTotal['iva']= $IVA;
			$RecurrentTotal['total']= $Total;
			$RecurrentTotal['bill_count']= $BillCount;
			$RecurrentTotal['count']= count($results);
		endif;
		/*** END - RECURRENT CLIENT - TABLE ORDERED ***/
		
		//rendering template
		return $this->render('WamclientAdminBundle:Statistic:ClientYearly.html.twig',array(
			'NewClient_Pie'=>$NewClient_Pie,
			'BilledClient_Pie'=>$BilledClient_Pie,
			'NewClient_Bar'=>$NewClient_Bar,
			'BilledClient_Bar'=>$BilledClient_Bar,
			'RecurrentClients'=>$RecurrentClients,
			'RecurrentTotal'=>$RecurrentTotal,
        	'StatisticForm'=>$StatisticForm->createView(),
			'YearDate'=> $YearDate
        ));
		
	}
		
    public function ServiceDailyAction(Request $request)
    {
		//init list session
		$session = $request->getSession();
		$StatisticSession='StatisticAdminServiceDaily';
		$EntityManager=$this->getUser()->getDbConnect();
		$em = $this->getDoctrine()->getManager($EntityManager);
		$connection = $em->getConnection();
		
		//preparing filter form 
    	$StatisticForm = $this->createForm(new ServiceDailyForm(), $session->get($StatisticSession) );
		
		//check if list has filter form
		if ($request->isMethod('POST')):
			$StatisticForm->bind($request);
			$this->ProcessFilter($StatisticForm, $StatisticSession);//processing filter POST
		endif;
		$DateSession=$session->get($StatisticSession) ;
		
		
		/******* STABLISH DATE PERIOD - DAILY *******/		
		$DateStart=date('Y-m-d',strtotime('-6 days'));
		$DateEnd=date('Y-m-d'); $FastDate='Últimos 7 días';
		switch ($DateSession['fast_date_selector']) {
			case 2:
				$DateStart=date('Y-m-d',strtotime('-14 days'));
				$DateEnd=date('Y-m-d'); $FastDate='Últimos 15 días';
				break;
			case 3:
				$DateStart=date('Y-m-d',strtotime('-29 days'));
				$DateEnd=date('Y-m-d'); $FastDate='Últimos 30 días';
				break;
			case 4:
				$DateStart=date('Y-m-1');
				$DateEnd=date('Y-m-d'); $FastDate='Este mes';
				break;
			case 5:
				$DateStart=date('Y-m-1',strtotime("first day of previous month"));
				$DateEnd=date('Y-m-t',strtotime("first day of previous month")); $FastDate='El mes pasado';
				break;
		}
		$EndPeriod=new \DateTime($DateEnd); $EndPeriod = $EndPeriod->modify( '+1 day' );
		$DatePeriod = new \DatePeriod(new \DateTime($DateStart), new \DateInterval('P1D'), $EndPeriod);
							
		/*** START - RECURRENT SERVICES - TABLE ORDERED ***/
		$statement = $connection->prepare("
			SELECT s.name AS name, COALESCE(SUM( bs.quantity ),0) AS quantity, COALESCE(SUM( bs.total ),0) AS total
			FROM BillService bs 
			JOIN Bill b ON bs.bill_id=b.id
			JOIN Service s ON bs.service_id=s.id
			WHERE b.locked = 2
			AND bs.bill_id=b.id
			AND b.canceled = 1
			AND b.bill_date >= '".$DateStart." 00:00:00'
			AND b.bill_date <= '".$DateEnd." 23:59:59' 
			GROUP BY bs.service_id
			ORDER BY quantity DESC");
		$statement->execute();
		$results = $statement->fetchAll();
		
		$RecurrentServices=array();
		
		$Quantity=0; $Total=0;
		$RecurrentTotal=array('quantity'=> 0, 'total'=> 0);
		
		if(count($results)>0):
			foreach($results as $result):
				$Quantity=$Quantity+$result['quantity']; $Total=$Total+$result['total']; 
				
				$RecurrentServices[]=array(
					'name'=>$result['name'],
					'quantity'=>$result['quantity'],
					'total'=>$result['total'],
				);
			endforeach;
			$RecurrentTotal['quantity']= $Quantity;
			$RecurrentTotal['total']= $Total;
		endif;
		/*** END - RECURRENT SERVICES - TABLE ORDERED ***/
		
		//rendering template
		return $this->render('WamclientAdminBundle:Statistic:ServiceDaily.html.twig',array(
        	'StatisticForm'=>$StatisticForm->createView(),
        	'RecurrentServices'=>$RecurrentServices, 
        	'RecurrentTotal'=>$RecurrentTotal, 
			'FastDate'=>$FastDate,
        ));
		
	}
    
	public function ServiceMonthlyAction(Request $request)
    {
		//init list session
		$session = $request->getSession();
		$StatisticSession='StatisticAdminServiceMonthly';
		$EntityManager=$this->getUser()->getDbConnect();
		$em = $this->getDoctrine()->getManager($EntityManager);
		$connection = $em->getConnection();
				
		$statement = $connection->prepare("
			SELECT  YEAR(bill_date) AS year, MONTH(bill_date) AS month FROM  Bill
			WHERE canceled = 1 AND locked= 2 GROUP BY YEAR(bill_date), MONTH(bill_date)  ORDER BY bill_date DESC");
		$statement->execute();
		$MonthArray = $statement->fetchAll();
		
		//preparing filter form 
    	$StatisticForm = $this->createForm(new ServiceMonthlyForm($MonthArray ), $session->get($StatisticSession) );
		
		//check if list has filter form
		if ($request->isMethod('POST')):
			$StatisticForm->bind($request);
			$this->ProcessFilter($StatisticForm, $StatisticSession);//processing filter POST
		endif;
		$DateSession=$session->get($StatisticSession) ;
		
		/******* STABLISH DATE PERIOD - MONTHLY *******/		
		$spn_month= array('0', 'Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo', 'Junio', 'Julio', 'Agosto', 'Septiembre', 'Octubre', 'Noviembre', 'Diciembre');
		
		$DateStart=date('Y-m-1',strtotime('now'));
		$DateEnd=date('Y-m-d',strtotime('now'));
			
		$FastDate= $spn_month[date('n')].', '.date('Y');
		
		if($DateSession['month_selector']) {
			$MonthSelected=explode('_', $DateSession['month_selector']);
			
			$DateStart=date('Y-m-1',strtotime($MonthSelected[0].'-'.$MonthSelected[1].'-01'));
			if(date('n')==$MonthSelected[1] && date('Y')==$MonthSelected[0]):
				$DateEnd=date('Y-m-d',strtotime('now'));
			else:
				$DateEnd=date('Y-m-t',strtotime($MonthSelected[0].'-'.$MonthSelected[1].'-01'));
			endif;
			$FastDate= $spn_month[$MonthSelected[1]].', '.$MonthSelected[0];
		}
		$EndPeriod=new \DateTime($DateEnd); $EndPeriod = $EndPeriod->modify( '+1 day' );
		$DatePeriod = new \DatePeriod(new \DateTime($DateStart), new \DateInterval('P1D'), $EndPeriod);
		
		/*** START - RECURRENT SERVICES - TABLE ORDERED ***/
		$statement = $connection->prepare("
			SELECT s.name AS name, COALESCE(SUM( bs.quantity ),0) AS quantity, COALESCE(SUM( bs.total ),0) AS total
			FROM BillService bs 
			JOIN Bill b ON bs.bill_id=b.id
			JOIN Service s ON bs.service_id=s.id
			WHERE b.locked = 2
			AND bs.bill_id=b.id
			AND b.canceled = 1
			AND b.bill_date >= '".$DateStart." 00:00:00'
			AND b.bill_date <= '".$DateEnd." 23:59:59' 
			GROUP BY bs.service_id
			ORDER BY quantity DESC");
		$statement->execute();
		$results = $statement->fetchAll();
		
		$RecurrentServices=array();
		
		$Quantity=0; $Total=0;
		$RecurrentTotal=array('quantity'=> 0, 'total'=> 0);
		
		if(count($results)>0):
			foreach($results as $result):
				$Quantity=$Quantity+$result['quantity']; $Total=$Total+$result['total']; 
				
				$RecurrentServices[]=array(
					'name'=>$result['name'],
					'quantity'=>$result['quantity'],
					'total'=>$result['total'],
				);
			endforeach;
			$RecurrentTotal['quantity']= $Quantity;
			$RecurrentTotal['total']= $Total;
		endif;
		/*** END - RECURRENT SERVICES - TABLE ORDERED ***/
		
		//rendering template
		return $this->render('WamclientAdminBundle:Statistic:ServiceMonthly.html.twig',array(
        	'StatisticForm'=>$StatisticForm->createView(),
        	'RecurrentServices'=>$RecurrentServices, 
        	'RecurrentTotal'=>$RecurrentTotal, 
			'FastDate'=>$FastDate,
        ));
		
	}
    
	public function ServiceYearlyAction(Request $request)
    {
		//init list session
		$session = $request->getSession();
		$StatisticSession='StatisticAdminServiceYearly';
		$EntityManager=$this->getUser()->getDbConnect();
		$em = $this->getDoctrine()->getManager($EntityManager);
		$connection = $em->getConnection();
		
		$statement = $connection->prepare("
			SELECT  YEAR(bill_date) AS year FROM  Bill
			WHERE canceled = 1 AND locked= 2 GROUP BY YEAR(bill_date) ORDER BY bill_date DESC");
		$statement->execute();
		$YearArray = $statement->fetchAll();
		
		//preparing filter form 
    	$StatisticForm = $this->createForm(new ServiceYearlyForm($YearArray), $session->get($StatisticSession) );
		
		//check if list has filter form
		if ($request->isMethod('POST')):
			$StatisticForm->bind($request);
			$this->ProcessFilter($StatisticForm, $StatisticSession);//processing filter POST
		endif;
		$DateSession=$session->get($StatisticSession) ;
		
		
		/******* STABLISH DATE PERIOD - DAILY *******/		
		$YearStart=date('Y-01-1');
		$YearEnd=date('Y-m-d'); $FastDate='Periodo '.date('Y');
		
		if($DateSession['year_selector'] && $DateSession['year_selector'] != date('Y') ):
			$YearStart=date($DateSession['year_selector'].'-01-1');
			$YearEnd=date($DateSession['year_selector'].'-12-31'); $FastDate='Periodo '.$DateSession['year_selector'] ;
		endif;
		
		$YearEndPeriod=new \DateTime($YearEnd);
		$YearPeriod = new \DatePeriod(new \DateTime($YearStart), new \DateInterval('P1M'), $YearEndPeriod);
							
		/*** START - RECURRENT SERVICES - TABLE ORDERED ***/
		$statement = $connection->prepare("
			SELECT s.name AS name, COALESCE(SUM( bs.quantity ),0) AS quantity, COALESCE(SUM( bs.total ),0) AS total
			FROM BillService bs 
			JOIN Bill b ON bs.bill_id=b.id
			JOIN Service s ON bs.service_id=s.id
			WHERE b.locked = 2
			AND bs.bill_id=b.id
			AND b.canceled = 1
			AND b.bill_date >= '".$YearStart." 00:00:00'
			AND b.bill_date <= '".$YearEnd." 23:59:59' 
			GROUP BY bs.service_id
			ORDER BY quantity DESC");
		$statement->execute();
		$results = $statement->fetchAll();
		
		$RecurrentServices=array();
		
		$Quantity=0; $Total=0;
		$RecurrentTotal=array('quantity'=> 0, 'total'=> 0);
		
		if(count($results)>0):
			foreach($results as $result):
				$Quantity=$Quantity+$result['quantity']; $Total=$Total+$result['total']; 
				
				$RecurrentServices[]=array(
					'name'=>$result['name'],
					'quantity'=>$result['quantity'],
					'total'=>$result['total'],
				);
			endforeach;
			$RecurrentTotal['quantity']= $Quantity;
			$RecurrentTotal['total']= $Total;
		endif;
		/*** END - RECURRENT SERVICES - TABLE ORDERED ***/
		
		//rendering template
		return $this->render('WamclientAdminBundle:Statistic:ServiceYearly.html.twig',array(
        	'StatisticForm'=>$StatisticForm->createView(),
        	'RecurrentServices'=>$RecurrentServices, 
        	'RecurrentTotal'=>$RecurrentTotal, 
			'FastDate'=>$FastDate,
        ));
		
	}
	
	
	//PROCESS FILTER POST
    private function ProcessFilter($FilterForm, $FilterSession)
    {				
		$session = $this->getRequest()->getSession();
		if ($FilterForm->isValid())
		{
			$session->set($FilterSession, $FilterForm->getData()); //set filter session
			$this->get('session')->getFlashBag()->add('success', 'Se ha cambiado los parametros de las estadísticas');
		}
		else
		{			
			foreach($FilterForm->getErrors() as $error):
				$this->get('session')->getFlashBag()->add('error', $error->getMessage() );
			endforeach;
		}
	}
}
