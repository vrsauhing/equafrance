<?php

namespace Wamclient\AdminBundle\Controller;

//LIBRARIES
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;

//ENTITIES
use Wamclient\CoreBundle\Entity\Service;

//FORMS
use Wamclient\CoreBundle\Form\_Service\ServiceForm;
use Wamclient\CoreBundle\Form\_Service\ServiceFilterForm;

class ServiceController extends Controller
{
    public function ListAction(Request $request)
    {
		//init list session
		$session = $request->getSession();
		$ServiceSession='OrderAdminServiceList'; $FilterSession='FilterAdminServiceList';
		
		//preparing filter form 
    	$FilterForm = $this->createForm(new ServiceFilterForm(), $session->get($FilterSession) );
				
		//check if list has filter form
		if ($request->isMethod('POST')):
			$FilterForm->bind($request);
			$this->ProcessFilter($FilterForm, $FilterSession);//processing filter POST
		endif;
		
		//preparing respository with session variables
		$Repository= $this->getDoctrine()->getRepository('WamclientCoreBundle:Service', $this->getUser()->getDbConnect())
			->findServices(
				$session->get($FilterSession),
				$session->get($ServiceSession)
			);
			
		$ListPager = $this->get('ListPager');
		$ListPager->setRepositoryAndMaxResults(	$Repository, 15);
		
		//rendering template
		return $this->render('WamclientAdminBundle:Service:List.html.twig',array(
        	'Repository'=>$ListPager->getResults(), 
        	'ListPager'=>$ListPager->getPagerData(),
        	'FilterSession'=>$FilterSession, 
        	'ServiceSession'=>$ServiceSession, 
        	'FilterForm'=>$FilterForm->createView(),
        ));
	}
			
    public function NewAction(Request $request)
    {
		$Repository = new Service();
    	$Form = $this->createForm(new ServiceForm(), $Repository );
		
		if ($request->isMethod('POST')):
			$Form->bind($request);
			$ProcessResponse=$this->ProcessForm($Form, $Repository, true);//processing form POST
			if($ProcessResponse===true): 
				return $this->redirect($this->generateUrl('wamclient_Service_Edit', array('id' => $Repository->getId()) ));
			endif;
		endif;
		
		//rendering template
		return $this->render('WamclientAdminBundle:Service:New.html.twig',array( 
        	'form'=>$Form->createView(),
        ));
		
    }
	
    public function EditAction(Request $request, $id)
    {	
		if($id==1): return $this->redirect($this->generateUrl('wamclient_Service_New')); endif;
		
    	$Repository = $this->getDoctrine()->getRepository('WamclientCoreBundle:Service', $this->getUser()->getDbConnect())->find($id);
    	
		$Form = $this->createForm(new ServiceForm(), $Repository );
		
		if ($request->isMethod('POST')):
			$Form->bind($request);
			$this->ProcessForm($Form, $Repository, false); //processing form POST
		endif;
		
		//rendering template
		return $this->render('WamclientAdminBundle:Service:Edit.html.twig',array( 
        	'form'=>$Form->createView(),
        	'Repository'=>$Repository,
        ));
			
    }
	
	//PROCESS FORM POST
    private function ProcessForm($Form, $Repository, $IsNew)
    {
		if ($Form->isValid()):
				
			$em = $this->getDoctrine()->getManager($this->getUser()->getDbConnect());
			$em->persist($Repository); $em->flush();
			
			$this->get('session')->getFlashBag()->add('success', 'El Servicio fue guardado con éxito');
			return true;
		else:	
			foreach($Form->getErrors() as $error):
				$this->get('session')->getFlashBag()->add('error', $error->getMessage() );
			endforeach;
		endif;
	}
	
	//PROCESS FILTER POST
    private function ProcessFilter($FilterForm, $FilterSession)
    {				
		$session = $this->getRequest()->getSession();
		if ($FilterForm->isValid())
		{
			$session->set($FilterSession, $FilterForm->getData()); //set filter session
			$this->get('session')->getFlashBag()->add('success', 'Se ha filtrado el listado exitosamente');
		}
		else
		{			
			foreach($FilterForm->getErrors() as $error):
				$this->get('session')->getFlashBag()->add('error', $error->getMessage() );
			endforeach;
		}
	}
}
