<?php

namespace Wamclient\AdminBundle\Controller;

//LIBRARIES
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;

//ENTITIES
use Wamclient\CoreBundle\Entity\Cost;

//FORMS
use Wamclient\CoreBundle\Form\_Cost\CostForm;
use Wamclient\CoreBundle\Form\_Cost\CostFilterForm;

class CostController extends Controller
{
    public function ListAction(Request $request)
    {
		//init list session
		$session = $request->getSession();
		$CostSession='OrderAdminCostList'; $FilterSession='FilterAdminCostList';
		
		//preparing filter form 
		$ClientUser = $this->getDoctrine()->getRepository('WamadminCoreBundle:ClientUser')->findBy(
			array('db_connect' => $this->getUser()->getDbConnect())
		);
		
		$ClientUsers=array();
		foreach($ClientUser as $User):
			$ClientUsers[$User->getId()]=$User->getUsername();
		endforeach;
		
    	$FilterForm = $this->createForm(new CostFilterForm($ClientUsers), $session->get($FilterSession) );
				
		//check if list has filter form
		if ($request->isMethod('POST')):
			$FilterForm->bind($request);
			$this->ProcessFilter($FilterForm, $FilterSession);//processing filter POST
		endif;
		
		//preparing respository with session variables
		$Repository= $this->getDoctrine()->getRepository('WamclientCoreBundle:Cost', $this->getUser()->getDbConnect())
			->findCosts(
				$session->get($FilterSession),
				$session->get($CostSession),
				''
			);
			
		$ListPager = $this->get('ListPager');
		$ListPager->setRepositoryAndMaxResults(	$Repository, 15);
				
		//rendering template
		return $this->render('WamclientAdminBundle:Cost:List.html.twig',array(
        	'Repository'=>$ListPager->getResults(), 
        	'ListPager'=>$ListPager->getPagerData(),
        	'FilterSession'=>$FilterSession, 
        	'CostSession'=>$CostSession, 
        	'FilterForm'=>$FilterForm->createView(),
        	'ClientUsers'=>$ClientUsers,
        ));
	}
			
    public function NewAction(Request $request)
    {
		$Repository = new Cost();
		$EntityManager=$this->getUser()->getDbConnect();
		$em = $this->getDoctrine()->getManager($EntityManager);
		
    	$Form = $this->createForm(new CostForm($em), $Repository );
		
		if ($request->isMethod('POST')):
			$Form->bind($request);
			$ProcessResponse=$this->ProcessForm($Form, $Repository, true);//processing form POST
			if($ProcessResponse===true): 
				return $this->redirect($this->generateUrl('wamclient_Cost_Edit', array('id' => $Repository->getId()) ));
			endif;
		endif;
		
		//rendering template
		return $this->render('WamclientAdminBundle:Cost:New.html.twig',array( 
        	'form'=>$Form->createView(),
        	'db'=>$EntityManager,
        ));
		
    }
	
    public function EditAction(Request $request, $id)
    {		
		$EntityManager=$this->getUser()->getDbConnect();
		$em = $this->getDoctrine()->getManager($EntityManager);
		
    	$Repository = $this->getDoctrine()->getRepository('WamclientCoreBundle:Cost', $EntityManager)->find($id);
		
		if($Repository->getLocked()==1 && $Repository->getCanceled()==1){ //SI ES EDITABLE
			$Form = $this->createForm(new CostForm($em), $Repository );
				
			if ($request->isMethod('POST')):
				$Form->bind($request);
				$this->ProcessForm($Form, $Repository, false); //processing form POST
				return $this->redirect($this->generateUrl('wamclient_Cost_Edit', array('id' => $Repository->getId()) ));
			endif;
			
			$ClientUser=$this->getDoctrine()->getRepository('WamadminCoreBundle:ClientUser')->find($Repository->getWamUser());
						
			//rendering template
			return $this->render('WamclientAdminBundle:Cost:Edit.html.twig',array( 
				'form'=>$Form->createView(),
				'Repository'=>$Repository,
				'db'=>$EntityManager,
				'ClientUser'=>$ClientUser, 
			));
		}
		else{ // SI NO ES EDITABLE
			return $this->redirect($this->generateUrl('wamclient_Cost_View', array('id' => $Repository->getId()) ));
		}
			
    }
    
	public function ViewAction(Request $request, $id)
    {	
		$EntityManager=$this->getUser()->getDbConnect();
		$em = $this->getDoctrine()->getManager($EntityManager);
		
    	$Repository = $this->getDoctrine()->getRepository('WamclientCoreBundle:Cost', $EntityManager)->find($id);
											
		$ClientUser = $this->getDoctrine()->getRepository('WamadminCoreBundle:ClientUser')->findBy(
			array('db_connect' => $this->getUser()->getDbConnect())
		);
		
		$ClientUsers=array();
		foreach($ClientUser as $User):
			$ClientUsers[$User->getId()]=$User->getUsername();
		endforeach;
			
		//rendering template
		return $this->render('WamclientAdminBundle:Cost:View.html.twig',array( 
        	'Repository'=>$Repository,
			'ClientUsers'=>$ClientUsers, 
        ));
	}
	
	public function CancelAction(Request $request)
    {
		$EntityManager=$this->getUser()->getDbConnect();
		$Repository = $this->getDoctrine()->getRepository('WamclientCoreBundle:Cost', $EntityManager)->find( $request->query->get('id'));
		
		$CostId= $Repository->getId();
		$em = $this->getDoctrine()->getManager($EntityManager);		
				
		if($Repository->getCanceled()==1)
		{
			$Repository->setLocked(2);
			$Repository->setCanceled(2); 
			$em->persist($Repository); $em->flush();
												
			$this->get('session')->getFlashBag()->add('success', 'El Gasto #'.$CostId.' fue anulada con éxito.');
			return $this->redirect($this->generateUrl('wamclient_Cost_View', array('id' =>  $Repository->getId()) ));
		}
		else
		{
			$this->get('session')->getFlashBag()->add('error', 'Este gasto ya fue eliminado previamente');
			return $this->redirect($this->generateUrl('wamclient_Cost_View', array('id' =>  $Repository->getId()) ));
		}
		
	}
	
	//PROCESS POST
    private function ProcessForm($Form, $Repository, $IsNew)
    {		
		if ($Form->isValid()):
		
			$em = $this->getDoctrine()->getManager($this->getUser()->getDbConnect());			
			$Repository->setWamUser($this->getUser()->getId()); 
			
			
			
			$em->persist($Repository); $em->flush();
			
			$this->get('session')->getFlashBag()->add('success', 'El gasto fue guardado con éxito');
			return true;
		else:	
			foreach($Form->getErrors() as $error):
				$this->get('session')->getFlashBag()->add('error', $error->getMessage() );
			endforeach;
		endif;
	}
	
	
	//PROCESS FILTER POST
    private function ProcessFilter($FilterForm, $FilterSession)
    {				
		$session = $this->getRequest()->getSession();
		if ($FilterForm->isValid())
		{
			$session->set($FilterSession, $FilterForm->getData()); //set filter session
			$this->get('session')->getFlashBag()->add('success', 'Se ha filtrado el listado exitosamente');
		}
		else
		{			
			foreach($FilterForm->getErrors() as $error):
				$this->get('session')->getFlashBag()->add('error', $error->getMessage() );
			endforeach;
		}
	}
}
