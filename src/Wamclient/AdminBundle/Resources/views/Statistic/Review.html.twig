{% extends '::WamclientAdmin.html.twig' %}

{% block SiteTitle %}Resumen Estadístico{% endblock %}

{% block PageTitle %}Resumen Estadístico{% endblock %}

{% block PageAction %}
{% endblock %}

{% block PageContent %}

	<div class="col-md-4">
		<table class="table table-bordered table-condensed table-hover">
			<tr class="info"><td colspan="2" style="text-align:center;"><strong>INFORMATIVO</strong></td></tr>
			<tr class="info"><td><strong>Clientes con deudas</strong></td><td>{{Informative['client_to_collect']}}</td></tr>
			<tr class="info"><td><strong>Cuentas x Cobrar</strong></td><td>$ {{ (Informative['to_collect']*-1)|number_format(2, '.', ',') }}</td></tr>
			<tr class="info"><td colspan="2"></td></tr>
			<tr class="info"><td colspan="2"></td></tr>
			<tr class="info"><td><strong>Clientes con saldo a favor</strong></td><td>{{Informative['client_in_favor']}}</td></tr>
			<tr class="info"><td><strong>Saldos a Favor</strong></td><td>$ {{Informative['in_favor']|number_format(2, '.', ',')}}</td></tr>
			<tr class="info"><td colspan="2"></td></tr>
			<tr class="info"><td colspan="2"></td></tr>
			<tr class="info"><td><strong>Facturas Editables</strong></td><td>{{Informative['to_lock']}}</td></tr>
		</table>
		<table class="table table-bordered table-condensed table-hover">
			<tr class="active"><td colspan="2" style="text-align:center;"><strong>FACTURAS AÑO {{CurrentYear}}</strong></td></tr>
			<tr class="active"><td><strong>Facturas emitidas </strong></td><td>{{Informative['bill_count']}}</td></tr>
			<tr class="active"><td><strong>Ganancia Facturada</strong></td><td>$ {{Informative['bill_subtotal']|number_format(2, '.', ',')}}</td></tr>
			<tr class="active"><td><strong>IVA Facturado</strong></td><td>$ {{Informative['bill_iva']|number_format(2, '.', ',')}}</td></tr>
			<tr class="active"><td><strong>Total Facturado</strong></td><td>$ {{Informative['bill_total']|number_format(2, '.', ',')}}</td></tr>
		</table>
	</div>
	
	<div class="col-md-4">
		{% if Income['this_month'] > 0 %}
			<div id="IncomeChart"></div>
		{% endif %}
		<table class="table table-bordered table-condensed table-hover" >
			<tr class="success"><td colspan="2" style="text-align:center;"><strong>FACTURADO</strong></td></tr>
			<tr class="success"><td><strong>Hoy</strong></td><td>$ {{Income['today']|number_format(2, '.', ',')}}</td></tr>
			<tr class="success"><td><strong>Ayer</strong></td><td>$ {{Income['yesterday']|number_format(2, '.', ',')}}</td></tr>
			<tr class="success"><td><strong>Este mes</strong></td><td>$ {{Income['this_month']|number_format(2, '.', ',')}}</td></tr>
			<tr class="success"><td><strong>Mes Pasado</strong></td><td>$ {{Income['last_month']|number_format(2, '.', ',')}}</td></tr>
		</table>
	</div>
	
	<div class="col-md-4">
		{% if Bill['this_month'] > 0 %}
			<div id="BillChart"></div>
		{% endif %}
		<table class="table table-bordered table-condensed table-hover">
			<tr class="warning"><td colspan="2" style="text-align:center;"><strong>FACTURAS</strong></td></tr>
			<tr class="warning"><td><strong>Hoy</strong></td><td>{{Bill['today']}}</td></tr>
			<tr class="warning"><td><strong>Ayer</strong></td><td>{{Bill['yesterday']}}</td></tr>
			<tr class="warning"><td><strong>Este mes</strong></td><td>{{Bill['this_month']}}</td></tr>
			<tr class="warning"><td><strong>Mes Pasado</strong></td><td>{{Bill['last_month']}}</td></tr>
		</table>
	</div>
	
    <script src="{{ asset('/highcharts/js/highcharts.js') }}"></script>
    <script src="{{ asset('/highcharts/js/modules/exporting.js') }}"></script>
	<script>
		$(document).ready(function () {
			
			// Build the chart
			{% if Income['this_month'] > 0 %}
			
				$('#IncomeChart').highcharts({
					colors: ['#1aadce', '#624289'],
					chart: {
						backgroundColor: '#F2FFF5',
						borderColor:'#CCC',
						borderWidth: 1,
						plotBackgroundColor: null,
						plotBorderWidth: null,
						plotShadow: false
					},
					title: {
						text: 'Facturado este mes: $ {{Income['this_month']}}'
					},
					tooltip: {
						pointFormat: '{series.name}: <b>{point.percentage:.1f}%</b>'
					},
					plotOptions: {
						pie: {
							allowPointSelect: true,
							cursor: 'pointer',
							dataLabels: {
								enabled: true
							},
						}
					},
					series: [{
						type: 'pie',
						name: 'Porcentaje',
						data: [
							['Ingresos: <strong>$ {{IncomeChart['this_month_subtotal']}}</strong>',   {{ (IncomeChart['this_month_subtotal'] / Income['this_month'] * 100)|number_format(2, '.', '') }} ],
							['IVA: <strong>$ {{IncomeChart['this_month_iva']}}</strong>',   {{ (IncomeChart['this_month_iva'] / Income['this_month'] * 100)|number_format(2, '.', '') }} ],
						]
					}]
				});
			
			{% endif %}
			// Build the chart
			{% if Bill['this_month'] > 0 %}
			
				$('#BillChart').highcharts({
					colors: ['#BB4411', '#309C7E', '#1B7CB1'],
					chart: {
						backgroundColor: '#FFFDF2',
						borderColor:'#CCC',
						borderWidth: 1,
						plotBackgroundColor: null,
						plotBorderWidth: null,
						plotShadow: false
					},
					title: {
						text: 'Facturas este mes: {{Bill['this_month']}}'
					},
					tooltip: {
						pointFormat: '{series.name}: <b>{point.percentage:.1f}%</b>'
					},
					plotOptions: {
						pie: {
							allowPointSelect: true,
							cursor: 'pointer',
							dataLabels: {
								enabled: true
							},
						}
					},
					series: [{
						type: 'pie',
						name: 'Porcentaje',
						data: [
							{% for BillData in BillChart %}
								['{{BillData[0]}}: <strong>{{BillData[1]}}</strong>',   {{ (BillData[1] / Bill['this_month'] * 100)|number_format(2, '.', '') }} ],						
							{% endfor %}
						]
					}]
				});
			
			{% endif %}
		});
    
	</script>
	
	<style>
		.highcharts-button{display:none;}
	</style>
	
{% endblock %}