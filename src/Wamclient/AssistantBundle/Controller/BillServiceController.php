<?php

namespace Wamclient\AssistantBundle\Controller;

//LIBRARIES
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;

//ENTITIES
use Wamclient\CoreBundle\Entity\BillService;

//FORMS
use Wamclient\CoreBundle\Form\_Bill\BillServiceForm;

class BillServiceController extends Controller
{
    public function NewAction(Request $request, $bill_id)
    {
		$EntityManager=$this->getUser()->getDbConnect();
		$em = $this->getDoctrine()->getManager($EntityManager);
		
		$Repository = new BillService();
    	$Form = $this->createForm(new BillServiceForm($em), $Repository );
		
		if ($request->isMethod('POST')):
			$Form->bind($request);
			$ProcessResponse=$this->ProcessForm($Form, $Repository, $bill_id);//processing form POST
			
			return $this->redirect($this->generateUrl('wamclient_assistant_Bill_Edit', array('id' => $bill_id) ));
		endif;
    }
	
    public function EditAction(Request $request, $id)
    {		
		$EntityManager=$this->getUser()->getDbConnect();
		
    	$Repository = $this->getDoctrine()->getRepository('WamclientCoreBundle:BillService', $EntityManager)->find($id);
    	$Form = $this->createForm(new BillServiceForm($EntityManager), $Repository );
		
		if ($request->isMethod('POST')):
			$Form->bind($request);
			$this->ProcessForm($Form, $Repository, $Repository->getBill()->getId()); //processing form POST
			
			return $this->redirect($this->generateUrl('wamclient_assistant_Bill_Edit', array('id' =>  $Repository->getBill()->getId()) ));
		endif;
    }
		
	public function DeleteAction(Request $request)
    {
		$EntityManager=$this->getUser()->getDbConnect();
		$Repository = $this->getDoctrine()->getRepository('WamclientCoreBundle:BillService', $EntityManager)->find( $request->query->get('id'));
				
		$Bill = $this->getDoctrine()->getRepository('WamclientCoreBundle:Bill', $EntityManager)->findOneBy(
			array('id' => $Repository->getBill()->getId())
		);
		
		if($Bill->getLocked()==1)
		{
			$em = $this->getDoctrine()->getManager($EntityManager);
			$em->remove($Repository); $em->flush();
				
			$connection = $em->getConnection();
			//BILL TOTAL QUERY			
			$statement = $connection->prepare("
				SELECT SUM(total) AS total
				FROM  BillService
				WHERE bill_id =".$Bill->getId()."
				AND status = 1");
			$statement->execute();
			$results = $statement->fetchAll();
			
			$subtotal=0; $iva=0; $total=0;
			
			if($Bill->getHasIva()==1): // IVA 12% (AGREGAR IVA AL PRECIO)
				$subtotal=$results[0]['total'];
				$iva=$results[0]['total']*0.12;
				$total=$results[0]['total']*1.12;
			elseif($Bill->getHasIva()==2): // IVA 12% (DESCONTAR IVA AL PRECIO)
				$subtotal=$results[0]['total']/1.12;
				$iva=$results[0]['total']/1.12*0.12;
				$total=$results[0]['total'];			
			elseif($Bill->getHasIva()==3): // IVA 0%
				$subtotal=$results[0]['total'];
				$total=$subtotal;
			endif;
			
			$Bill->setSubtotal($subtotal); $Bill->setIva($iva); $Bill->setTotal($total);
			$em->persist($Bill); $em->flush();
			
			$this->get('session')->getFlashBag()->add('success', 'El servicio fue eliminado con éxito.');
			return $this->redirect($this->generateUrl('wamclient_assistant_Bill_Edit', array('id' =>  $Bill->getId()) ));
		}
		else
		{
			$this->get('session')->getFlashBag()->add('error', 'La factura no es editable.');
			return $this->redirect($this->generateUrl('wamclient_assistant_Bill_View', array('id' =>  $Bill->getId()) ));
		}
		
	}
	
	//PROCESS FORM POST
    private function ProcessForm($Form, $Repository, $BillId)
    {		
		if ($Form->isValid()):
			$EntityManager=$this->getUser()->getDbConnect();
			$em = $this->getDoctrine()->getManager($EntityManager);
		
            $data = $Form->getData();
			
			$Bill = $this->getDoctrine()->getRepository('WamclientCoreBundle:Bill',$EntityManager)->findOneBy(
				array('id' => $BillId)
			);
			
			$total=$data->getPrice()*$data->getQuantity();
			
			$Repository->setBill($Bill);
			$Repository->setTotal($total);
			
			$em->persist($Repository); $em->flush();
			
			$connection = $em->getConnection();
			//BILL TOTAL QUERY			
			$statement = $connection->prepare("
				SELECT SUM(total) AS total
				FROM  BillService
				WHERE bill_id =".$BillId."
				AND status = 1");
			$statement->execute();
			$results = $statement->fetchAll();
			
			$subtotal=0; $iva=0; $total=0;
			
			if($Bill->getHasIva()==1): // IVA 12% (AGREGAR IVA AL PRECIO)
				$subtotal=$results[0]['total'];
				$iva=$results[0]['total']*0.12;
				$total=$results[0]['total']*1.12;
			elseif($Bill->getHasIva()==2): // IVA 12% (DESCONTAR IVA AL PRECIO)
				$subtotal=$results[0]['total']/1.12;
				$iva=$results[0]['total']/1.12*0.12;
				$total=$results[0]['total'];			
			elseif($Bill->getHasIva()==3): // IVA 0%
				$subtotal=$results[0]['total'];
				$total=$subtotal;
			endif;
			
			$Bill->setSubtotal($subtotal); $Bill->setIva($iva); $Bill->setTotal($total);
			$em->persist($Bill); $em->flush();
			
			$this->get('session')->getFlashBag()->add('success', 'El servicio fue guardado con éxito');
			return true;
		else:	
			foreach($Form->getErrors() as $error):
				$this->get('session')->getFlashBag()->add('error', $error->getMessage() );
			endforeach;
		endif;
	}
}
