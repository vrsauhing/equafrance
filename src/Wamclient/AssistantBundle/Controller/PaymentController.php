<?php

namespace Wamclient\AssistantBundle\Controller;

//LIBRARIES
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;

//ENTITIES
use Wamclient\CoreBundle\Entity\Payment;
use Wamclient\CoreBundle\Entity\Client;

//FORMS
use Wamclient\CoreBundle\Form\_Payment\PaymentInForm;
use Wamclient\CoreBundle\Form\_Payment\PaymentOutForm;
use Wamclient\CoreBundle\Form\_Payment\PaymentFilterForm;

class PaymentController extends Controller
{
    public function IncomeBillAction(Request $request, $bill_id)
    {
		$Repository = new Payment();
		$EntityManager=$this->getUser()->getDbConnect();
		$em = $this->getDoctrine()->getManager($EntityManager);
		$Form = $this->createForm(new PaymentInForm($em), $Repository );
    			
		if ($request->isMethod('POST')):
			$Form->bind($request);
			$ProcessResponse=$this->ProcessBillForm($Form, $Repository);//processing form POST
			return $this->redirect($this->generateUrl('wamclient_assistant_Bill_Edit', array('id' => $bill_id)));
		endif;		
    }
	
    public function IncomeClientAction(Request $request, $client_id)
    {
		$Repository = new Payment();
		$EntityManager=$this->getUser()->getDbConnect();
		$em = $this->getDoctrine()->getManager($EntityManager);
		$Form = $this->createForm(new PaymentInForm($em), $Repository );
    					
		if ($request->isMethod('POST')):
			$Form->bind($request);
			$ProcessResponse=$this->ProcessInForm($Form);//processing form POST
			return $this->redirect($this->generateUrl('wamclient_assistant_Client_Edit', array('id' => $client_id)));
		endif;		
    }		
	
    public function OutcomeClientAction(Request $request, $client_id)
    {		
		$Repository = new Payment();
		$EntityManager=$this->getUser()->getDbConnect();
		$em = $this->getDoctrine()->getManager($EntityManager);
		$Form = $this->createForm(new PaymentOutForm($em), $Repository );
		
    	$Client = $this->getDoctrine()->getRepository('WamclientCoreBundle:Client', $EntityManager)->find($client_id);
		
		if ($request->isMethod('POST')):
			$Form->bind($request);
			$ProcessResponse=$this->ProcessOutForm($Form, $Repository); //processing form POST
			return $this->redirect($this->generateUrl('wamclient_assistant_Client_Edit', array('id' => $client_id)));
		endif;			
    }
	//PROCESS FORM POST
    private function ProcessInForm($Form)
    {
		if ($Form->isValid()):
            $data = $Form->getData(); $total=0;
			$EntityManager=$this->getUser()->getDbConnect();
			$em = $this->getDoctrine()->getManager($EntityManager);
			
			$Client = $this->getDoctrine()->getRepository('WamclientCoreBundle:Client', $EntityManager)->findOneBy(
				array('id' => $data->getClient()->getId())
			);
				
			$PaymentData=$data->getIncome();
			
			$Bills = $this->getDoctrine()->getRepository('WamclientCoreBundle:Bill', $EntityManager)->findBillsNoPaid(
				$data->getClient()->getId()
			);
			
			foreach($Bills as $Bill): //SI EL CLIENTE TIENE FACTURAS PENDIENTE DE PAGOS
				if($PaymentData>0):
					$TotalPaid=0; $Debt=$Bill->getTotal()-$Bill->getPaid(); 
					
					$Payment = new Payment();
					$Payment->setBill($Bill);
					$Payment->setClient($Client);
					$Payment->setWamUser($this->getUser()->getId());
					
					if($Debt > $PaymentData)://LA DEUDA ES MAS Q LO PAGADO (50-30)
						$TotalPaid=$PaymentData;
						$Payment->setDescription('<p>Se realiza un abono a la factura #'.$Bill->getId().'</p>');
					elseif($PaymentData>=$Debt ): //LO PAGADO ES MAYOR O IGUAL A LA DEUDA (30-20 20-20)
						$TotalPaid=$Debt;
						$Payment->setDescription('<p>Se cancela valores adeudados por factura #'.$Bill->getId().'</p>');
					endif;
					
					$total=$Client->getAccount()+$TotalPaid;
					
					$Payment->setIncome($TotalPaid);
					$Payment->setTotal($total);
					$Payment->setType(3);
					$em->persist($Payment);
					
					$Client->setAccount($total);
					$em->persist($Client);
					
					$Bill->setPaid($Bill->getPaid()+$TotalPaid);
					$em->persist($Bill);
					
					$em->flush();		
									
					$PaymentData=$PaymentData-$TotalPaid;
				else:
					break;
				endif;
			endforeach;
			
			if($PaymentData>0): //SI EL CLIENTE TIENE DEUDAS NO REFERENCIADAS CON FACTURAS
				$Payment = new Payment();
				$Payment->setClient($Client);
				$Payment->setWamUser($this->getUser()->getId());
				
				if($Client->getAccount()<0):
					$Payment->setDescription('<p>El cliente cancela deudas no ligadas a facturas.</p>');
					if($PaymentData>$Client->getAccount()):
					$Payment->setDescription('<p>El cliente cancela deudas y deja abono para futuras facturas.</p>');
					endif;
				elseif($Client->getAccount()>=0):
					$Payment->setDescription('<p>El cliente realiza abono para futuras facturas.</p>');
				endif;
				$total=$Client->getAccount()+$PaymentData;
				
				$Payment->setIncome($PaymentData);
				$Payment->setTotal($total);
				$Payment->setType(3); //ABONO
				$em->persist($Payment);
			
				$Client->setAccount($total);
				$em->persist($Client);
				
				$em->flush();	
			endif;
						
			$this->get('session')->getFlashBag()->add('success', 'El pago fue realizado con éxito');
			return true;
		else:	
			foreach($Form->getErrors() as $error):
				$this->get('session')->getFlashBag()->add('error', $error->getMessage() );
			endforeach;
		endif;
	}
	
    private function ProcessOutForm($Form, $Repository)
    {
		if ($Form->isValid()):
            $data = $Form->getData(); $total=0;
			$EntityManager=$this->getUser()->getDbConnect();
			$em = $this->getDoctrine()->getManager($EntityManager);
			
			$Client = $this->getDoctrine()->getRepository('WamclientCoreBundle:Client', $EntityManager)->findOneBy(
				array('id' => $data->getClient()->getId())
			);
		
			$total=$Client->getAccount()-$data->getOutcome();
			
			$Repository->setDescription('<p>Se registra deuda por '.$data->getDescription().'</p>');
			$Repository->setWamUser($this->getUser()->getId());
			$Repository->setOutcome($data->getOutcome());
			$Repository->setTotal($total);
			$Repository->setType(4); //DEUDA
			$em->persist($Repository);
			
			$Client->setAccount($total);
			$em->persist($Client);
			
			$em->flush();			
			
			$this->get('session')->getFlashBag()->add('success', 'El pago fue realizado con éxito');
			return true;
		else:	
			foreach($Form->getErrors() as $error):
				$this->get('session')->getFlashBag()->add('error', $error->getMessage() );
			endforeach;
		endif;
	}
	
	private function ProcessBillForm($Form, $Repository)
    {
		//REGISTRA PAGOS O ABONOS REALIZADOS DESDE "VER FACTURAS"
		if ($Form->isValid()):
            $data = $Form->getData(); $total=0;
			$EntityManager=$this->getUser()->getDbConnect();
			$em = $this->getDoctrine()->getManager($EntityManager);
			
			$Client = $this->getDoctrine()->getRepository('WamclientCoreBundle:Client', $EntityManager)->findOneBy(
				array('id' => $data->getClient()->getId())
			);
			
			if($data->getBill()):
				$Bill = $this->getDoctrine()->getRepository('WamclientCoreBundle:Bill', $EntityManager)->findOneBy(
					array('id' => $data->getBill()->getId())
				);
				$Debt=$Bill->getTotal() - $Bill->getPaid();
				if($Debt+0.001 >= $data->getIncome()):
					$Bill->setPaid($Bill->getPaid()+$data->getIncome());
					$em->persist($Bill);
				else:
					$this->get('session')->getFlashBag()->add('error', 'El abono excede al valor por pagar.');
					return true;
				endif;
				
				if($Debt  > $data->getIncome()):
					$Repository->setDescription('<p>Se realiza un abono a la factura #'.$Bill->getId().'</p>');
				endif;
			else:
				$this->get('session')->getFlashBag()->add('error', 'Este pago no tiene asignado ninguna factura.');
				return true;
			endif;
			
			$total=$Client->getAccount()+$data->getIncome();
			
			$Repository->setWamUser($this->getUser()->getId());
			$Repository->setTotal($total);
			$em->persist($Repository);
			
			$Client->setAccount($total);
			$em->persist($Client);
			
			$em->flush();			
			
			$this->get('session')->getFlashBag()->add('success', 'El pago fue guardado con éxito');
			return true;
		else:	
			foreach($Form->getErrors() as $error):
				$this->get('session')->getFlashBag()->add('error', $error->getMessage() );
			endforeach;
		endif;
	}
}
