<?php

namespace Wamclient\AssistantBundle\Controller;

//LIBRARIES
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;

//ENTITIES
use Wamclient\CoreBundle\Entity\Client;
use Wamclient\CoreBundle\Entity\Payment;

//FORMS
use Wamclient\CoreBundle\Form\_Client\ClientForm;
use Wamclient\CoreBundle\Form\_Client\ClientFilterForm;
use Wamclient\CoreBundle\Form\_Payment\PaymentInForm;
use Wamclient\CoreBundle\Form\_Payment\PaymentOutForm;


class ClientController extends Controller
{
    public function ListAction(Request $request)
    {
		//init list session
		$session = $request->getSession();
		$OrderSession='OrderAdminClientList'; $FilterSession='FilterAdminClientList';
		
		//preparing filter form 
    	$FilterForm = $this->createForm(new ClientFilterForm(), $session->get($FilterSession) );
				
		//check if list has filter form
		if ($request->isMethod('POST')):
			$FilterForm->bind($request);
			$this->ProcessFilter($FilterForm, $FilterSession);//processing filter POST
		endif;
		
		//preparing respository with session variables
		$Repository= $this->getDoctrine()->getRepository('WamclientCoreBundle:Client', $this->getUser()->getDbConnect())
			->findClients(
				$session->get($FilterSession),
				$session->get($OrderSession)
			);
			
		$ListPager = $this->get('ListPager');
		$ListPager->setRepositoryAndMaxResults(	$Repository, 15);
		
		//rendering template
		return $this->render('WamclientAssistantBundle:Client:List.html.twig',array(
        	'Repository'=>$ListPager->getResults(), 
        	'ListPager'=>$ListPager->getPagerData(),
        	'FilterSession'=>$FilterSession, 
        	'OrderSession'=>$OrderSession, 
        	'FilterForm'=>$FilterForm->createView(),
        ));
	}
			
    public function NewAction(Request $request)
    {
		$Repository = new Client();
		$EntityManager=$this->getUser()->getDbConnect();
		$em = $this->getDoctrine()->getManager($EntityManager);
		
    	$Form = $this->createForm(new ClientForm($em), $Repository );
		
		if ($request->isMethod('POST')):
			$Form->bind($request);
			$ProcessResponse=$this->ProcessForm($Form, $Repository, true);//processing form POST
			if($ProcessResponse===true): 
				return $this->redirect($this->generateUrl('wamclient_assistant_Client_Edit', array('id' => $Repository->getId()) ));
			endif;
		endif;
		
		//rendering template
		return $this->render('WamclientAssistantBundle:Client:New.html.twig',array( 
        	'form'=>$Form->createView(),
        	'db'=>$EntityManager,
        ));
		
    }
	
    public function EditAction(Request $request, $id)
    {		
		if($id==1): return $this->redirect($this->generateUrl('wamclient_assistant_Client_New')); endif;
		
		$EntityManager=$this->getUser()->getDbConnect();
		$em = $this->getDoctrine()->getManager($EntityManager);
		
    	$Repository = $this->getDoctrine()->getRepository('WamclientCoreBundle:Client', $EntityManager)->find($id);
		$Payments= $this->getDoctrine()->getRepository('WamclientCoreBundle:Payment', $EntityManager)->findClientPayments($id);
		$Bills= $this->getDoctrine()->getRepository('WamclientCoreBundle:Bill', $EntityManager)->findClientBills($id);
							
		$Form = $this->createForm(new ClientForm($em), $Repository );
		
		$ClientUser = $this->getDoctrine()->getRepository('WamadminCoreBundle:ClientUser')->findBy(
			array('db_connect' => $EntityManager)
		);
		$ClientUsers=array();
		foreach($ClientUser as $User):
			$ClientUsers[$User->getId()]=$User->getUsername();
		endforeach;
				
		$NewPaymentIn = new Payment();
		$PaymentInForm = $this->createForm(new PaymentInForm($em), $NewPaymentIn );
		
		$NewPaymentOut = new Payment();
		$PaymentOutForm = $this->createForm(new PaymentOutForm($em), $NewPaymentOut );
		
		//check if list has filter form
		if ($request->isMethod('POST')):
			$Form->bind($request);
			$this->ProcessForm($Form, $Repository, false); //processing form POST
		endif;
		
		//rendering template
		return $this->render('WamclientAssistantBundle:Client:Edit.html.twig',array( 
        	'form'=>$Form->createView(),
        	'PaymentInForm'=>$PaymentInForm->createView(),
        	'PaymentOutForm'=>$PaymentOutForm->createView(),
        	'Repository'=>$Repository,
        	'Payments'=>$Payments,
        	'Bills'=>$Bills,
        	'ClientUsers'=>$ClientUsers,
        	'db'=>$EntityManager,
        ));
			
    }
	
	//PROCESS FORM POST
    private function ProcessForm($Form, $Repository, $IsNew)
    {
		if ($Form->isValid()):
					
			$em = $this->getDoctrine()->getManager($this->getUser()->getDbConnect());
			$em->persist($Repository); $em->flush();
			
			$this->get('session')->getFlashBag()->add('success', 'El cliente fue guardado con éxito');
			return true;
		else:	
			foreach($Form->getErrors() as $error):
				$this->get('session')->getFlashBag()->add('error', $error->getMessage() );
			endforeach;
		endif;
	}
	
	//PROCESS FILTER POST
    private function ProcessFilter($FilterForm, $FilterSession)
    {				
		$session = $this->getRequest()->getSession();
		if ($FilterForm->isValid())
		{
			$session->set($FilterSession, $FilterForm->getData()); //set filter session
			$this->get('session')->getFlashBag()->add('success', 'Se ha filtrado el listado exitosamente');
		}
		else
		{			
			foreach($FilterForm->getErrors() as $error):
				$this->get('session')->getFlashBag()->add('error', $error->getMessage() );
			endforeach;
		}
	}
}
