<?php

namespace Wamclient\AssistantBundle\Controller;

//LIBRARIES
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;

//ENTITIES
use Wamclient\CoreBundle\Entity\Bill;
use Wamclient\CoreBundle\Entity\Payment;
use Wamclient\CoreBundle\Entity\BillService;

//FORMS
use Wamclient\CoreBundle\Form\_Bill\BillForm;
use Wamclient\CoreBundle\Form\_Bill\BillServiceForm;
use Wamclient\CoreBundle\Form\_Bill\BillFilterForm;
use Wamclient\CoreBundle\Form\_Payment\PaymentInForm;

class BillController extends Controller
{
    public function ListAction(Request $request)
    {
		//init list session
		$session = $request->getSession();
		$BillSession='OrderAdminBillList'; $FilterSession='FilterAdminBillList';
		
		//preparing filter form 
		$ClientUser = $this->getDoctrine()->getRepository('WamadminCoreBundle:ClientUser')->findBy(
			array('db_connect' => $this->getUser()->getDbConnect())
		);
		
		$ClientUsers=array();
		foreach($ClientUser as $User):
			$ClientUsers[$User->getId()]=$User->getUsername();
		endforeach;
		
    	$FilterForm = $this->createForm(new BillFilterForm(array()), $session->get($FilterSession) );
				
		//check if list has filter form
		if ($request->isMethod('POST')):
			$FilterForm->bind($request);
			$this->ProcessFilter($FilterForm, $FilterSession);//processing filter POST
		endif;
		
		//preparing respository with session variables
		$Repository= $this->getDoctrine()->getRepository('WamclientCoreBundle:Bill', $this->getUser()->getDbConnect())
			->findBills(
				$session->get($FilterSession),
				$session->get($BillSession),
				$this->getUser()->getId()
			);
			
		$ListPager = $this->get('ListPager');
		$ListPager->setRepositoryAndMaxResults(	$Repository, 15);
				
		//rendering template
		return $this->render('WamclientAssistantBundle:Bill:List.html.twig',array(
        	'Repository'=>$ListPager->getResults(), 
        	'ListPager'=>$ListPager->getPagerData(),
        	'FilterSession'=>$FilterSession, 
        	'BillSession'=>$BillSession, 
        	'FilterForm'=>$FilterForm->createView(),
        	'ClientUsers'=>$ClientUsers,
        ));
	}
			
    public function NewAction(Request $request)
    {
		$Repository = new Bill();
		$EntityManager=$this->getUser()->getDbConnect();
		$em = $this->getDoctrine()->getManager($EntityManager);
    	$Form = $this->createForm(new BillForm($em), $Repository );
		
		if ($request->isMethod('POST')):
			$Form->bind($request);
			$ProcessResponse=$this->ProcessForm($Form, $Repository, true, 0);//processing form POST
			if($ProcessResponse===true): 
				return $this->redirect($this->generateUrl('wamclient_assistant_Bill_Edit', array('id' => $Repository->getId()) ));
			endif;
		endif;
		
		//rendering template
		return $this->render('WamclientAssistantBundle:Bill:New.html.twig',array( 
        	'form'=>$Form->createView(),
        	'db'=>$EntityManager,
        ));
		
    }
	
    public function EditAction(Request $request, $id)
    {		
		$EntityManager=$this->getUser()->getDbConnect();
		$em = $this->getDoctrine()->getManager($EntityManager);
		
    	$Repository = $this->getDoctrine()->getRepository('WamclientCoreBundle:Bill', $EntityManager)->find($id);
		$HasIva=$Repository->getHasIva();
		
		if($Repository->getLocked()==1){ //SI ES EDITABLE
			$Form = $this->createForm(new BillForm($em), $Repository );
				
			$NewBillService = new BillService();
			$BillServiceForm = $this->createForm(new BillServiceForm($em), $NewBillService );
			
			$session = $request->getSession();
			$BillServiceSession='OrderAdminBillServiceList'; 
			$BillService = $this->getDoctrine()->getRepository('WamclientCoreBundle:BillService',$EntityManager)->findBillServices(
				$Repository,
				$session->get($BillServiceSession)
			);

			if ($request->isMethod('POST')):
				$Form->bind($request);
				$NotEditable= $this->ProcessForm($Form, $Repository, false, $HasIva); //processing form POST
				if($NotEditable=='No Editable'): 
					return $this->redirect($this->generateUrl('wamclient_assistant_Bill_View', array('id' => $Repository->getId()) ));
				endif;
			endif;
			
			$ClientUser=$this->getDoctrine()->getRepository('WamadminCoreBundle:ClientUser')->find($Repository->getWamUser());
						
			//rendering template
			return $this->render('WamclientAssistantBundle:Bill:Edit.html.twig',array( 
				'form'=>$Form->createView(),
				'new_service_form'=>$BillServiceForm->createView(),
				'Repository'=>$Repository,
				'BillService'=>$BillService,
				'db'=>$EntityManager,
				'BillServiceSession'=>$BillServiceSession, 
				'ClientUser'=>$ClientUser, 
			));
		}
		else{ // SI NO ES EDITABLE
			return $this->redirect($this->generateUrl('wamclient_assistant_Bill_View', array('id' => $Repository->getId()) ));
		}
			
    }
    
	public function ViewAction(Request $request, $id)
    {	
		$EntityManager=$this->getUser()->getDbConnect();
		$em = $this->getDoctrine()->getManager($EntityManager);
		
    	$Repository = $this->getDoctrine()->getRepository('WamclientCoreBundle:Bill', $EntityManager)->find($id);
						    	
		$session = $request->getSession();
		$BillServiceSession='OrderAdminBillServiceList'; 
		$BillService = $this->getDoctrine()->getRepository('WamclientCoreBundle:BillService',$EntityManager)->findBillServices(
			$Repository,
			$session->get($BillServiceSession)
		);
		
		$NewPayment = new Payment();
		$PaymentInForm = $this->createForm(new PaymentInForm($em), $NewPayment );
				
		$ClientUser = $this->getDoctrine()->getRepository('WamadminCoreBundle:ClientUser')->findBy(
			array('db_connect' => $this->getUser()->getDbConnect())
		);
		
		$ClientUsers=array();
		foreach($ClientUser as $User):
			$ClientUsers[$User->getId()]=$User->getUsername();
		endforeach;
		
		$Payments= $this->getDoctrine()->getRepository('WamclientCoreBundle:Payment', $this->getUser()->getDbConnect())->findBillPayments($id);
			
		//rendering template
		return $this->render('WamclientAssistantBundle:Bill:View.html.twig',array( 
        	'PaymentInForm'=>$PaymentInForm->createView(),
        	'Repository'=>$Repository,
        	'BillService'=>$BillService,
        	'BillServiceSession'=>$BillServiceSession, 
			'ClientUsers'=>$ClientUsers, 
			'Payments'=>$Payments, 
        ));
	}
	
	public function CancelAction(Request $request)
    {
		$EntityManager=$this->getUser()->getDbConnect();
		$Repository = $this->getDoctrine()->getRepository('WamclientCoreBundle:Bill', $EntityManager)->find( $request->query->get('id'));
				
		$em = $this->getDoctrine()->getManager($EntityManager);
		
		if($Repository->getCanceled()==1)
		{
			if($Repository->getLocked()==1)
			{
				$Repository->setLocked(2);
				$Repository->setCanceled(2); 
				$em->persist($Repository); $em->flush();
										
				$this->get('session')->getFlashBag()->add('success', 'La factura fue anulada con éxito.');
				return $this->redirect($this->generateUrl('wamclient_assistant_Bill_View', array('id' =>  $Repository->getId()) ));
			}
			elseif($Repository->getLocked()==2)
			{
				$Repository->setCanceled(2); 
				$em->persist($Repository); 
							
				$Client = $this->getDoctrine()->getRepository('WamclientCoreBundle:Client', $EntityManager)->findOneBy(
					array('id' => $Repository->getClient()->getId())
				);
				
				$BillId=$Repository->getId();
				
				$PaymentIn = new Payment();
				$PaymentIn->setBill($Repository);
				$PaymentIn->setClient($Client);
				$PaymentIn->setWamUser($this->getUser()->getId());
				$PaymentIn->setDescription('<p>Se anuló la factura #'.$BillId.'.<br />Se repone valores ingresados previamente como deuda.</p>');
				$PaymentIn->setIncome($Repository->getTotal());
				$PaymentIn->setTotal($Client->getAccount()+$Repository->getTotal());
				$PaymentIn->setType(2);
				
				$em->persist($PaymentIn);
				
				$Client->setAccount($PaymentIn->getTotal());
				$em->persist($Client); 
				
				if($Repository->getPaid()>0):
					$PaymentOut = new Payment();
					$PaymentOut->setBill($Repository);
					$PaymentOut->setClient($Client);
					$PaymentOut->setWamUser($this->getUser()->getId());
					$PaymentOut->setDescription('<p>Se devuelve los valores cancelados por la factura #'.$BillId.'.</p>');
					$PaymentOut->setOutcome($Repository->getPaid());
					$PaymentOut->setTotal($Client->getAccount()-$Repository->getPaid());
					$PaymentOut->setType(2);
					
					$em->persist($PaymentOut);
					
					$Client->setAccount($PaymentOut->getTotal());
					$em->persist($Client); 
				endif;
				
				$em->flush();
										
				$this->get('session')->getFlashBag()->add('success', 'La factura #'.$BillId.' fue anulada con éxito.');
				return $this->redirect($this->generateUrl('wamclient_assistant_Bill_View', array('id' =>  $Repository->getId()) ));
			}
		}
		else
		{
			$this->get('session')->getFlashBag()->add('error', 'Esta factura ya fue eliminada previamente');
			return $this->redirect($this->generateUrl('wamclient_assistant_Bill_View', array('id' =>  $Repository->getId()) ));
		}
		
	}
	
	//PROCESS POST
    private function ProcessForm($Form, $Repository, $IsNew, $HasIva)
    {
		
		if ($Form->isValid()):
            $data = $Form->getData();
			$em = $this->getDoctrine()->getManager($this->getUser()->getDbConnect());
			
			if($IsNew===false) //SI NO ES NUEVO
			{
				if($Repository->getHasIva()!=$HasIva) //SI CAMBIA EL TIPO DE IVA
				{
					//BILL TOTAL QUERY	
					$connection = $em->getConnection();		
					$statement = $connection->prepare("
						SELECT SUM(total) AS total
						FROM  BillService
						WHERE bill_id =".$Repository->getId()."
						AND status = 1");
					$statement->execute();
					$results = $statement->fetchAll();
						
					$subtotal=0; $iva=0; $total=0;
					
					if($data->getHasIva()==1): // IVA 12% (AGREGAR IVA AL PRECIO)
						$subtotal=$results[0]['total'];
						$iva=$results[0]['total']*0.12;
						$total=$results[0]['total']*1.12;
					elseif($data->getHasIva()==2): // IVA 12% (DESCONTAR IVA AL PRECIO)
						$subtotal=$results[0]['total']/1.12;
						$iva=$results[0]['total']/1.12*0.12;
						$total=$results[0]['total'];			
					elseif($data->getHasIva()==3): // IVA 0%
						$subtotal=$results[0]['total'];
						$total=$subtotal;
					endif;
					
					$Repository->setSubtotal($subtotal); $Repository->setIva($iva); $Repository->setTotal($total);
				}
				$em->persist($Repository); $em->flush();	
			}
			else //SI ES NUEVO
			{
				$Repository->setWamUser($this->getUser()->getId()); 
				$em->persist($Repository); $em->flush();
				
				$this->get('session')->getFlashBag()->add('success', 'La Factura fue guardada con éxito');
				return true;
			}			
			
			$this->get('session')->getFlashBag()->add('success', 'La Factura fue guardada con éxito');
				
			if($data->getLocked()==2 ) //EL USUARIO TERMINO LA EDICION
			{			
				$Client = $this->getDoctrine()->getRepository('WamclientCoreBundle:Client', $this->getUser()->getDbConnect())->findOneBy(
					array('id' => $Repository->getClient()->getId())
				);
				$Credit=$Client->getAccount();
				
				$BillId=$Repository->getId();
				
				$PaymentOut = new Payment();
				$PaymentOut->setBill($Repository);
				$PaymentOut->setClient($Client);
				$PaymentOut->setWamUser($this->getUser()->getId());
				$PaymentOut->setDescription('<p>El cliente adeuda valores por la factura #'.$BillId.'</p>');
				
				if($Credit>0)://SI EL CLIENTE TIENE CREDITO
					if($Credit+0.001>=$Repository->getTotal() ): 
						$PaymentOut->setDescription('<p>El cliente adeuda valores por la factura #'.$BillId.'.<br />Se cruza $ '.$Repository->getTotal().' del credito a favor.</p>');
						$Repository->setPaid($Repository->getTotal());
						$em->persist($Repository); 
					elseif($Credit<$Repository->getTotal()):
						$PaymentOut->setDescription('<p>El cliente adeuda valores por la factura #'.$BillId.'.<br />Se cruza $ '.$Credit.' del credito a favor</p>');
						$Repository->setPaid($Credit);
						$em->persist($Repository); 
					endif;
				endif;
				
				$PaymentOut->setOutcome($Repository->getTotal());
				$PaymentOut->setTotal($Client->getAccount()-$Repository->getTotal());
				$em->persist($PaymentOut);
				
				$Client->setAccount($PaymentOut->getTotal());
				$em->persist($Client); 
				
				
			
				if($Client->getId()==1): //SI EL CLIENTE ES CONSUMIDOR FINAL				
					$PaymentIn = new Payment();
					$PaymentIn->setBill($Repository);
					$PaymentIn->setClient($Client);
					$PaymentIn->setWamUser($this->getUser()->getId());
					$PaymentIn->setDescription('<p>Se cancela valores adeudados por factura #'.$BillId.'</p>');
					$PaymentIn->setIncome($Repository->getTotal());
					$PaymentIn->setTotal($Client->getAccount()+$Repository->getTotal());
					$em->persist($PaymentIn);
					
					$Repository->setPaid($Repository->getTotal());
					$em->persist($Repository); 
										
					$Client->setAccount($PaymentIn->getTotal());
					$em->persist($Client); 
				endif;
				
				
				$em->flush();
				
				return 'No Editable';
			}
			
			return false;
		else:	
			foreach($Form->getErrors() as $error):
				$this->get('session')->getFlashBag()->add('error', $error->getMessage() );
			endforeach;
		endif;
	}
	
	
	//PROCESS FILTER POST
    private function ProcessFilter($FilterForm, $FilterSession)
    {				
		$session = $this->getRequest()->getSession();
		if ($FilterForm->isValid())
		{
			$session->set($FilterSession, $FilterForm->getData()); //set filter session
			$this->get('session')->getFlashBag()->add('success', 'Se ha filtrado el listado exitosamente');
		}
		else
		{			
			foreach($FilterForm->getErrors() as $error):
				$this->get('session')->getFlashBag()->add('error', $error->getMessage() );
			endforeach;
		}
	}
}
