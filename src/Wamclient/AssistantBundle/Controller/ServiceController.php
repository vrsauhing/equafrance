<?php

namespace Wamclient\AssistantBundle\Controller;

//LIBRARIES
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;

//ENTITIES
use Wamclient\CoreBundle\Entity\Service;

//FORMS
use Wamclient\CoreBundle\Form\_Service\ServiceForm;
use Wamclient\CoreBundle\Form\_Service\ServiceFilterForm;

class ServiceController extends Controller
{
    public function ListAction(Request $request)
    {
		//init list session
		$session = $request->getSession();
		$ServiceSession='OrderAdminServiceList'; $FilterSession='FilterAdminServiceList';
		
		//preparing filter form 
    	$FilterForm = $this->createForm(new ServiceFilterForm(), $session->get($FilterSession) );
				
		//check if list has filter form
		if ($request->isMethod('POST')):
			$FilterForm->bind($request);
			$this->ProcessFilter($FilterForm, $FilterSession);//processing filter POST
		endif;
		
		//preparing respository with session variables
		$Repository= $this->getDoctrine()->getRepository('WamclientCoreBundle:Service', $this->getUser()->getDbConnect())
			->findServices(
				$session->get($FilterSession),
				$session->get($ServiceSession)
			);
			
		$ListPager = $this->get('ListPager');
		$ListPager->setRepositoryAndMaxResults(	$Repository, 15);
		
		//rendering template
		return $this->render('WamclientAssistantBundle:Service:List.html.twig',array(
        	'Repository'=>$ListPager->getResults(), 
        	'ListPager'=>$ListPager->getPagerData(),
        	'FilterSession'=>$FilterSession, 
        	'ServiceSession'=>$ServiceSession, 
        	'FilterForm'=>$FilterForm->createView(),
        ));
	}
	
	//PROCESS FILTER POST
    private function ProcessFilter($FilterForm, $FilterSession)
    {				
		$session = $this->getRequest()->getSession();
		if ($FilterForm->isValid())
		{
			$session->set($FilterSession, $FilterForm->getData()); //set filter session
			$this->get('session')->getFlashBag()->add('success', 'Se ha filtrado el listado exitosamente');
		}
		else
		{			
			foreach($FilterForm->getErrors() as $error):
				$this->get('session')->getFlashBag()->add('error', $error->getMessage() );
			endforeach;
		}
	}
}
